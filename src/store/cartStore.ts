// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

import { create } from 'zustand';
import { persist } from 'zustand/middleware';

import { QueryProduct } from '@/lib/graphql';
import { isOnSale, toFixedNumber } from '@/lib/helper';
import { CartItem, Discount, Product } from '@/lib/interfaces';

import { AddressFormType } from '@/components/elements/forms/AddressForm';

export interface CartState {
  cartItems: CartItem[];
  totalItems: number;
  totalPrice: number;
  orderId?: string;
  orderIntentId?: string;
  discount?: Discount;
  address: AddressFormType;
  stripeClientSecret?: string;
  stripePaymentIntentId?: string;
}

export interface CartActions {
  increment: (product: Product, quantity?: number) => void;
  decrement: (product: Product, quantity?: number) => void;
  emptyCart: () => void;
  refreshCart: () => void;
  setOrderId: (orderId: string) => void;
  setOrderIntentId: (orderIntentId: string) => void;
  setDiscount: (discount: Discount) => void;
  setAddress: (address: AddressFormType) => void;
  setStripeClientSecret: (stripeClientSecret: string) => void;
  setStripePaymentIntentId: (stripePaymentIntentId: string) => void;
}

const initialState: CartState = {
  cartItems: [],
  totalItems: 0,
  totalPrice: 0,
  orderId: undefined,
  orderIntentId: undefined,
  discount: undefined,
  stripeClientSecret: undefined,
  stripePaymentIntentId: undefined,
};

export const useCart = create<CartState & CartActions>()(
  persist(
    (set, get) => ({
      ...initialState,
      increment: (product, quantity = 1) =>
        set((state) => {
          const item = state.cartItems.find(
            (el) =>
              el.product.id === product.id &&
              el.product.selectedSize === product.selectedSize &&
              (product.selectedType
                ? el.product.selectedType === product.selectedType
                : true)
          );

          const global_quantity = product.attributes.global_quantity;

          //Check if product quantity doesn't exceed global product quantity regardless of size
          const total_quantity = state.cartItems
            .filter((it) => it.product.id === product.id)
            .reduce((acc, item) => acc + item.qty, 0);
          if (
            global_quantity !== null &&
            global_quantity !== undefined &&
            global_quantity < total_quantity + quantity
          ) {
            return {};
          }

          if (!item) {
            if (product.selectedSize) {
              // Find the corresponding size in the product attributes
              const size = product.attributes.sizes.find(
                (pSize) => pSize.size === product.selectedSize
              );

              // Check if global quantity exists and has enough quantity
              if (
                (typeof global_quantity !== undefined ||
                  global_quantity !== null) &&
                global_quantity > 0 &&
                global_quantity >= quantity
              ) {
                // Check if the size exists and if the quantity is sufficient or unlimited
                if (size && (size.quantity > 0 || size.quantity === -1)) {
                  // Check if the requested quantity does not exceed the available quantity
                  if (size.quantity >= quantity || size.quantity === -1) {
                    const price = isOnSale(
                      product.attributes.date_on_sale_from,
                      product.attributes.date_on_sale_to
                    )
                      ? product.attributes.sale_price ??
                        product.attributes.price
                      : product.attributes.price;

                    const colorName = product.attributes.colors?.find(
                      (c) => c.product.data.id === product.id
                    )?.name;

                    const totalPrice = Number(
                      (state.totalPrice + price * quantity).toFixed(2)
                    );

                    // Check if the discount is applicable
                    const discountFit = !(
                      state.discount &&
                      ((state.discount.attributes.minimum_spend &&
                        totalPrice < state.discount.attributes.minimum_spend) ||
                        (state.discount.attributes.maximum_spend &&
                          totalPrice > state.discount.attributes.maximum_spend))
                    );

                    return {
                      cartItems: [
                        ...state.cartItems,
                        {
                          product: product,
                          qty: quantity,
                          price: price,
                          size: product.selectedSize,
                          type: product.selectedType,
                          color: colorName,
                          gift_card: product.attributes.gift_card,
                        },
                      ],
                      totalItems: state.totalItems + quantity,
                      totalPrice,
                      discount: discountFit ? state.discount : undefined,
                    };
                  }
                }
              }
              // Check if the size exists and if the quantity is sufficient or unlimited
              if (
                (typeof global_quantity === undefined ||
                  global_quantity === null) &&
                size &&
                (size.quantity > 0 || size.quantity === -1)
              ) {
                // Check if the requested quantity does not exceed the available quantity
                if (size.quantity >= quantity || size.quantity === -1) {
                  const price = isOnSale(
                    product.attributes.date_on_sale_from,
                    product.attributes.date_on_sale_to
                  )
                    ? product.attributes.sale_price ?? product.attributes.price
                    : product.attributes.price;

                  const colorName = product.attributes.colors?.find(
                    (c) => c.product.data.id === product.id
                  )?.name;

                  const totalPrice = Number(
                    (state.totalPrice + price * quantity).toFixed(2)
                  );

                  // Check if the discount is applicable
                  const discountFit = !(
                    state.discount &&
                    ((state.discount.attributes.minimum_spend &&
                      totalPrice < state.discount.attributes.minimum_spend) ||
                      (state.discount.attributes.maximum_spend &&
                        totalPrice > state.discount.attributes.maximum_spend))
                  );

                  return {
                    cartItems: [
                      ...state.cartItems,
                      {
                        product: product,
                        qty: quantity,
                        price: price,
                        size: product.selectedSize,
                        type: product.selectedType,
                        color: colorName,
                        gift_card: product.attributes.gift_card,
                      },
                    ],
                    totalItems: state.totalItems + quantity,
                    totalPrice,
                    discount: discountFit ? state.discount : undefined,
                  };
                }
              }
            }
          } else {
            // Find the corresponding size in the product attributes
            const size = product.attributes.sizes.find(
              (pSize) => pSize.size === item.product.selectedSize
            );

            // Check if global quantity exists and has enough quantity and if item qty + quantity is less than or equal to global quantity
            if (
              (typeof global_quantity !== undefined ||
                global_quantity !== null) &&
              global_quantity > 0 &&
              global_quantity >= item.qty + quantity
            ) {
              // Check if the quantity is sufficient or unlimited
              if (size && (size.quantity > 0 || size.quantity === -1)) {
                if (
                  size.quantity >= item.qty + quantity ||
                  size.quantity === -1
                ) {
                  const updatedCart = state.cartItems.map((el) =>
                    el.product.id === product.id &&
                    el.product.selectedSize === product.selectedSize &&
                    (product.selectedType
                      ? el.product.selectedType === product.selectedType
                      : true)
                      ? { ...el, qty: el.qty + 1 }
                      : el
                  );

                  const totalPrice = toFixedNumber(
                    state.totalPrice + item.price * quantity,
                    2
                  );

                  // Check if the discount is applicable
                  const discountFit = !(
                    state.discount &&
                    ((state.discount.attributes.minimum_spend &&
                      totalPrice < state.discount.attributes.minimum_spend) ||
                      (state.discount.attributes.maximum_spend &&
                        totalPrice > state.discount.attributes.maximum_spend))
                  );

                  return {
                    cartItems: updatedCart,
                    totalItems: state.totalItems + quantity,
                    totalPrice,
                    discount: discountFit ? state.discount : undefined,
                  };
                }
              }
            }

            // Check if the quantity is sufficient or unlimited
            if (
              (typeof global_quantity === undefined ||
                global_quantity === null) &&
              size &&
              (size.quantity > 0 || size.quantity === -1)
            ) {
              if (
                size.quantity >= item.qty + quantity ||
                size.quantity === -1
              ) {
                const updatedCart = state.cartItems.map((el) =>
                  el.product.id === product.id &&
                  el.product.selectedSize === product.selectedSize &&
                  (product.selectedType
                    ? el.product.selectedType === product.selectedType
                    : true)
                    ? { ...el, qty: el.qty + 1 }
                    : el
                );

                const totalPrice = toFixedNumber(
                  state.totalPrice + item.price * quantity,
                  2
                );

                // Check if the discount is applicable
                const discountFit = !(
                  state.discount &&
                  ((state.discount.attributes.minimum_spend &&
                    totalPrice < state.discount.attributes.minimum_spend) ||
                    (state.discount.attributes.maximum_spend &&
                      totalPrice > state.discount.attributes.maximum_spend))
                );

                return {
                  cartItems: updatedCart,
                  totalItems: state.totalItems + quantity,
                  totalPrice,
                  discount: discountFit ? state.discount : undefined,
                };
              }
            }

            return {};
          }
        }),
      decrement: (product, quantity = 1) =>
        set((state) => {
          const item = state.cartItems.find(
            (el) =>
              el.product.id === product.id &&
              el.product.selectedSize === product.selectedSize &&
              (product.selectedType
                ? el.product.selectedType === product.selectedType
                : true)
          );

          if (!item) {
            return {};
          }

          const updatedCart = state.cartItems
            .map((el) =>
              el.product.id === product.id &&
              el.product.selectedSize === product.selectedSize &&
              (product.selectedType
                ? el.product.selectedType === product.selectedType
                : true)
                ? { ...el, qty: el.qty - quantity }
                : el
            )
            .filter((el) => el.qty > 0);

          const totalItems =
            state.totalItems - quantity > 0 ? state.totalItems - quantity : 0;
          const totalPrice = toFixedNumber(
            state.totalPrice - (item?.price * quantity ?? 0) > 0
              ? state.totalPrice - (item?.price * quantity ?? 0)
              : 0,
            2
          );

          // Check if the discount is applicable
          const discountFit = !(
            state.discount &&
            ((state.discount.attributes.minimum_spend &&
              totalPrice < state.discount.attributes.minimum_spend) ||
              (state.discount.attributes.maximum_spend &&
                totalPrice > state.discount.attributes.maximum_spend))
          );

          return {
            cartItems: updatedCart,
            totalItems: totalItems,
            totalPrice,
            discount: discountFit ? state.discount : undefined,
          };
        }),
      emptyCart: () => set(initialState),
      refreshCart: async () => {
        let updatedCart = await Promise.all(
          get().cartItems.map(async (item) => {
            const { data } = await QueryProduct(item.product.id);

            const price = isOnSale(
              data.attributes.date_on_sale_from,
              data.attributes.date_on_sale_to
            )
              ? data.attributes.sale_price ?? data.attributes.price
              : data.attributes.price;

            const size = data.attributes.sizes.find(
              (el) => el.size === item.size
            );

            if (size && (size.quantity > 0 || size.quantity === -1)) {
              if (size.quantity >= item.qty || size.quantity === -1) {
                return {
                  ...item,
                  product: {
                    ...data,
                    selectedSize: item.size,
                    selectedType: item.type,
                  },
                  price: price,
                };
              }
            }
          })
        );
        updatedCart = updatedCart.filter(Boolean);

        const totalItems = get().cartItems.length;
        const totalPrice = toFixedNumber(
          updatedCart.reduce(
            (total, item) => total + item?.price * item?.qty,
            0
          ),
          2
        );
        set({
          cartItems: updatedCart,
          totalItems: totalItems,
          totalPrice: totalPrice,
        });
      },
      setOrderId: (orderId) => set({ orderId }),
      setOrderIntentId: (orderIntentId) => set({ orderIntentId }),
      setDiscount: (discount) => set({ discount }),
      setAddress: (address) => set({ address }),
      setStripeClientSecret: (stripeClientSecret: string) =>
        set({ stripeClientSecret }),
      setStripePaymentIntentId: (stripePaymentIntentId: string) =>
        set({ stripePaymentIntentId }),
    }),
    {
      name: 'cart',
      version: 1,
      partialize: (state) => ({
        cartItems: state.cartItems.map((item) => ({
          product: { id: item.product.id },
          size: item.size,
          type: item.type,
          qty: item.qty,
          price: item.price,
          color: item.color,
          gift_card: item.gift_card,
        })),
      }),
      onRehydrateStorage: () => (state) => {
        state.refreshCart();
      },
    }
  )
);
