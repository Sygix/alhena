import { Metadata } from 'next';
import { Icon } from 'next/dist/lib/metadata/types/metadata-types';
import { Inter, Noto_Sans_Display } from 'next/font/google';
import Script from 'next/script';

import '@/assets/styles/globals.css';

import {
  Queryi18NLocales,
  QuerySettings,
  QueryStaticTexts,
} from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';
import { PAYMENT_PROVIDER } from '@/lib/interfaces';
import { seo } from '@/lib/seo';

import Toasts from '@/components/elements/toaster/Toasts';
import Announce from '@/components/layout/Announce';
import Footer from '@/components/layout/Footer';
import Header from '@/components/layout/Header';
import UmamiEventTracker from '@/components/UmamiEventTracker';
import { ZustandProvider } from '@/components/ZustandProvider';

import { useServer } from '@/store/serverStore';

const noto_sans_display = Noto_Sans_Display({
  subsets: ['latin'],
  variable: '--font-noto-sans-display',
  display: 'swap',
});

const inter = Inter({
  subsets: ['latin'],
  variable: '--font-inter',
  display: 'swap',
});

export async function generateMetadata({
  params: { lang },
}: {
  params: { lang: string };
}): Promise<Metadata> {
  const {
    favicons,
    seo: defaultSeo,
    payment_provider,
    categories_page_size,
  } = await QuerySettings(lang);
  const iconList: Icon[] = [];
  useServer.setState({
    paymentProvider: payment_provider as PAYMENT_PROVIDER,
    categories_page_size,
  });
  favicons.data.forEach((icon) => {
    return iconList.push(MediaUrl(icon.attributes.url));
  });

  const metadata = seo({
    ...defaultSeo,
    lang: lang,
    icons: iconList,
  });
  return metadata;
}

export default async function BaseLayout({
  children,
  params: { lang },
}: {
  children: React.ReactNode;
  params: { lang: string };
}) {
  const { umami_website_id, umami_script_link, cart_page } =
    await QuerySettings(lang);
  const { translations } = await QueryStaticTexts(lang);
  const { i18NLocales } = await Queryi18NLocales();
  useServer.setState({
    locale: lang,
    locales: i18NLocales.data,
    translations: translations,
    cart_page: `/${cart_page.data?.attributes.slug}`,
  });

  return (
    <html
      lang={lang ?? 'fr'}
      className={`${noto_sans_display.variable} ${inter.variable}`}
    >
      {umami_script_link && umami_website_id && (
        <Script src={umami_script_link} data-website-id={umami_website_id} />
      )}
      <body className='flex min-h-screen flex-col text-carbon-900'>
        <UmamiEventTracker />
        <ZustandProvider serverState={useServer.getState()} />
        <Announce />
        <Header />
        <Toasts />
        <main className='flex flex-auto flex-col items-center'>{children}</main>
        <Footer />
      </body>
    </html>
  );
}
