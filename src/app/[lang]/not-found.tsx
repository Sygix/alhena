import DynamicIcon from '@/components/elements/DynamicIcon';
import ArrowLink from '@/components/elements/links/ArrowLink';

import { useServer } from '@/store/serverStore';

export default function NotFound({
  lang,
  slug,
}: {
  slug: string[];
  lang: string;
}) {
  const translations = useServer.getState().translations;

  return (
    <main className='flex flex-1 flex-col'>
      <div className='layout flex flex-1 flex-col items-center justify-center text-center text-black'>
        <DynamicIcon
          icon='ri:alarm-warning-fill'
          className='drop-shadow-glow h-32 w-32 animate-flicker text-red-500 md:h-60 md:w-60'
          wrapperClassName='w-32 h-32 md:w-60 md:h-60 '
        />
        <h1 className='mt-8 text-4xl md:text-6xl'>
          {translations.page_not_found} - {slug}
        </h1>
        {lang && (
          <ArrowLink
            className='mt-4 md:text-lg'
            title={translations.back_to_home_btn}
            href={`/${lang ?? ''}`}
          >
            {translations.back_to_home_btn}
          </ArrowLink>
        )}
      </div>
    </main>
  );
}
