import { QueryPageFromSlug } from '@/lib/graphql';

import Sections from '@/components/sections';

import notFound from '@/app/[lang]/not-found';

export default async function Page({
  params: { lang, slug },
  searchParams,
}: {
  params: { slug: string[]; lang: string };
  searchParams: { [key: string]: string | string[] | undefined };
}) {
  const { data } = await QueryPageFromSlug(lang, slug);

  if (data.length <= 0) return notFound({ lang, slug });

  const pageID = data[0].id;
  const { content } = data[0].attributes;

  return (
    <>
      <Sections
        sections={content}
        pageID={pageID}
        searchParams={searchParams}
      />
    </>
  );
}
