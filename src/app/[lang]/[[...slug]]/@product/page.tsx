import NextLink from 'next/link';
import { notFound } from 'next/navigation';

import clsxm from '@/lib/clsxm';
import { QueryProductFromSlug } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';

import AddToCart from '@/components/elements/cart/AddToCart';
import SizeSelect from '@/components/elements/cart/SizeSelect';
import TypeSelect from '@/components/elements/cart/TypeSelect';
import ButtonLink from '@/components/elements/links/ButtonLink';
import Tabs from '@/components/elements/tabs/Tabs';
import TabsContent from '@/components/elements/tabs/TabsContent';
import TabsNav from '@/components/elements/tabs/TabsNav';
import RemoteMDX from '@/components/elements/texts/RemoteMDX';
import NextImage from '@/components/NextImage';
import Sections from '@/components/sections';
import ProductPageCarousel from '@/components/sections/product/ProductPageCarousel';

import { useServer } from '@/store/serverStore';

const ProductPage = async ({
  params: { lang, slug },
}: {
  params: { slug: string[]; lang: string };
}) => {
  const { data } = await QueryProductFromSlug(lang, slug);
  const translations = useServer.getState().translations;

  if (data.length <= 0) return notFound();

  const {
    medias,
    title,
    slug: productSlug,
    categories,
    description_tabs,
    colors,
    sizes,
    types,
    content,
    gift_card,
    global_quantity,
  } = data[0].attributes;

  return (
    <>
      <div className='flex flex-1 flex-col items-center justify-between pb-3 md:p-6 lg:px-12'>
        <div className='flex flex-col md:flex-row'>
          <div className='flex flex-col gap-3 md:mr-3 md:w-1/2 md:shrink-0 lg:mr-6 lg:gap-6'>
            <ProductPageCarousel medias={medias} />
          </div>

          <div className='p-3 pt-6 md:w-1/2 md:py-0'>
            {categories && (
              <h2 className='h3 text-carbon-700'>
                {categories.data.map((cat, index) => (
                  <span key={`cat-${cat.attributes.slug}-${index}`}>
                    <NextLink
                      href={includeLocaleLink(`/${cat.attributes.slug}`)}
                      title={cat.attributes.title}
                    >
                      {cat.attributes.title}
                    </NextLink>
                    {index < categories.data.length - 1 && ' - '}
                  </span>
                ))}
              </h2>
            )}
            <h1 className='h2'>{title}</h1>

            {description_tabs && (
              <Tabs productSlug={productSlug}>
                <TabsNav
                  className='mt-6 flex w-full justify-center'
                  ulClassName='w-full flex divide-x divide-carbon-900'
                  liClassName='w-full text-center flex items-center first-of-type:pl-0 px-1 xs:px-3 last-of-type:pr-0 hover:cursor-pointer'
                >
                  {description_tabs.map((tab, index) => (
                    <h4
                      key={'nav' + index}
                      className='line-clamp-2 w-full text-sm md:text-base'
                    >
                      {tab.title}
                    </h4>
                  ))}
                </TabsNav>
                <TabsContent>
                  {description_tabs.map((tab, index) => (
                    <section
                      key={'content' + index}
                      className='prose prose-carbon mt-3 flex flex-col gap-3 md:prose-md prose-a:underline md:gap-6'
                    >
                      <RemoteMDX source={tab.content} />
                    </section>
                  ))}
                </TabsContent>
              </Tabs>
            )}

            <div className='mt-6 flex flex-col gap-3 md:flex-row md:items-center'>
              <h5 className='shrink-0 text-base font-bold text-carbon-900 first-letter:uppercase'>
                {gift_card
                  ? translations.choose_gift_card_value ?? 'Choose value '
                  : translations.choose_color ?? 'Choose your color '}
                :{' '}
              </h5>
              <ul className='flex flex-wrap items-center gap-2'>
                {colors &&
                  colors.map((color, index) => (
                    <li
                      key={`color-${color.name}-${index}`}
                      className='flex flex-col items-center justify-center gap-1 xs:flex-row md:flex-col lg:flex-row'
                    >
                      <ButtonLink
                        href={includeLocaleLink(
                          `/${color.product.data.attributes.slug}`
                        )}
                        title={color.name}
                        className={clsxm(
                          'aspect-square w-8 overflow-hidden rounded-full border-2 border-carbon-400 p-2',
                          color.image.data && 'p-0',
                          gift_card && 'w-fit',
                          color.product.data.attributes.slug === productSlug &&
                            'border-carbon-800'
                        )}
                        data-analytics-event='product-select-color'
                        data-analytics-event-product={slug}
                        data-analytics-event-color={color.name}
                        style={{
                          backgroundColor: `${!gift_card && color.color}`,
                        }}
                      >
                        {!gift_card && color.image.data && (
                          <NextImage
                            className='relative aspect-square w-full overflow-hidden'
                            imgClassName='z-10 w-full h-full object-center object-cover rounded-md'
                            width={color.image.data.attributes.width}
                            height={color.image.data.attributes.height}
                            src={MediaUrl(color.image.data.attributes.url)}
                            title={color.name}
                            alt={
                              color.image.data.attributes.alternativeText ?? ''
                            }
                            quality={50}
                            sizes='5vw'
                          />
                        )}
                        {gift_card && color.name.replace('_', '')}
                      </ButtonLink>
                      {!gift_card && (
                        <p
                          className={clsxm(
                            'text-base font-semibold text-carbon-400',
                            color.product.data.attributes.slug ===
                              productSlug && 'text-carbon-800'
                          )}
                        >
                          {color.name.replace('_', '')}
                        </p>
                      )}
                    </li>
                  ))}
              </ul>
            </div>

            <div className='mt-3 flex flex-col gap-3'>
              <AddToCart
                product={data[0]}
                containerClassName='mt-3 flex gap-3 items-center justify-between xs:justify-start font-bold text-base md:text-lg lg:text-xl'
                btnClassName='w-full xs:w-fit xs:px-6 md:px-12 justify-center font-bold text-base lg:text-lg'
                hideContinueShopping={false}
              >
                {!gift_card && (
                  <>
                    <div className='flex flex-col gap-3 md:flex-row md:items-center'>
                      <h5 className='shrink-0 text-base font-bold text-carbon-900 first-letter:uppercase'>
                        {translations.choose_size ?? 'Choose your size'}:{' '}
                      </h5>
                      <div className='flex flex-wrap gap-2'>
                        {sizes.map((size, index) => (
                          <SizeSelect
                            key={`size-${size.size}-${index}`}
                            disabled={
                              global_quantity === 0 ? true : size.quantity === 0
                            }
                            size={size}
                            className='font-semibold'
                          >
                            {size.size.replace('_', '')}
                          </SizeSelect>
                        ))}
                      </div>
                    </div>

                    {types.length >= 1 && (
                      <div className='flex flex-col gap-3 md:flex-row md:items-center'>
                        <h5 className='shrink-0 text-base font-bold text-carbon-900 first-letter:uppercase'>
                          {/* !To fill in the translation */}
                          {translations.choose_type ?? 'Choose your type'}:{' '}
                        </h5>
                        <div className='flex flex-wrap gap-2'>
                          {types.map((type, index) => (
                            <TypeSelect
                              key={`type-${type.name}-${index}`}
                              type={type}
                              className='font-semibold'
                            >
                              {type.name.replace('_', '')}
                            </TypeSelect>
                          ))}
                        </div>
                      </div>
                    )}
                  </>
                )}
              </AddToCart>
            </div>
          </div>
        </div>
      </div>
      <Sections
        sections={content as unknown as []}
        pageID={data[0].id}
        pageType='product'
      />
    </>
  );
};

export default ProductPage;
