import { notFound } from 'next/navigation';

import { QueryCategoryFromSlug } from '@/lib/graphql';

import Sections from '@/components/sections';

const CategoryPage = async ({
  params: { lang, slug },
  searchParams,
}: {
  params: { slug: string[]; lang: string };
  searchParams: { [key: string]: string | string[] | undefined };
}) => {
  const { data } = await QueryCategoryFromSlug(lang, slug);

  if (data.length <= 0) notFound();

  const { title, description, content } = data[0].attributes;

  return (
    <>
      <div className='flex max-w-3xl flex-col items-center gap-3 p-3 md:gap-6 md:p-6 lg:px-12'>
        <h1 className='first-letter:uppercase'>{title}</h1>
        <p className='text-center'>{description}</p>
      </div>
      <Sections
        sections={content as unknown as []}
        pageID={data[0].id}
        pageType='category'
        searchParams={searchParams}
      />
    </>
  );
};

export default CategoryPage;
