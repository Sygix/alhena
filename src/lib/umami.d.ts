interface Window {
  umami?: {
    track(eventName: string, eventData?: unknown): void;
  };
}
