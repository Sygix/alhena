import { gql, GraphQLClient } from 'graphql-request';

import {
  Category,
  Discount,
  localeProps,
  Media,
  Menu,
  Order,
  Page,
  Product,
  ProductSize,
  QueryMetaProps,
  Redirection,
  Setting,
} from '@/lib/interfaces';

import { ContactFormType } from '@/components/elements/forms/ContactForm';

const API_URL = process.env.NEXT_PUBLIC_STRAPI_URL ?? 'http://localhost:1337';
const STRAPI_TOKEN = process.env.STRAPI_API_TOKEN;

export { gql } from 'graphql-request';

export const StrapiClient = new GraphQLClient(`${API_URL}/graphql` as string, {
  headers: {
    authorization: STRAPI_TOKEN ? `Bearer ${STRAPI_TOKEN}` : '',
  },
  fetch: fetch,
});

/**
 * Query settings from Strapi
 * @returns settings data
 */
export const QuerySettings = async (locale: string) => {
  const queryVariables = {
    locale: locale,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['setting'] } });

  const { setting } = await StrapiClient.request<{
    setting: {
      data: Setting;
    };
  }>(
    gql`
      query Settings($locale: I18NLocaleCode!) {
        setting(locale: $locale) {
          data {
            attributes {
              favicons {
                data {
                  attributes {
                    url
                  }
                }
              }
              seo {
                title
                siteName
                description
              }
              payment_provider
              categories_page_size
              umami_script_link
              umami_website_id
              cart_page {
                data {
                  attributes {
                    slug
                  }
                }
              }
            }
          }
        }
      }
    `,
    queryVariables
  );

  return setting.data.attributes;
};

/**
 * Query static texts from Strapi
 * @returns translations of static text json format
 */
export const QueryStaticTexts = async (locale: string) => {
  const queryVariables = {
    locale: locale,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['static-text'] } });

  const { staticText } = await StrapiClient.request<{
    staticText: {
      data: {
        attributes: {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          translations: any;
        };
      };
    };
  }>(
    gql`
      query Settings($locale: I18NLocaleCode!) {
        staticText(locale: $locale) {
          data {
            attributes {
              translations
            }
          }
        }
      }
    `,
    queryVariables
  );

  return staticText.data.attributes;
};

/**
 * Query page, product or category id from slug
 * @param locale locale language
 * @param slug array of slugs
 * @returns id and slug
 */
export const QueryIdFromSlug = async (
  locale: string,
  slug: string[] | undefined
) => {
  const joinedSlug = !slug
    ? '/'
    : slug instanceof Array
    ? slug.join('/')
    : Array.of(slug).join('/');

  const queryVariables = {
    locale: locale,
    joinedSlug: joinedSlug,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, {
      ...options,
      next: { tags: ['page', 'product', 'category'] },
    });

  const data = await StrapiClient.request<{
    pages: {
      data: Page[];
    };
    products: {
      data: Product[];
    };
    categories: {
      data: Category[];
    };
  }>(
    gql`
      query idFromSlug($locale: I18NLocaleCode!, $joinedSlug: String!) {
        pages(
          filters: { slug: { eq: $joinedSlug } }
          locale: $locale
          pagination: { limit: 1 }
        ) {
          data {
            id
            attributes {
              slug
            }
          }
        }

        products(
          filters: { slug: { eq: $joinedSlug } }
          locale: $locale
          pagination: { limit: 1 }
        ) {
          data {
            id
            attributes {
              slug
            }
          }
        }

        categories(
          filters: { slug: { eq: $joinedSlug } }
          locale: $locale
          pagination: { limit: 1 }
        ) {
          data {
            id
            attributes {
              slug
            }
          }
        }
      }
    `,
    queryVariables
  );

  return data;
};

type graphQLPathsProps = {
  pages: {
    data: Page[];
    meta: QueryMetaProps;
  };
  products: {
    data: Product[];
    meta: QueryMetaProps;
  };
  categories: {
    data: Category[];
    meta: QueryMetaProps;
  };
};

/**
 * Query all paths from Strapi
 * @returns list of paths including languages
 */
export const QueryAllPaths = async (page?: number, pageSize = 50) => {
  const queryVariables = {
    page,
    pageSize,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, {
      ...options,
      next: { tags: ['page', 'product', 'category', 'media'] },
    });

  const data = await StrapiClient.request<graphQLPathsProps>(
    gql`
      query Paths($page: Int, $pageSize: Int) {
        pages(
          publicationState: LIVE
          locale: "all"
          pagination: { page: $page, pageSize: $pageSize }
        ) {
          data {
            id
            attributes {
              locale
              slug
              updatedAt
            }
          }
          meta {
            pagination {
              page
              pageCount
            }
          }
        }

        products(
          publicationState: LIVE
          locale: "all"
          pagination: { page: $page, pageSize: $pageSize }
        ) {
          data {
            id
            attributes {
              locale
              slug
              updatedAt
            }
          }
          meta {
            pagination {
              page
              pageCount
            }
          }
        }

        categories(
          locale: "all"
          pagination: { page: $page, pageSize: $pageSize }
        ) {
          data {
            id
            attributes {
              locale
              slug
              updatedAt
            }
          }
          meta {
            pagination {
              page
              pageCount
            }
          }
        }
      }
    `,
    queryVariables
  );

  return data;
};

/**
 * Query seo from Strapi
 * @param locale language of the requested page
 * @param slug array of slugs
 * @returns seo data
 */
export const QuerySeo = async (locale: string, slug: string[] | undefined) => {
  const joinedSlug = !slug
    ? '/'
    : slug instanceof Array
    ? slug.join('/')
    : Array.of(slug).join('/');

  const queryVariables = {
    locale: locale,
    joinedSlug: joinedSlug,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, {
      ...options,
      next: { tags: ['page', 'product', 'category'] },
    });

  const data = await StrapiClient.request<{
    pages: {
      data: Page[];
    };
    products: {
      data: Product[];
    };
    categories: {
      data: Category[];
    };
  }>(
    gql`
      query Seo($locale: I18NLocaleCode!, $joinedSlug: String!) {
        pages(
          filters: { slug: { eq: $joinedSlug } }
          locale: $locale
          pagination: { limit: 1 }
        ) {
          data {
            attributes {
              slug
              metadata {
                template_title
                title_suffix
                meta_description
              }
              updatedAt
              localizations {
                data {
                  attributes {
                    slug
                    locale
                  }
                }
              }
            }
          }
        }

        products(
          filters: { slug: { eq: $joinedSlug } }
          locale: $locale
          pagination: { limit: 1 }
        ) {
          data {
            attributes {
              slug
              metadata {
                template_title
                title_suffix
                meta_description
              }
              updatedAt
              localizations {
                data {
                  attributes {
                    slug
                    locale
                  }
                }
              }
            }
          }
        }

        categories(
          filters: { slug: { eq: $joinedSlug } }
          locale: $locale
          pagination: { limit: 1 }
        ) {
          data {
            attributes {
              slug
              metadata {
                template_title
                title_suffix
                meta_description
              }
              updatedAt
              localizations {
                data {
                  attributes {
                    slug
                    locale
                  }
                }
              }
            }
          }
        }
      }
    `,
    queryVariables
  );

  return data;
};

type graphQLPageProps = {
  pages: {
    data: [
      {
        id: number;
        attributes: {
          slug: string;
          content: [];
        };
      }
    ];
  };
};

/**
 * Query a single page from Strapi
 * @param locale language of the requested page
 * @param slug array of slugs
 * @returns data of a page with direct content
 */
export const QueryPageFromSlug = async (
  locale: string,
  slug: string[] | undefined
) => {
  const joinedSlug = !slug
    ? '/'
    : slug instanceof Array
    ? slug.join('/')
    : Array.of(slug).join('/');

  const queryVariables = {
    locale: locale,
    joinedSlug: joinedSlug,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['page'] } });

  const { pages } = await StrapiClient.request<graphQLPageProps>(
    gql`
      query Pages($locale: I18NLocaleCode!, $joinedSlug: String!) {
        pages(
          filters: { slug: { eq: $joinedSlug } }
          locale: $locale
          pagination: { limit: 1 }
        ) {
          data {
            id
            attributes {
              slug

              content {
                __typename
              }
            }
          }
        }
      }
    `,
    queryVariables
  );

  return pages;
};

/**
 * Query a single product from Strapi
 * @param id id of the product
 * @param disableCaching disable fetch caching
 * @returns data of a product
 */
export const QueryProduct = async (id: number, disableCaching = false) => {
  const queryVariables = {
    id: id,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, {
      ...options,
      next: { tags: ['product'] },
      cache: `${disableCaching ? 'no-store' : 'default'}`,
    });

  const { product } = await StrapiClient.request<{
    product: { data: Product };
  }>(
    gql`
      query Product($id: ID!) {
        product(id: $id) {
          data {
            id
            attributes {
              title
              slug
              price
              global_quantity
              new_product_tag
              sale_price
              date_on_sale_from
              date_on_sale_to
              medias {
                data {
                  attributes {
                    alternativeText
                    name
                    url
                    width
                    height
                    mime
                  }
                }
              }
              short_description
              sizes {
                id
                size
                quantity
              }
              colors {
                name
                color
                image {
                  data {
                    attributes {
                      alternativeText
                      name
                      url
                      width
                      height
                      mime
                    }
                  }
                }
                product {
                  data {
                    id
                    attributes {
                      slug
                    }
                  }
                }
              }
              types {
                name
              }
              categories {
                data {
                  attributes {
                    title
                    slug
                  }
                }
              }
              gift_card
            }
          }
        }
      }
    `,
    queryVariables
  );

  return product;
};

/**
 * Query a single product from Strapi
 * @param locale language of the requested page
 * @param slug slug of the product
 * @returns data of a product
 */
export const QueryProductFromSlug = async (
  locale: string,
  slug: string[] | undefined
) => {
  const joinedSlug = !slug
    ? '/'
    : slug instanceof Array
    ? slug.join('/')
    : Array.of(slug).join('/');

  const queryVariables = {
    locale: locale,
    joinedSlug: joinedSlug,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['product'] } });

  const { products } = await StrapiClient.request<{
    products: { data: Product[] };
  }>(
    gql`
      query QueryProductFromSlug(
        $locale: I18NLocaleCode!
        $joinedSlug: String!
      ) {
        products(
          filters: { slug: { eq: $joinedSlug } }
          locale: $locale
          pagination: { limit: 1 }
        ) {
          data {
            id
            attributes {
              title
              slug
              price
              global_quantity
              new_product_tag
              sale_price
              date_on_sale_from
              date_on_sale_to
              medias {
                data {
                  attributes {
                    alternativeText
                    name
                    url
                    width
                    height
                    mime
                  }
                }
              }
              short_description
              description_tabs {
                title
                content
              }
              sizes {
                id
                size
                quantity
              }
              colors {
                name
                color
                image {
                  data {
                    attributes {
                      alternativeText
                      name
                      url
                      width
                      height
                      mime
                    }
                  }
                }
                product {
                  data {
                    id
                    attributes {
                      slug
                    }
                  }
                }
              }
              types {
                name
              }
              categories {
                data {
                  attributes {
                    title
                    slug
                  }
                }
              }
              content {
                __typename
              }
              gift_card
            }
          }
        }
      }
    `,
    queryVariables
  );

  return products;
};

/**
 * Query a products
 * @param locale locale of the product
 * @param page number of page to query
 * @param pageSize page size to query
 * @param dateNow current Date
 * @param category id of the category to query
 * @param priceFrom minimum price to query
 * @param priceTo maximum price to query
 * @param size size string
 * @param color color string
 * @returns multiple products
 */
export const QueryProducts = async (
  locale: string,
  page: number,
  pageSize: number,
  dateNow: string,
  category?: (string | number)[],
  priceFrom?: number,
  priceTo?: number,
  size?: string[],
  color?: string[],
  sort?: 'price:asc' | 'price:desc' | 'title:asc' | 'title:desc'
) => {
  const queryVariables = {
    locale,
    page,
    pageSize,
    dateNow,
    ...(category !== undefined ? { category } : {}),
    ...(priceFrom !== undefined && !isNaN(priceFrom)
      ? { priceFrom }
      : { priceFrom: 0 }),
    ...(priceTo !== undefined && !isNaN(priceTo) ? { priceTo } : {}),
    ...(size !== undefined ? { size } : {}),
    ...(color !== undefined ? { color } : {}),
    ...(sort !== undefined ? { sort } : { sort: 'title:asc' }),
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, {
      ...options,
      next: { tags: ['product'] },
    });

  const { products } = await StrapiClient.request<{
    products: { data: Product[]; meta: QueryMetaProps };
  }>(
    gql`
      query QueryProducts(
        $locale: I18NLocaleCode!
        $page: Int!
        $pageSize: Int!
        $category: [ID]
        $size: [String]
        $color: [String]
        $priceFrom: Float!
        $priceTo: Float
        $dateNow: DateTime!
        $sort: [String]
      ) {
        products(
          locale: $locale
          publicationState: LIVE
          sort: $sort
          pagination: { page: $page, pageSize: $pageSize }
          filters: {
            and: {
              categories: { id: { in: $category } }
              sizes: {
                or: [{ quantity: { gte: 1 } }, { quantity: { eq: -1 } }]
                and: { size: { in: $size }, quantity: { ne: 0 } }
              }
              colors: { name: { in: $color } }
              or: [
                {
                  and: [
                    { price: { gte: $priceFrom } }
                    { price: { lte: $priceTo } }
                  ]
                }
                {
                  and: {
                    or: [
                      { sale_price: { gte: $priceFrom } }
                      { sale_price: { lte: $priceTo } }
                    ]
                    date_on_sale_from: { lte: $dateNow }
                    date_on_sale_to: { gte: $dateNow }
                  }
                }
              ]
            }
          }
        ) {
          data {
            id
            attributes {
              title
              slug
              price
              new_product_tag
              sale_price
              date_on_sale_from
              date_on_sale_to
              medias {
                data {
                  attributes {
                    alternativeText
                    name
                    url
                    width
                    height
                    mime
                  }
                }
              }
              short_description
              sizes {
                id
                size
                quantity
              }
              colors {
                name
                color
                image {
                  data {
                    attributes {
                      alternativeText
                      name
                      url
                      width
                      height
                      mime
                    }
                  }
                }
                product {
                  data {
                    id
                    attributes {
                      slug
                    }
                  }
                }
              }
              categories {
                data {
                  id
                }
              }
              gift_card
            }
          }
          meta {
            pagination {
              page
              pageCount
            }
          }
        }
      }
    `,
    queryVariables
  );

  return products;
};

/**
 * Query a single category from Strapi
 * @param locale language of the requested page
 * @param slug slug of the category
 * @returns data of a category
 */
export const QueryCategoryFromSlug = async (
  locale: string,
  slug: string[] | undefined
) => {
  const joinedSlug = !slug
    ? '/'
    : slug instanceof Array
    ? slug.join('/')
    : Array.of(slug).join('/');

  const queryVariables = {
    locale: locale,
    joinedSlug: joinedSlug,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['category'] } });

  const { categories } = await StrapiClient.request<{
    categories: { data: Category[] };
  }>(
    gql`
      query QueryCategoryFromSlug(
        $locale: I18NLocaleCode!
        $joinedSlug: String!
      ) {
        categories(
          filters: { slug: { eq: $joinedSlug } }
          locale: $locale
          pagination: { limit: 1 }
        ) {
          data {
            id
            attributes {
              title
              slug
              description
              short_description
              image {
                data {
                  attributes {
                    name
                    alternativeText
                    width
                    height
                    mime
                    url
                  }
                }
              }
              content {
                __typename
              }
            }
          }
        }
      }
    `,
    queryVariables
  );

  return categories;
};

/**
 * Query menus from Strapi
 * @param locale language of the requested page
 * @returns the menus objects
 */
export const QueryMenus = async (locale: string) => {
  const queryVariables = {
    locale: locale,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['menu'] } });

  const { menu } = await StrapiClient.request<{
    menu: { data: Menu };
  }>(
    gql`
      query QueryMenus {
        menu {
          data {
            attributes {
              announce {
                text
              }
              header {
                id
                logo {
                  data {
                    attributes {
                      alternativeText
                      width
                      height
                      url
                    }
                  }
                }
                logo_link
                items {
                  id
                  link {
                    id
                    name
                    href
                    icon
                    style
                    direction
                    variant
                  }
                  sublinks {
                    id
                    name
                    href
                    icon
                    style
                    direction
                    variant
                  }
                }
                cart_page {
                  data {
                    attributes {
                      slug
                    }
                  }
                }
              }

              footer {
                id
                columns {
                  id
                  title
                  description
                  socials {
                    id
                    name
                    href
                    icon
                    style
                    direction
                    variant
                  }
                  links {
                    id
                    name
                    href
                    icon
                    style
                    direction
                    variant
                  }
                  newsletter {
                    id
                    placeholder
                  }
                }
                copyright
              }
            }
          }
        }
      }
    `,
    queryVariables
  );

  return menu;
};

/**
 * Query content of a specific component on a page from Strapi
 * @param locale language of the requested page
 * @param id id of the page or other in wich the section is
 * @param type type of the query (page, category, product)
 * @param tags tags for the cached query (page, category, product)
 * @param fragment pass the fragment of the component
 * @param fragmentSpread the query spread of the fragment without ...
 * @returns data of products
 */
export const QueryContentComponent = async (
  locale: string,
  id: number,
  type: string,
  tags: string[],
  fragment: string,
  fragmentSpread: string
) => {
  const queryVariables = {
    locale: locale,
    id: id,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: [...tags] } });

  const response =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    await StrapiClient.request<any>(
      gql`
        query QueryContentComponent($id: ID!, $locale: I18NLocaleCode!) {
          ${type}(id: $id, locale: $locale) {
            data {
              attributes {
                content {
                  ...${fragmentSpread}
                }
              }
            }
          }
        }
        ${fragment}
      `,
      queryVariables
    );

  return response[type];
};

/**
 * Query a single order from Strapi
 * @param payment_intent_id payment intent of stripe
 * @returns data of the order
 */
export const QueryOrderFromPaymentIntent = async (
  payment_intent_id: string
) => {
  const queryVariables = {
    payment_intent_id,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  const { orders } = await StrapiClient.request<{
    orders: { data: Order[] };
  }>(
    gql`
      query orderFromPaymentIntent($payment_intent_id: String!) {
        orders(filters: { payment_intent_id: { eq: $payment_intent_id } }) {
          data {
            id
            attributes {
              amount
              status
              products
              locale
            }
          }
        }
      }
    `,
    queryVariables
  );

  return orders;
};

/**
 * Query a single order from Strapi
 * @param id id of the order
 * @param order_intent_id order intent id in Strapi
 * @returns data as a list of the order
 */
export const QueryOrderWithIntent = async (
  id: string | number,
  order_intent_id: string
) => {
  const queryVariables = {
    id,
    order_intent_id,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  const { orders } = await StrapiClient.request<{
    orders: { data: Order[] };
  }>(
    gql`
      query orderWithIntent($id: ID!, $order_intent_id: String!) {
        orders(
          filters: {
            id: { eq: $id }
            order_intent_id: { eq: $order_intent_id }
          }
        ) {
          data {
            id
            attributes {
              amount
              subtotal
              products
              status
            }
          }
        }
      }
    `,
    queryVariables
  );

  return orders;
};

/**
 * Query a discount by id from Strapi
 * @param id discount id
 * @returns data of the discount code
 */
export const QueryDiscountById = async (id: number | string) => {
  const queryVariables = {
    id,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  const { discount } = await StrapiClient.request<{
    discount: { data: Discount };
  }>(
    gql`
      query discountFromId($id: ID!) {
        discount(id: $id) {
          data {
            id
            attributes {
              code
              type
              value
              value_left
              minimum_spend
              maximum_spend
              start_date
              expiration
              max_uses
              uses
            }
          }
        }
      }
    `,
    queryVariables
  );

  return discount;
};

/**
 * Query a discount by code from Strapi
 * @param code discount code
 * @returns data of the discount code
 */
export const QueryDiscountFromCode = async (code: string) => {
  const queryVariables = {
    code,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  const { discounts } = await StrapiClient.request<{
    discounts: { data: Discount[] };
  }>(
    gql`
      query discountFromCode($code: String!) {
        discounts(filters: { code: { eq: $code } }, publicationState: LIVE) {
          data {
            id
            attributes {
              code
              type
              value
              value_left
              minimum_spend
              maximum_spend
              start_date
              expiration
              max_uses
              uses
            }
          }
        }
      }
    `,
    queryVariables
  );

  return discounts;
};

/**
 * Query Media from slug
 * @param locale
 * @param slug slugs array
 * @returns media
 */
export const QueryUploadFileFromSrc = async (src: string) => {
  const queryVariables = {
    src,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['file'] } });

  const response = await StrapiClient.request<{
    uploadFiles: { data: Media[] };
  }>(
    gql`
      query uploadFileFromSrc($src: String!) {
        uploadFiles(filters: { url: { eq: $src } }) {
          data {
            id
            attributes {
              name
              url
              width
              height
              mime
              caption
              alternativeText
            }
          }
        }
      }
    `,
    queryVariables
  );

  return response;
};

/**
 * Create an order in Strapi and return Order details
 * @param input
 * @returns data of order
 */
export const MutationCreateOrder = async (input: unknown) => {
  const queryVariables = {
    input,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  const response = await StrapiClient.request<{ createOrder: { data: Order } }>(
    gql`
      mutation createOrder($input: OrderInput!) {
        createOrder(data: $input) {
          data {
            id
            attributes {
              payment_intent_id
              order_intent_id
              email
              billing_name
              billing_city
              billing_country
              billing_line1
              billing_line2
              billing_postal_code
              billing_state
              shipping_name
              shipping_city
              shipping_country
              shipping_line1
              shipping_line2
              shipping_postal_code
              shipping_state
              status
              amount
              subtotal
              products
              locale
              createdAt
              updatedAt
            }
          }
        }
      }
    `,
    queryVariables
  );

  return response;
};

/**
 * Update an order in Strapi and return Order details
 * @param id
 * @param input
 * @returns data of order
 */
export const MutationUpdateOrder = async (
  id: string | number,
  input: unknown
) => {
  const queryVariables = {
    id,
    input,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  const response = await StrapiClient.request<{ updateOrder: { data: Order } }>(
    gql`
      mutation updateOrder($id: ID!, $input: OrderInput!) {
        updateOrder(id: $id, data: $input) {
          data {
            id
            attributes {
              payment_intent_id
              order_intent_id
              email
              billing_name
              billing_city
              billing_country
              billing_line1
              billing_line2
              billing_postal_code
              billing_state
              shipping_name
              shipping_city
              shipping_country
              shipping_line1
              shipping_line2
              shipping_postal_code
              shipping_state
              status
              amount
              subtotal
              products
              createdAt
              updatedAt
            }
          }
        }
      }
    `,
    queryVariables
  );

  return response;
};

/**
 * Update an order in Strapi and return Order details
 * @param id
 * @param order_intent_id
 * @param input
 * @returns data of order
 */
export const MutationUpdateOrderByIntentId = async (
  id: string | number,
  order_intent_id: string,
  input: unknown
) => {
  const queryVariables = {
    id,
    order_intent_id,
    input,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  const response = await StrapiClient.request<{
    updateByOrderIntentId: { data: Order };
  }>(
    gql`
      mutation updateOrderByIntentId(
        $id: ID!
        $order_intent_id: String!
        $input: OrderInput!
      ) {
        updateByOrderIntentId(
          id: $id
          order_intent_id: $order_intent_id
          data: $input
        ) {
          data {
            id
            attributes {
              payment_intent_id
              order_intent_id
              email
              billing_name
              billing_city
              billing_country
              billing_line1
              billing_line2
              billing_postal_code
              billing_state
              shipping_name
              shipping_city
              shipping_country
              shipping_line1
              shipping_line2
              shipping_postal_code
              shipping_state
              status
              amount
              subtotal
              discount_id
              products
              createdAt
              updatedAt
            }
          }
        }
      }
    `,
    queryVariables
  );

  return response;
};

/**
 * Delete an order in Strapi and return Order details
 * @param id
 * @returns data of order
 */
export const MutationDeleteOrder = async (id: string) => {
  const queryVariables = {
    id,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  const response = await StrapiClient.request<{ deleteOrder: { data: Order } }>(
    gql`
      mutation deleteOrder($id: ID!) {
        deleteOrder(id: $id) {
          data {
            id
            attributes {
              payment_intent_id
              email
              billing_name
              billing_city
              billing_country
              billing_line1
              billing_line2
              billing_postal_code
              billing_state
              shipping_name
              shipping_city
              shipping_country
              shipping_line1
              shipping_line2
              shipping_postal_code
              shipping_state
              status
              amount
              subtotal
              products
              createdAt
              updatedAt
            }
          }
        }
      }
    `,
    queryVariables
  );

  return response;
};

/**
 * Update a product in Strapi
 * @param id
 * @param input product
 * @returns data of product
 */
export const MutationUpdateProduct = async (
  id: number,
  input: Partial<Product['attributes']>
) => {
  const queryVariables = {
    id,
    input,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  const response = await StrapiClient.request<{
    updateProduct: { data: Product };
  }>(
    gql`
      mutation updateProduct($id: ID!, $input: ProductInput!) {
        updateProduct(id: $id, data: $input) {
          data {
            id
            attributes {
              title
              slug
              price
              sale_price
              date_on_sale_from
              date_on_sale_to
              medias {
                data {
                  attributes {
                    alternativeText
                    name
                    url
                    width
                    height
                    mime
                  }
                }
              }
              short_description
              sizes {
                size
                quantity
              }
              colors {
                name
                color
                image {
                  data {
                    attributes {
                      alternativeText
                      name
                      url
                      width
                      height
                      mime
                    }
                  }
                }
                product {
                  data {
                    id
                    attributes {
                      slug
                    }
                  }
                }
              }
              categories {
                data {
                  attributes {
                    title
                    slug
                  }
                }
              }
            }
          }
        }
      }
    `,
    queryVariables
  );

  return response;
};

/**
 * Update many products in Strapi
 * @param ids
 * @param input [ProductsInput!]
 * @param locale
 * @returns data of the size
 */
export const MutationUpdateManyProducts = (
  ids: number[],
  input: Partial<Product['attributes']>[],
  locale: string
) => {
  const queryVariables = {
    ids,
    input,
    locale,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  return StrapiClient.request<{
    updateManyProducts: Product[];
  }>(
    gql`
      mutation updateManyProducts(
        $ids: [ID!]
        $input: [ProductInput!]
        $locale: I18NLocaleCode!
      ) {
        updateManyProducts(ids: $ids, datas: $input, locale: $locale) {
          id
          attributes {
            global_quantity
          }
        }
      }
    `,
    queryVariables
  );
};

/**
 * Update a product size in Strapi
 * @param id
 * @param input size
 * @returns data of the size
 */
export const MutationUpdateProductSize = (
  id: number,
  input: Partial<ProductSize>
) => {
  const queryVariables = {
    id,
    input,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  return StrapiClient.request<{
    updateProductSize: ProductSize;
  }>(
    gql`
        mutation updateProductSize($id: ID!, $input: ComponentProductsSizesInput!) {
          updateProductSize(id: $id, data: $input) {
            ${Object.keys(input).join('\n')}
          }
        }
      `,
    queryVariables
  );
};

/**
 * Update many product sizes in Strapi
 * @param ids
 * @param input size
 * @returns data of the size
 */
export const MutationUpdateManyProductSize = (
  ids: number[],
  input: Partial<ProductSize>[]
) => {
  const queryVariables = {
    ids,
    input,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  return StrapiClient.request<{
    updateManyProductSize: ProductSize[];
  }>(
    gql`
      mutation updateManyProductSize(
        $ids: [ID!]
        $input: [ComponentProductsSizesInput!]
      ) {
        updateManyProductSize(ids: $ids, datas: $input) {
          quantity
        }
      }
    `,
    queryVariables
  );
};

/**
 * Query locales from Strapi
 * @returns locales
 */
export const Queryi18NLocales = async () => {
  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['i18NLocales'] } });

  const response =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    await StrapiClient.request<localeProps>(
      gql`
        query i18NLocales {
          i18NLocales {
            data {
              attributes {
                code
                name
              }
            }
          }
        }
      `
    );

  return response;
};

/**
 * Request Strapi to send contact mail
 * @param input
 * @returns nothing
 */
export const MutationSendContactMail = async (
  locale: string,
  data: ContactFormType
) => {
  const queryVariables = {
    locale,
    data,
  };

  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, cache: 'no-store' });

  const response = await StrapiClient.request<{
    sendContactMail: { data: null };
  }>(
    gql`
      mutation sendContactMail(
        $locale: I18NLocaleCode!
        $data: sendContactMailDataType!
      ) {
        sendContactMail(locale: $locale, data: $data)
      }
    `,
    queryVariables
  );

  return response;
};

/**
 * Query all product sizes from Strapi
 * @returns sizes [String]!
 */
export const QuerySizes = async () => {
  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['product'] } });

  const response =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    await StrapiClient.request<{ sizes: [string] }>(
      gql`
        query QuerySizes {
          sizes
        }
      `
    );

  return response;
};

/**
 * Query all product sizes in a category from Strapi
 * @returns sizes [String]!
 */
export const QueryCategoryProductsSizes = async (
  categoryId: string | number
) => {
  const queryVariables = {
    categoryId,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['product'] } });

  const response =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    await StrapiClient.request<{ categoryProductsSizes: [string] }>(
      gql`
        query QueryCategoryProductsSizes($categoryId: ID!) {
          categoryProductsSizes(categoryId: $categoryId)
        }
      `,
      queryVariables
    );

  return response;
};

/**
 * Query all product colors from Strapi
 * @returns colors [String]!
 */
export const QueryColors = async () => {
  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['product'] } });

  const response =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    await StrapiClient.request<{ colors: [string] }>(
      gql`
        query QueryColors {
          colors
        }
      `
    );

  return response;
};

/**
 * Query all product colors in a category from Strapi
 * @returns colors [String]!
 */
export const QueryCategoryProductsColors = async (
  categoryId: string | number
) => {
  const queryVariables = {
    categoryId,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['product'] } });

  const response =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    await StrapiClient.request<{ categoryProductsColors: [string] }>(
      gql`
        query QueryCategoryProductsColors($categoryId: ID!) {
          categoryProductsColors(categoryId: $categoryId)
        }
      `,
      queryVariables
    );

  return response;
};

/**
 * Query all categories from Strapi
 * @returns categories
 */
export const QueryCategories = async () => {
  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['category'] } });

  const response =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    await StrapiClient.request<{
      categories: { data: { id: string; attributes: { title: string } }[] };
    }>(
      gql`
        query QueryCategories {
          categories {
            data {
              id
              attributes {
                title
              }
            }
          }
        }
      `
    );

  return response;
};

/**
 * Query all redirections
 * @param page nb of the page to query
 * @param pageSize size of the page to query
 * @returns data of redirections
 */
export const QueryRedirections = async (page?: number, pageSize = 50) => {
  const queryVariables = {
    page,
    pageSize,
  };

  //Add revalidate Tags to next.js fetch
  StrapiClient.requestConfig.fetch = (url, options) =>
    fetch(url as URL, { ...options, next: { tags: ['redirection'] } });

  const response = await StrapiClient.request<{
    redirections: {
      data: Redirection[];
      meta: QueryMetaProps;
    };
  }>(
    gql`
      query Redirections($page: Int, $pageSize: Int) {
        redirections(pagination: { page: $page, pageSize: $pageSize }) {
          data {
            attributes {
              newPath
              oldPath
              type
            }
          }
          meta {
            pagination {
              page
              pageCount
            }
          }
        }
      }
    `,
    queryVariables
  );

  return response;
};
