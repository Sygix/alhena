export interface CartItem {
  product: Product;
  size: string;
  type: string;
  price: number;
  qty: number;
  color?: string;
  gift_card?: boolean;
}

export type ColorProducts = {
  [id: number]: Product[];
};

export interface Product {
  id: number;
  selectedSize?: string;
  selectedType?: string;
  attributes: {
    title: string;
    slug: string;
    price: number;
    global_quantity?: number | null;
    new_product_tag: boolean;
    sale_price?: number;
    date_on_sale_from?: string;
    date_on_sale_to?: string;
    medias: {
      data: Media[];
    };
    short_description?: string;
    description_tabs: {
      title: string;
      content: string;
    }[];
    sizes: ProductSize[];
    colors?: {
      name: string;
      color: string;
      image: {
        data?: Media;
      };
      product: {
        data: {
          id: number;
          attributes: {
            slug: string;
          };
        };
      };
    }[];
    types: ProductType[];
    categories?: {
      data: Category[];
    };
    content: {
      __typename: string;
    };
    gift_card: boolean;
    metadata: SeoMetadata;
    locale: string;
    updatedAt: string;
    localizations: {
      data: Localizations[];
    };
  };
}

export interface Category {
  id: number;
  attributes: {
    title: string;
    slug: string;
    description: string;
    short_description: string;
    image: {
      data?: Media;
    };
    content: {
      __typename: string;
    };
    metadata: SeoMetadata;
    locale: string;
    updatedAt: string;
    localizations: {
      data: Localizations[];
    };
  };
}

export interface Page {
  id: number;
  attributes: {
    slug: string;
    content: {
      __typename: string;
    };
    metadata: SeoMetadata;
    locale: string;
    updatedAt: string;
    localizations: {
      data: Localizations[];
    };
  };
}

export interface Discount {
  id: number;
  attributes: {
    code: string;
    type: 'percentage' | 'fixed' | 'gift_card';
    value: number;
    value_left: number;
    minimum_spend?: number;
    maximum_spend?: number;
    start_date?: string;
    expiration: string;
    max_uses?: number;
    uses: number;
  };
}

export interface QueryMetaProps {
  pagination: {
    page: number;
    pageCount: number;
  };
}

export interface Setting {
  attributes: {
    favicons: {
      data: {
        attributes: {
          url: string;
        };
      }[];
    };
    seo: DefaultSeoMetadata;
    payment_provider: string;
    categories_page_size: number;
    umami_script_link?: string;
    umami_website_id?: string;
    cart_page: {
      data?: Page;
    };
  };
}

export interface Menu {
  attributes: {
    announce?: {
      text: string;
    };
    header: Header;
    footer: Footer;
  };
}

export interface Header {
  id: number;
  logo: {
    data: Media;
  };
  logo_link: string;
  items: HeaderItem[];
  cart_page?: {
    data: Page;
  };
}

export interface HeaderItem {
  id: number;
  link: LinkInterface;
  sublinks: LinkInterface[];
}

export interface Footer {
  id: number;
  columns: FooterColumn[];
  copyright: string;
}

export interface FooterColumn {
  id: number;
  title: string;
  description?: string;
  socials: LinkInterface[];
  links: LinkInterface[];
  newsletter?: NewsletterInput;
}

interface NewsletterInput {
  id: number;
  placeholder: string;
}

export interface LinkInterface {
  id?: number;
  name: string;
  href: string;
  open_new_tab: boolean;
  icon?: string;
  style: ENUM_ELEMENTS_LINK_STYLE;
  direction: ENUM_ELEMENTS_LINK_DIRECTION;
  variant: ENUM_ELEMENTS_LINK_VARIANT;
}

export interface Order {
  id: number;
  attributes: {
    payment_intent_id?: string;
    order_intent_id: string;
    email: string;
    billing_name: string;
    billing_city: string;
    billing_country: string;
    billing_line1: string;
    billing_line2: string;
    billing_postal_code: string;
    billing_state: string;
    shipping_name: string;
    shipping_city: string;
    shipping_country: string;
    shipping_line1: string;
    shipping_line2: string;
    shipping_postal_code: string;
    shipping_state: string;
    status: ENUM_ORDER_STATUS;
    amount: number;
    subtotal?: number;
    products: OrderProducts[];
    locale?: string;
    discount_id?: number;
  };
}

export interface ProductSize {
  id: number;
  size: string;
  quantity: number;
}

export interface ProductType {
  id: number;
  name: string;
}

export interface OrderProducts {
  id: number;
  title: string;
  price: number;
  qty: number;
  size: string;
  sizeId: number;
  type: string;
  color?: string;
  gift_card?: boolean;
}

export type localeProps = {
  i18NLocales: {
    data: [
      {
        attributes: {
          code: string;
          name: string;
        };
      }
    ];
  };
};

export interface Media {
  id: number;
  attributes: {
    alternativeText?: string;
    caption?: string;
    name?: string;
    url: string;
    width: number;
    height: number;
    mime: string;
  };
}

interface DefaultSeoMetadata {
  title: string;
  siteName: string;
  description: string;
}

interface SeoMetadata {
  template_title?: string;
  title_suffix?: string;
  meta_description?: string;
}

interface Localizations {
  attributes: {
    locale: string;
    slug: string;
  };
}

export enum ENUM_ORDER_STATUS {
  succeeded = 'succeeded',
  processing = 'processing',
  pending = 'pending',
  checkout = 'checkout',
  failed = 'failed',
  canceled = 'canceled',
}

export enum PAYMENT_PROVIDER {
  STRIPE = 'STRIPE',
}

export enum ENUM_ELEMENTS_LINK_STYLE {
  primary = 'primary',
  underline = 'underline',
  button = 'button',
  icon = 'icon',
  arrow = 'arrow',
  none = 'none',
}

export enum ENUM_ELEMENTS_LINK_DIRECTION {
  left = 'left',
  right = 'right',
}

export enum ENUM_ELEMENTS_LINK_VARIANT {
  primary = 'primary',
  outline = 'outline',
  ghost = 'ghost',
  light = 'light',
  dark = 'dark',
}

export enum ENUM_MAX_WIDTH_SCREEN {
  'default' = '',
  'none' = 'max-w-none',
  'max_w_screen_sm' = 'max-w-screen-sm',
  'max_w_screen_md' = 'max-w-screen-md',
  'max_w_screen_lg' = 'max-w-screen-lg',
  'max_w_screen_xl' = 'max-w-screen-xl',
  'max_w_screen_2xl' = 'max-w-screen-2xl',
}

export enum listTypeEnum {
  'numbers' = '1',
  'uppercase_letters' = 'A',
  'letters' = 'a',
  'uppercase_roman_numbers' = 'I',
  'roman_numbers' = 'i',
}

export interface Redirection {
  attributes: {
    newPath: string;
    oldPath: string;
    type: REDIRECTIONS;
  };
}

export enum REDIRECTIONS {
  'permanent_redirect' = 'permanent_redirect',
  'temporary_redirect' = 'temporary_redirect',
  'rewrite' = 'rewrite',
}
