'use server';

import { QueryProducts } from '@/lib/graphql';
import { Product, QueryMetaProps } from '@/lib/interfaces';

export const loadProductsFromStrapi = async (
  locale: string,
  page: number,
  pageSize: number,
  categories?: (string | number)[],
  priceFrom?: number,
  priceTo?: number,
  size?: string[],
  color?: string[],
  sort?: 'price:asc' | 'price:desc' | 'title:asc' | 'title:desc'
): Promise<{ data: Product[]; meta: QueryMetaProps }> => {
  try {
    const res = await QueryProducts(
      locale,
      page,
      pageSize,
      new Date().toISOString(),
      categories,
      priceFrom,
      priceTo,
      size,
      color,
      sort
    );
    return res;
  } catch (error) {
    throw new Error(
      'Oups, there was a problem with your request, please try again later or contact us'
    );
  }
};
