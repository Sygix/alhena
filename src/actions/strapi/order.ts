'use server';

import {
  MutationCreateOrder,
  MutationUpdateManyProducts,
  MutationUpdateManyProductSize,
  MutationUpdateOrderByIntentId,
  MutationUpdateProductSize,
  QueryOrderWithIntent,
} from '@/lib/graphql';
import { ENUM_ORDER_STATUS, OrderProducts } from '@/lib/interfaces';

import { AddressFormType } from '@/components/elements/forms/AddressForm';

import { handlePaymentIntentSucceeded } from '@/actions/stripe/webhooks';

export type StrapiError = {
  error?: {
    type: string;
    message?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data?: any;
  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data?: any;
};

const createOrUpdateOrder = async (
  total: number,
  subtotal: number,
  orderProducts: OrderProducts[],
  locale: string,
  discount_id?: number,
  order_id?: string,
  order_intent_id?: string,
  payment_intent_id?: string
): Promise<StrapiError> => {
  const input = {
    payment_intent_id,
    amount: total,
    subtotal,
    products: orderProducts,
    discount_id,
    locale,
  };
  try {
    if (order_id && order_intent_id) {
      const { updateByOrderIntentId } = await MutationUpdateOrderByIntentId(
        order_id,
        order_intent_id,
        input
      );
      return {
        data: {
          order_id: updateByOrderIntentId.data.id,
          order_intent_id:
            updateByOrderIntentId.data.attributes.order_intent_id,
        },
      };
    }
    const { createOrder } = await MutationCreateOrder(input);
    return {
      data: {
        order_id: createOrder.data.id,
        order_intent_id: createOrder.data.attributes.order_intent_id,
      },
    };
  } catch (error) {
    return { error: { type: 'internal-server-error' } };
  }
};

export const updateOrderAddress = async (
  ckAddress: AddressFormType,
  order_id: string,
  order_intent_id: string,
  payment_intent_id?: string
): Promise<StrapiError> => {
  try {
    const input = {
      payment_intent_id: payment_intent_id,
      email: ckAddress.email,
      billing_name: ckAddress.address.name,
      billing_city: ckAddress.address.city,
      billing_country: ckAddress.address.country,
      billing_line1: ckAddress.address.line1,
      billing_line2: ckAddress.address.line2,
      billing_postal_code: ckAddress.address.postal_code,
      billing_state: ckAddress.address.state,
      shipping_name: ckAddress.shipping.name,
      shipping_city: ckAddress.shipping.city,
      shipping_country: ckAddress.shipping.country,
      shipping_line1: ckAddress.shipping.line1,
      shipping_line2: ckAddress.shipping.line2,
      shipping_postal_code: ckAddress.shipping.postal_code,
      shipping_state: ckAddress.shipping.state,
      send_to_recipient: ckAddress.giftCard?.sendToRecipient,
      send_me_copy: ckAddress.giftCard?.sendMeCopy,
      gift_card_email: ckAddress.giftCard?.email,
    };

    let updatedOrderData;

    try {
      const { updateByOrderIntentId } = await MutationUpdateOrderByIntentId(
        order_id,
        order_intent_id,
        input
      );
      updatedOrderData = updateByOrderIntentId.data;
    } catch (error) {
      return { error: { type: 'forbidden-update' } };
    }

    const { subtotal, status, discount_id, products, locale } =
      updatedOrderData.attributes;

    if (
      subtotal === 0 &&
      discount_id &&
      status === ENUM_ORDER_STATUS.checkout &&
      products.length > 0
    ) {
      try {
        // Update stock
        if (products.length > 1) {
          await MutationUpdateManyProductSize(
            products.map((product) => product.sizeId),
            products.map((product) => {
              return { quantity: -product.qty };
            })
          );
        } else {
          await MutationUpdateProductSize(products[0].sizeId, {
            quantity: -products[0].qty,
          });
        }

        //Also update global quantity on product if it exists
        await MutationUpdateManyProducts(
          products.map((product) => product.id),
          products.map((product) => {
            return { global_quantity: -product.qty };
          }),
          locale ?? 'fr'
        );

        //Update order status to succeeded
        await handlePaymentIntentSucceeded({ metadata: { order_id } });
      } catch (error) {
        return { error: { type: 'no-valid-cart' } };
      }

      return {
        data: {
          status: ENUM_ORDER_STATUS.succeeded,
          order_id,
          order_intent_id,
        },
      };
    }
    return {};
  } catch (error) {
    return { error: { type: 'internal-server-error' } };
  }
};

export const queryOrderByIntentId = async (
  order_id: string | number,
  order_intent_id: string
) => {
  try {
    const { data } = await QueryOrderWithIntent(order_id, order_intent_id);
    return {
      data,
    };
  } catch (error) {
    return { error: { type: 'internal-server-error' } };
  }
};

export default createOrUpdateOrder;
