'use server';

import { isAfter, isBefore, parseISO } from 'date-fns';

import { QueryDiscountFromCode } from '@/lib/graphql';

import { StrapiError } from '@/actions/strapi/order';

const getDiscountFromCode = async (
  code: string,
  spendAmount = 0
): Promise<StrapiError> => {
  try {
    const { data } = await QueryDiscountFromCode(code);
    if (data && data.length <= 0)
      return { error: { type: 'discount-not-found' } };
    const discount = data[0];
    const discountStartDate = discount.attributes.start_date
      ? parseISO(discount.attributes.start_date)
      : undefined;
    const discountExpiration = parseISO(discount.attributes.expiration);
    const currentDate = new Date();
    if (discountStartDate && isBefore(currentDate, discountStartDate))
      return { error: { type: 'discount-not-started' } };
    if (isAfter(currentDate, discountExpiration))
      return { error: { type: 'discount-expired' } };
    if (
      discount.attributes.max_uses &&
      discount.attributes.max_uses <= discount.attributes.uses
    )
      return { error: { type: 'discount-max-uses' } };
    if (
      discount.attributes.type === 'gift_card' &&
      discount.attributes.value_left <= 0
    )
      return { error: { type: 'discount-no-value-left' } };
    if (
      discount.attributes.minimum_spend &&
      spendAmount < discount.attributes.minimum_spend
    )
      return {
        error: {
          type: 'discount-minimum-spend',
          data: { min: discount.attributes.minimum_spend },
        },
      };
    if (
      discount.attributes.maximum_spend &&
      spendAmount > discount.attributes.maximum_spend
    )
      return {
        error: {
          type: 'discount-maximum-spend',
          data: { max: discount.attributes.maximum_spend },
        },
      };
    return { data: discount };
  } catch (error) {
    return { error: { type: 'internal-server-error' } };
  }
};

export default getDiscountFromCode;
