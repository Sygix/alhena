'use server';

import { isAfter, isBefore, parseISO } from 'date-fns';

import { QueryDiscountById, QueryProduct } from '@/lib/graphql';
import { deepEqual, isOnSale } from '@/lib/helper';
import { Discount, OrderProducts } from '@/lib/interfaces';

type checkCart = {
  error?: {
    type: string;
    message?: string;
  };
  data?: unknown;
};

const checkCart = async (
  orderProducts: OrderProducts[],
  discountId?: number
): Promise<checkCart> => {
  try {
    let discountData: Discount | undefined = undefined;
    if (discountId) {
      // Query the discount data by id
      const { data: dData } = await QueryDiscountById(discountId);
      discountData = dData;
      if (!discountData) return { error: { type: 'discount-not-found' } };
      const discountStartDate = discountData.attributes.start_date
        ? parseISO(discountData.attributes.start_date)
        : undefined;
      const discountExpiration = parseISO(discountData.attributes.expiration);
      const currentDate = new Date();
      if (discountStartDate && isBefore(currentDate, discountStartDate))
        return { error: { type: 'discount-not-started' } };
      if (isAfter(currentDate, discountExpiration))
        return { error: { type: 'discount-expired' } };
      if (
        discountData.attributes.max_uses &&
        discountData.attributes.max_uses <= discountData.attributes.uses
      )
        return { error: { type: 'discount-max-uses' } };
      if (
        discountData.attributes.type === 'gift_card' &&
        discountData.attributes.value_left <= 0
      )
        return { error: { type: 'discount-no-value-left' } };
    }

    // Use Promise.all to check each item in parallel
    const checkedItems = await Promise.all(
      orderProducts.map(async (item) => {
        // Query the product data by id
        const { data } = await QueryProduct(item.id, true);
        const global_quantity = data.attributes.global_quantity;

        //Check if product quantity doesn't exceed global product quantity regardless of size
        const total_product_quantity = orderProducts
          .filter((it) => it.id === item.id)
          .reduce((acc, item) => acc + item.qty, 0);
        if (
          global_quantity !== null &&
          global_quantity !== undefined &&
          global_quantity < total_product_quantity
        ) {
          return Promise.reject({
            type: 'insufficient-quantity-available',
            message: JSON.stringify(item),
          });
        }

        // Determine the price based on the sale dates
        const price = isOnSale(
          data.attributes.date_on_sale_from,
          data.attributes.date_on_sale_to
        )
          ? data.attributes.sale_price ?? data.attributes.price
          : data.attributes.price;

        // Find the size that matches the item size
        const size = data.attributes.sizes.find((el) => el.size === item.size);

        //Check if global quantity exists and has enough quantity
        if (
          global_quantity &&
          global_quantity > 0 &&
          global_quantity >= item.qty
        ) {
          // If global quantity does exist, check if the size exists and has enough quantity
          if (size && (size.quantity > 0 || size.quantity === -1)) {
            if (size.quantity >= item.qty || size.quantity === -1) {
              // Return the item with the price
              return {
                ...item,
                price: price,
              };
            }
          }
        }

        // If global quantity does not exist, check if the size exists and has enough quantity
        if (
          (typeof global_quantity === undefined || global_quantity === null) &&
          size &&
          (size.quantity > 0 || size.quantity === -1)
        ) {
          if (size.quantity >= item.qty || size.quantity === -1) {
            // Return the item with the price
            return {
              ...item,
              price: price,
            };
          }
        }

        return Promise.reject({
          type: 'insufficient-quantity-available',
          message: JSON.stringify(item),
        });
      })
    );

    // Check if the checked items are equal to the order products
    if (deepEqual(orderProducts, checkedItems)) {
      // Return the checked items as data
      return { data: { orderProducts: checkedItems, discount: discountData } };
    }
    // Return an error object with not-equal message
    return {
      error: {
        type: 'not-equal',
      },
    };
  } catch (error) {
    // Return an error object with the caught error
    return {
      error: error as checkCart['error'],
    };
  }
};

export default checkCart;
