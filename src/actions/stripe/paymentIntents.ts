'use server';

import Stripe from 'stripe';

import { toFixedNumber } from '@/lib/helper';
import { Discount, OrderProducts } from '@/lib/interfaces';

import checkCart from '@/actions/checkCart';
import createOrUpdateOrder from '@/actions/strapi/order';

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY ?? '', {
  apiVersion: '2022-11-15',
});

type createPaymentIntent = {
  error?: {
    type: string;
    message?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data?: any;
  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data?: any;
};

const createOrUpdatePaymentIntent = async (
  orderProducts: OrderProducts[],
  locale: string,
  order_id?: string,
  order_intent_id?: string,
  discountId?: number,
  payment_intent_id?: string
): Promise<createPaymentIntent> => {
  // Check the cart
  const { data, error } = (await checkCart(orderProducts, discountId)) as {
    data: { orderProducts: OrderProducts[]; discount?: Discount };
    error: checkCart['error'];
  };
  // If there is an error, return the error
  if (error) return { error };

  const checkedItems = data.orderProducts;

  const total = checkedItems?.reduce((acc, item) => {
    return item ? acc + item.price * item.qty : acc;
  }, 0);

  //Checking min/max spend limits on discount
  if (
    data.discount &&
    data.discount.attributes.maximum_spend &&
    total > data.discount.attributes.maximum_spend
  ) {
    return {
      error: {
        type: 'discount-maximum-spend',
        data: { max: data.discount.attributes.maximum_spend },
      },
    };
  }
  if (
    data.discount &&
    data.discount.attributes.minimum_spend &&
    total < data.discount.attributes.minimum_spend
  ) {
    return {
      error: {
        type: 'discount-minimum-spend',
        data: { min: data.discount.attributes.minimum_spend },
      },
    };
  }

  let subTotal = total;

  //if discount calculate subtotal
  if (data.discount) {
    const discountType = data.discount.attributes.type;
    const discountValue =
      discountType === 'gift_card'
        ? data.discount.attributes.value_left
        : data.discount.attributes.value;

    const discountAmount = toFixedNumber(
      discountType === 'percentage'
        ? (total * discountValue) / 100
        : discountValue > total
        ? total
        : discountValue,
      2
    );
    subTotal = toFixedNumber(total - discountAmount, 2);
  }

  try {
    // If payment_intent_id exists, update the payment intent
    if (payment_intent_id && order_intent_id && subTotal >= 0.5) {
      const { id, client_secret, metadata } =
        await stripe.paymentIntents.update(payment_intent_id, {
          amount: toFixedNumber(subTotal * 100, 0),
          currency: 'eur',
          metadata: {
            products: JSON.stringify(checkedItems),
          },
        });
      const { error } = await createOrUpdateOrder(
        toFixedNumber(total, 2),
        toFixedNumber(subTotal, 2),
        checkedItems,
        locale,
        discountId,
        metadata.order_id,
        order_intent_id,
        payment_intent_id
      );
      if (error) return { error };
      return { data: { payment_intent_id: id, client_secret } };
    }

    if (order_intent_id && order_id) {
      const { error, data } = await createOrUpdateOrder(
        toFixedNumber(total, 2),
        toFixedNumber(subTotal, 2),
        checkedItems,
        locale,
        discountId,
        order_id,
        order_intent_id,
        payment_intent_id
      );
      if (error) return { error };
      return {
        data: {
          order_intent_id: data.order_intent_id,
          order_id: data.order_id,
        },
      };
    }

    // If payment_intent_id or order_intent_id does not exist, create a new payment intent
    // Create or update an order in Strapi
    const { error, data } = await createOrUpdateOrder(
      toFixedNumber(total, 2),
      toFixedNumber(subTotal, 2),
      checkedItems,
      locale,
      discountId
    );

    // If there is an error, return the error
    if (error) return { error };

    if (subTotal <= 0)
      return {
        data: {
          order_id: data.order_id,
          order_intent_id: data.order_intent_id,
        },
      };

    if (subTotal < 0.5) return { error: { type: 'cart-minimum-spend' } };

    // Create a new payment intent
    const { id, client_secret } = await stripe.paymentIntents.create({
      amount: toFixedNumber(subTotal * 100, 0),
      currency: 'eur',
      automatic_payment_methods: {
        enabled: true,
      },
      metadata: {
        order_id: data.order_id,
        products: JSON.stringify(checkedItems),
      },
    });
    return {
      data: {
        order_id: data.order_id,
        order_intent_id: data.order_intent_id,
        payment_intent_id: id,
        client_secret,
      },
    };
  } catch (error) {
    return { error: { type: 'internal-server-error' } };
  }
};

export const abandonPaymentIntent = async (payment_intent_id: string) => {
  try {
    await stripe.paymentIntents.cancel(payment_intent_id, {
      cancellation_reason: 'abandoned',
    });
  } catch (err) {
    return;
  }
};

export default createOrUpdatePaymentIntent;
