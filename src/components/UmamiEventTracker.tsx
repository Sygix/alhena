'use client';

import { useEffect } from 'react';

import { umamiAnalytics } from '@/lib/helper';

const UmamiEventTracker = () => {
  const findATagWithAnalyticsEvent = (
    rootElem: HTMLElement,
    maxSearchDepth: number
  ) => {
    let currentElement: HTMLElement | null = rootElem;
    for (let i = 0; i < maxSearchDepth; i++) {
      if (
        currentElement.tagName === 'A' &&
        currentElement.getAttribute('data-analytics-event')
      ) {
        return currentElement;
      }
      currentElement = currentElement.parentElement;
      if (!currentElement) {
        return null;
      }
    }
    return null;
  };

  const attributesIterator = (elem: HTMLElement) => {
    const analyticsData: { [key: string]: string } = {};
    for (let i = 0; i < elem.attributes.length; i++) {
      const attr = elem.attributes[i];
      if (attr.name.startsWith('data-analytics-event-')) {
        const key = attr.name.replace('data-analytics-event-', '');
        analyticsData[key] = attr.value;
      }
    }
    return analyticsData;
  };

  const callback = (event: MouseEvent) => {
    let target = event.target as HTMLElement;
    let analyticsEvent = target.getAttribute('data-analytics-event');

    if (!analyticsEvent) {
      const aTag = findATagWithAnalyticsEvent(target, 10);
      if (aTag) {
        target = aTag;
        analyticsEvent = target.getAttribute('data-analytics-event');
      }
    }

    if (analyticsEvent) {
      const analyticsData = attributesIterator(target);
      umamiAnalytics(analyticsEvent, analyticsData);
    }
  };

  useEffect(() => {
    document.addEventListener('click', callback);

    return () => {
      document.removeEventListener('click', callback);
    };
  });

  return null;
};

export default UmamiEventTracker;
