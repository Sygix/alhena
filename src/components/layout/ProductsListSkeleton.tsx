import clsxm from '@/lib/clsxm';

import Skeleton from '@/components/Skeleton';

const ProductsListSkeleton = () => {
  return (
    <ul className='grid w-full max-w-screen-3xl grid-cols-1 gap-3 p-3 xs:grid-cols-2 md:grid-cols-3 md:gap-6 md:py-6 lg:grid-cols-4 xl:grid-cols-5'>
      {[...Array(5)].map((_, index) => (
        <li
          key={index}
          className={clsxm(
            index > 1 && 'hidden',
            index === 2 && 'md:block',
            index === 3 && 'lg:block',
            index === 4 && 'xl:block'
          )}
        >
          <div className='flex flex-col gap-4'>
            <div className='flex flex-col gap-4'>
              <Skeleton className='aspect-[2/3] h-full overflow-hidden rounded-md' />
              <Skeleton className='flex items-center justify-between gap-2 whitespace-nowrap font-bold' />
              <Skeleton className='h-[1.5em] w-full rounded-md' />
              <div className='flex flex-col gap-1'>
                <Skeleton className='h-[.8em] w-3/4 rounded-md' />
                <Skeleton className='h-[.8em] w-full rounded-md' />
                <Skeleton className='h-[.8em] w-full rounded-md' />
                <Skeleton className='h-[.8em] w-2/4 rounded-md' />
              </div>
            </div>
          </div>
        </li>
      ))}
    </ul>
  );
};

export default ProductsListSkeleton;
