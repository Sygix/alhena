import { MDXRemote } from 'next-mdx-remote/rsc';

import { QueryMenus } from '@/lib/graphql';

import { useServer } from '@/store/serverStore';

const Announce = async () => {
  const locale = useServer.getState().locale;
  const { data } = await QueryMenus(locale);
  const { announce } = data.attributes;

  if (!announce) return null;

  return (
    <div className='flex justify-center top-0 z-50 bg-primary-200 text-carbon-900 p-1'>
      <div className='w-full max-w-screen-3xl flex justify-center prose prose-carbon prose-p:text-carbon-700'>
        <MDXRemote source={announce.text} />
      </div>
    </div>
  );
};

export default Announce;
