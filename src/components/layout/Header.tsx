import Image from 'next/image';
import * as React from 'react';

import { QueryMenus } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';

import DynamicIcon from '@/components/elements/DynamicIcon';
import Link from '@/components/elements/links';
import UnstyledLink from '@/components/elements/links/UnstyledLink';
import HeaderBurger from '@/components/layout/HeaderBurger';
import HeaderCart from '@/components/layout/HeaderCart';
import HeaderItem from '@/components/layout/HeaderItem';

import { useServer } from '@/store/serverStore';

export default async function Header() {
  const locale = useServer.getState().locale;
  const translations = useServer.getState().translations;
  const { data } = await QueryMenus(locale);
  const { header } = data.attributes;

  return (
    <header className='sticky top-0 z-50 flex justify-center bg-secondary-100 font-bold text-carbon-900'>
      <div className='grid w-full max-w-screen-3xl grid-flow-row grid-cols-[auto_1fr_auto] items-center gap-3 p-3 text-lg md:p-6 lg:gap-6 lg:px-12 xl:text-xl'>
        <HeaderBurger
          items={header.items}
          className='flex justify-center md:hidden'
        />

        <UnstyledLink
          href={includeLocaleLink(header.logo_link)}
          title='Home'
          className='flex justify-center md:order-first'
          data-analytics-event='header-logo'
        >
          <Image
            src={MediaUrl(header.logo.data.attributes.url)}
            priority
            quality={100}
            width={header.logo.data.attributes.width}
            height={header.logo.data.attributes.height}
            title={header.logo.data.attributes.name}
            alt={header.logo.data.attributes.alternativeText ?? ''}
            className='h-10 w-full object-contain object-center'
            sizes='80vw'
          />
        </UnstyledLink>

        {/* Desktop links */}
        <ul className='hidden justify-center gap-3 md:flex lg:gap-6'>
          {header.items.map((item) => (
            <HeaderItem
              key={item.id}
              name={item.link.name}
              sublinks={item.sublinks}
            >
              <Link
                href={includeLocaleLink(item.link.href)}
                title={item.link.name}
                style={item.link.style}
                icon={item.link.icon}
                openNewTab={item.link.open_new_tab}
                variant={item.link.variant}
                data-analytics-event='header-link'
                data-analytics-event-link={item.link.href}
                data-analytics-event-name={item.link.name.toLocaleLowerCase()}
              >
                {item.link.name}
              </Link>
            </HeaderItem>
          ))}
        </ul>

        <HeaderCart
          cartPage={includeLocaleLink(
            `/${header.cart_page?.data.attributes.slug}`
          )}
        >
          <div className='flex flex-row flex-nowrap items-center gap-3 lg:gap-6'>
            <p className='hidden md:inline-block'>{translations.cart.title}</p>
            <DynamicIcon
              icon='heroicons:shopping-bag'
              className='h-full w-full grow-0 text-carbon-900 lg:h-10 lg:w-10'
              wrapperClassName='w-8 h-8 lg:w-10 lg:h-10'
            />
          </div>
        </HeaderCart>
      </div>
    </header>
  );
}
