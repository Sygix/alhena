'use client';

import dynamic from 'next/dynamic';
import { ReactNode, useEffect, useRef, useState } from 'react';

import { CartItemCard } from '@/components/elements/cards/CartItemCard';
import ButtonLink from '@/components/elements/links/ButtonLink';

import useStore from '@/store';
import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';

const MotionDiv = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.div)
);

const HeaderCart = ({
  children,
  cartPage,
}: {
  children?: ReactNode;
  cartPage: string;
}) => {
  const divRef = useRef<HTMLSpanElement>(null);
  const cartItems = useStore(useCart, (state) => state.cartItems);
  const cartTotalPrice = useStore(useCart, (state) => state.totalPrice) ?? 0;
  const translations = useServer.getState().translations;
  const [isOpen, setIsOpen] = useState(false);

  const divVariants = {
    open: {
      display: 'flex',
      opacity: 1,
      transition: {
        when: 'beforeChildren',
        delayChildren: 0.3,
        staggerChildren: 0.05,
      },
    },
    closed: {
      display: 'none',
      opacity: 0,
      transition: {
        when: 'afterChildren',
      },
    },
  };

  const itemVariants = {
    open: {
      opacity: 1,
      y: 0,
      transition: {
        type: 'spring',
        stiffness: 300,
        damping: 24,
      },
    },
    closed: {
      opacity: 0,
      y: 20,
      transition: {
        duration: 0.2,
      },
    },
  };

  useEffect(() => {
    if (!isOpen) return;
    function handleClick({ target }: MouseEvent) {
      if (divRef.current && !divRef.current.contains(target as Node)) {
        setIsOpen(false);
      }
    }
    window.addEventListener('click', handleClick);
    return () => window.removeEventListener('click', handleClick);
  }, [isOpen]);

  return (
    <span ref={divRef}>
      <MotionDiv
        onClick={() => setIsOpen((state) => !state)}
        className='cursor-pointer'
      >
        {children}
        {/* Cart */}
        <MotionDiv
          initial={false}
          animate={isOpen ? 'open' : 'closed'}
          variants={divVariants}
          className='max-md:no-scrollbar md:display-scrollbar absolute left-0 top-0 -z-10 hidden h-fit max-h-screen w-full cursor-default flex-col items-center gap-3 overflow-y-auto bg-secondary-100 px-3 pb-3 pt-20 md:left-auto md:right-0 md:top-20 md:max-h-[70vh] md:max-w-md md:p-6 md:px-12'
        >
          {cartItems?.map((item, index) => (
            <MotionDiv key={index} variants={itemVariants}>
              <CartItemCard cartItem={item} className='text-base' />
            </MotionDiv>
          ))}
          {!cartItems?.length && (
            <MotionDiv variants={itemVariants}>
              <p className='first-letter:uppercase'>
                {translations.cart?.empty ?? 'Cart is empty'}
              </p>
            </MotionDiv>
          )}
          <MotionDiv
            variants={itemVariants}
            className='flex w-full justify-between border-t-2 border-carbon-900 pt-4 text-base font-bold md:pt-8'
          >
            <p className='first-letter:uppercase'>
              {translations.total ?? 'Total'}
            </p>
            <p>{cartTotalPrice} €</p>
          </MotionDiv>
          <MotionDiv
            variants={itemVariants}
            className='flex w-full justify-center'
          >
            <ButtonLink
              href={cartPage}
              variant='dark'
              className='w-full max-w-xs justify-center'
              data-analytics-event='header-cart'
            >
              <p className='first-letter:uppercase'>
                {translations.cart?.go_to_cart ?? 'Go to cart'}
              </p>
            </ButtonLink>
          </MotionDiv>
        </MotionDiv>
      </MotionDiv>
    </span>
  );
};

export default HeaderCart;
