import { MDXRemote } from 'next-mdx-remote/rsc';

import { QueryMenus } from '@/lib/graphql';
import { includeLocaleLink } from '@/lib/helper';

import DynamicIcon from '@/components/elements/DynamicIcon';
import NewsletterForm from '@/components/elements/forms/NewsletterForm';
import Link from '@/components/elements/links';
import LanguageSwitch from '@/components/layout/LanguageSwitch';

import { useServer } from '@/store/serverStore';

const Footer = async () => {
  const locale = useServer.getState().locale;
  const locales = useServer.getState().locales;
  const currentLocale = locales?.find((el) => el.attributes.code === locale);

  const { data } = await QueryMenus(locale);
  const { footer } = data.attributes;

  return (
    <footer className='w-full bg-carbon-900 text-white'>
      <div className='layout flex flex-col gap-6 p-3 py-6 md:px-6 lg:px-12'>
        <div className='grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-5'>
          {footer.columns.map((column) => (
            <nav
              key={column.id}
              className='flex flex-col gap-3 text-center md:text-left'
            >
              {column.title && (
                <h3 className='text-base font-semibold xl:text-lg'>
                  {column.title}
                </h3>
              )}
              {column.description && (
                <p className='text-sm xl:text-base'>{column.description}</p>
              )}
              {column.socials.length > 0 && (
                <div className='flex flex-row justify-center gap-3 md:justify-start'>
                  {column.socials.map((social) => (
                    <Link
                      key={social.id}
                      href={includeLocaleLink(social.href)}
                      title={social.name}
                      icon={social.icon}
                      openNewTab={social.open_new_tab}
                      style={social.style}
                      variant={social.variant}
                      className='text-xl lg:text-2xl xl:text-3xl'
                      data-analytics-event='footer-social'
                      data-analytics-event-social={social.name.toLocaleLowerCase()}
                    >
                      {social.name}
                    </Link>
                  ))}
                </div>
              )}
              {column.links.length > 0 && (
                <ul className='flex flex-col gap-3 text-sm font-normal xl:text-base'>
                  {column.links.map((item) => (
                    <li key={item.id}>
                      <Link
                        href={includeLocaleLink(item.href)}
                        title={item.name}
                        icon={item.icon}
                        openNewTab={item.open_new_tab}
                        style={item.style}
                        variant={item.variant}
                        data-analytics-event='footer-link'
                        data-analytics-event-name={item.name.toLocaleLowerCase()}
                        data-analytics-event-link={item.href.toLocaleLowerCase()}
                      >
                        {item.name}
                      </Link>
                    </li>
                  ))}
                </ul>
              )}
              {column.newsletter && (
                <div className='flex w-full justify-center md:justify-start'>
                  <NewsletterForm
                    placeholder={column.newsletter.placeholder}
                    className='max-w-xs'
                  />
                </div>
              )}
            </nav>
          ))}
        </div>
        <div className='flex flex-col gap-3 border-t-2 border-white pt-6 text-center text-sm md:flex-row md:gap-6 xl:text-base'>
          <div className='flex justify-center md:flex-1 md:justify-start'>
            <LanguageSwitch className='flex flex-row items-center justify-center gap-3'>
              <>
                <DynamicIcon
                  icon={`flagpack:${
                    currentLocale?.attributes.code ?? 'gb-ukm'
                  }`}
                  wrapperClassName='w-8 rounded-md overflow-hidden'
                  className='h-fit w-full'
                />
              </>
              <span>{currentLocale?.attributes.name}</span>
            </LanguageSwitch>
          </div>
          <div className='md:flex-1'>
            <MDXRemote source={footer.copyright} />
          </div>
          <div className='hidden md:block md:flex-1'></div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
