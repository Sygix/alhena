'use client';

import dynamic from 'next/dynamic';
import { useEffect, useRef, useState } from 'react';

import { includeLocaleLink, umamiAnalytics } from '@/lib/helper';
import { HeaderItem } from '@/lib/interfaces';

import DynamicIcon from '@/components/elements/DynamicIcon';
import Link from '@/components/elements/links';

const MotionNav = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.nav)
);

const MotionButton = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.button)
);

const MotionUl = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.ul)
);

const MotionLi = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.li)
);

const HeaderBurger = ({
  items,
  className,
}: {
  items: HeaderItem[];
  className?: string;
}) => {
  const navRef = useRef<HTMLSpanElement>(null);
  const ulRef = useRef<HTMLSpanElement>(null);
  const [isOpen, setIsOpen] = useState(false);

  const ulVariants = {
    open: {
      display: 'flex',
      opacity: 1,
      transition: {
        when: 'beforeChildren',
        delayChildren: 0.3,
        staggerChildren: 0.05,
      },
    },
    closed: {
      display: 'none',
      opacity: 0,
      transition: {
        when: 'afterChildren',
      },
    },
  };

  const itemVariants = {
    open: {
      opacity: 1,
      y: 0,
      transition: {
        type: 'spring',
        stiffness: 300,
        damping: 24,
      },
    },
    closed: {
      opacity: 0,
      y: 20,
      transition: {
        duration: 0.2,
      },
    },
  };

  useEffect(() => {
    if (!isOpen) return;
    function handleClick({ target }: MouseEvent) {
      if (
        navRef.current &&
        ulRef.current &&
        !navRef.current.contains(target as Node) &&
        !ulRef.current.contains(target as Node)
      ) {
        setIsOpen(false);
      }
    }
    window.addEventListener('click', handleClick);
    return () => window.removeEventListener('click', handleClick);
  }, [isOpen]);

  return (
    <span ref={navRef} className={className}>
      <MotionNav initial={false} animate={isOpen ? 'open' : 'closed'}>
        <MotionButton
          name='burger-menu'
          onClick={() => {
            umamiAnalytics('header-burger');
            setIsOpen(!isOpen);
          }}
          className='h-8 w-8 p-0'
        >
          {isOpen ? (
            <DynamicIcon
              icon='heroicons:x-mark-20-solid'
              className='h-full w-full text-carbon-900'
            />
          ) : (
            <DynamicIcon
              icon='heroicons:bars-2'
              className='h-full w-full text-carbon-900'
            />
          )}
        </MotionButton>
        <span ref={ulRef}>
          <MotionUl
            variants={ulVariants}
            className='absolute left-0 top-0 -z-10 hidden h-fit max-h-screen w-full flex-col items-center gap-3 bg-secondary-100 pb-10 pt-20'
          >
            {items.map((item) => (
              <MotionLi
                variants={itemVariants}
                key={item.id}
                className='mx-3 flex w-full flex-col items-center'
              >
                <Link
                  href={includeLocaleLink(item.link.href)}
                  title={item.link.name}
                  style={item.link.style}
                  icon={item.link.icon}
                  variant={item.link.variant}
                  openNewTab={item.link.open_new_tab}
                  className='flex w-full justify-center'
                  onClick={() => {
                    umamiAnalytics('header-burger-link', {
                      link: item.link.href,
                      name: item.link.name.toLocaleLowerCase(),
                    });
                    setIsOpen((state) => !state);
                  }}
                >
                  {item.link.name}
                </Link>
                {item.sublinks.length > 0 && (
                  <ul className='no-scrollbar flex w-fit max-w-full snap-x flex-nowrap gap-3 overflow-x-scroll px-2 pt-2'>
                    {item.sublinks.map((subItem) => (
                      <li key={subItem.id} className='snap-center'>
                        <Link
                          href={includeLocaleLink(subItem.href)}
                          title={subItem.name}
                          style={subItem.style}
                          icon={subItem.icon}
                          openNewTab={item.link.open_new_tab}
                          variant={subItem.variant}
                          size='lg'
                          onClick={() => {
                            umamiAnalytics('header-burger-sublink', {
                              link: item.link.href,
                              name: item.link.name.toLocaleLowerCase(),
                              sublink: subItem.href,
                              subname: subItem.name.toLocaleLowerCase(),
                            });
                            setIsOpen((state) => !state);
                          }}
                        >
                          {subItem.name}
                        </Link>
                      </li>
                    ))}
                  </ul>
                )}
              </MotionLi>
            ))}
          </MotionUl>
        </span>
      </MotionNav>
    </span>
  );
};

export default HeaderBurger;
