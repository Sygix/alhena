'use client';

import dynamic from 'next/dynamic';
import { ReactNode, useEffect, useRef, useState } from 'react';

import { includeLocaleLink } from '@/lib/helper';
import { LinkInterface } from '@/lib/interfaces';

import Link from '@/components/elements/links';

const MotionNav = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.nav)
);

const MotionLi = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.li)
);

const HeaderItem = ({
  name,
  sublinks,
  children,
}: {
  name: string;
  sublinks: LinkInterface[];
  children: ReactNode;
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const timeoutRef = useRef<NodeJS.Timeout | null>(null);

  const navVariants = {
    open: {
      display: 'flex',
      opacity: 1,
      transition: {
        when: 'beforeChildren',
        delayChildren: 0.05,
        staggerChildren: 0.05,
      },
    },
    closed: {
      display: 'none',
      opacity: 0,
      transition: {
        when: 'afterChildren',
      },
    },
  };

  const itemVariants = {
    open: {
      opacity: 1,
      y: 0,
      transition: {
        type: 'spring',
        stiffness: 300,
        damping: 24,
        duration: 0.2,
      },
    },
    closed: {
      opacity: 0,
      y: 20,
      transition: {
        duration: 0.2,
      },
    },
  };

  // delete timeout when unmounting
  useEffect(() => {
    return () => {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
    };
  }, []);

  return (
    <MotionLi
      onHoverStart={() => {
        if (timeoutRef.current) {
          clearTimeout(timeoutRef.current);
        }
        if (sublinks.length > 0) {
          setIsOpen(true);
        }
      }}
      onHoverEnd={() => {
        timeoutRef.current = setTimeout(() => {
          setIsOpen(false);
        }, 300);
      }}
    >
      {children}
      {/* SubMenu */}
      {sublinks.length > 0 && (
        <MotionNav
          initial={false}
          animate={isOpen ? 'open' : 'closed'}
          variants={navVariants}
          className='absolute left-0 top-20 -z-10 hidden w-full flex-col gap-2 bg-secondary-100 p-6 lg:px-12'
        >
          <h2 className='h0 pb-4 uppercase'>{name}</h2>
          <ul className='flex flex-row gap-6'>
            {sublinks.map((item) => (
              <MotionLi key={item.id} variants={itemVariants}>
                <Link
                  href={includeLocaleLink(item.href)}
                  title={item.name}
                  style={item.style}
                  icon={item.icon}
                  variant={item.variant}
                  size='xl'
                  data-analytics-event='header-sublink'
                  data-analytics-event-name={name}
                  data-analytics-event-sublink={item.href}
                  data-analytics-event-subname={item.name}
                >
                  {item.name}
                </Link>
              </MotionLi>
            ))}
          </ul>
        </MotionNav>
      )}
    </MotionLi>
  );
};

export default HeaderItem;
