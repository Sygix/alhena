import dynamic from 'next/dynamic';

import Brand from '@/components/sections/Brand';
import BrandComplete from '@/components/sections/BrandComplete';
import Carousel from '@/components/sections/Carousel';
import Categories from '@/components/sections/Categories';
import Concept from '@/components/sections/Concept';
import Contact from '@/components/sections/Contact';
import GiftCards from '@/components/sections/GiftCard';
import LookbookCarousel from '@/components/sections/lookbook/LookbookCarousel';
import LookbookFour from '@/components/sections/lookbook/LookbookFour';
import LookbookOne from '@/components/sections/lookbook/LookbookOne';
import LookbookThree from '@/components/sections/lookbook/LookbookThree';
import LookbookTwo from '@/components/sections/lookbook/LookbookTwo';
import MdxBlock from '@/components/sections/MdxBlock';
import Featured from '@/components/sections/products/Featured';
import Products from '@/components/sections/products/Products';
import SelectedList from '@/components/sections/products/SelectedList';
import Services from '@/components/sections/Services';

type sectionTypeProps = {
  __typename: keyof typeof sectionComponents;
};

// Map Strapi sections to section components
const sectionComponents = {
  ComponentSectionsProductSelectedList: SelectedList,
  ComponentSectionsProductList: Products,
  ComponentSectionsCart: dynamic(
    () => import('@/components/sections/checkout/cart/CartSection')
  ),
  ComponentSectionsCheckoutTunnel: dynamic(
    () => import('@/components/sections/checkout/CheckoutTunnel')
  ),
  ComponentSectionsCheckoutComplete: dynamic(
    () => import('@/components/sections/checkout/CheckoutComplete')
  ),
  ComponentSectionsCarousel: Carousel,
  ComponentSectionsMdxBlock: MdxBlock,
  ComponentSectionsBrand: Brand,
  ComponentSectionsCategories: Categories,
  ComponentSectionsFeatured: Featured,
  ComponentSectionsServices: Services,
  ComponentSectionsLookbookCarousel: LookbookCarousel,
  ComponentSectionsLookbook1: LookbookOne,
  ComponentSectionsLookbook2: LookbookTwo,
  ComponentSectionsLookbook3: LookbookThree,
  ComponentSectionsLookbook4: LookbookFour,
  ComponentSectionsContact: Contact,
  ComponentSectionsBrandComplete: BrandComplete,
  ComponentSectionsConcept: Concept,
  ComponentSectionsGiftCard: GiftCards,
};

// Display a section individually
const Section = (props: {
  sectionType: sectionTypeProps;
  pageType: string;
  index: number;
  pageID: number;
  searchParams?: { [key: string]: string | string[] | undefined };
}) => {
  // Prepare the component
  const SectionComponent = sectionComponents[props.sectionType.__typename];

  if (!SectionComponent) {
    return null;
  }

  // Display the section
  return (
    <SectionComponent
      pageID={props.pageID}
      index={props.index}
      pageType={props.pageType}
      searchParams={props.searchParams}
    />
  );
};

// Display the list of sections
const Sections = (props: {
  sections: [];
  pageID: number;
  pageType?: string;
  searchParams?: { [key: string]: string | string[] | undefined };
}) => {
  return (
    <>
      {/* Show the actual sections */}
      {props.sections?.map((section: sectionTypeProps, index) => (
        <Section
          sectionType={section}
          pageType={props.pageType ?? 'page'}
          index={index}
          pageID={props.pageID}
          key={`${section.__typename}${index}`}
          searchParams={props.searchParams}
        />
      ))}
    </>
  );
};

export default Sections;
