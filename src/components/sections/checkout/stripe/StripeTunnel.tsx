'use client';

import { Elements } from '@stripe/react-stripe-js';
import dynamic from 'next/dynamic';
import { redirect, useRouter } from 'next/navigation';
import { useState } from 'react';

import { ENUM_ORDER_STATUS } from '@/lib/interfaces';

import Discount from '@/components/elements/cart/Discount';
import AddressForm, {
  AddressFormType,
} from '@/components/elements/forms/AddressForm';
import ProgressSteps from '@/components/elements/steps/ProgressSteps';
import StepContainer from '@/components/elements/steps/StepContainer';
import StepIcon from '@/components/elements/steps/StepIcon';
import StepLabel from '@/components/elements/steps/StepLabel';
import StripePayment from '@/components/sections/checkout/stripe/StripePayment';

import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

import { updateOrderAddress } from '@/actions/strapi/order';

const AnimatePresence = dynamic(() =>
  import('framer-motion').then((mod) => mod.AnimatePresence)
);
const MotionDiv = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.div)
);

const loadStripe = async () => {
  return (await import('@stripe/stripe-js')).loadStripe(
    process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY as string
  );
};

const StripeTunnel = ({
  cartPage,
  completePage,
}: {
  cartPage?: string;
  completePage?: string;
}) => {
  const router = useRouter();

  const {
    setAddress,
    stripePaymentIntentId,
    stripeClientSecret,
    orderId,
    orderIntentId,
    refreshCart,
    totalPrice,
    discount,
  } = useCart((state) => state);

  const translations = useServer.getState().translations;

  const notify = useToaster((state) => state.notify);

  const [step, setStep] = useState(0);

  const elementsOptions = {
    clientSecret: stripeClientSecret ?? '',
    paymentMethodCreation: 'manual',
    theme: 'stripe',
  };

  const validateAddressForm = (data: AddressFormType) => {
    if (orderId && orderIntentId) {
      updateOrderAddress(data, orderId, orderIntentId, stripePaymentIntentId)
        .then(({ data: resData, error }) => {
          if (error) {
            switch (error.type) {
              case 'forbidden-update':
                notify(
                  2,
                  <p>
                    {translations.error?.order_forbidden_update ??
                      'Vous ne pouvez pas mettre à jour cette commande'}
                  </p>
                );
                break;

              case 'no-valid-cart':
                refreshCart();
                notify(
                  2,
                  <p>
                    {translations.error?.cart_not_equal_updated ??
                      'Panier invalide, il a été mis à jour'}
                  </p>
                );
                if (cartPage) router.push(cartPage);
                break;

              default:
                notify(
                  2,
                  <p>
                    {translations.error?.internal_server_error ??
                      "Une erreur interne s'est produite"}
                  </p>
                );
                break;
            }
            return;
          }

          setAddress(data);

          if (resData && resData.status === ENUM_ORDER_STATUS.succeeded) {
            //go to confirmation page with order id and order intent id in params
            router.push(
              `${completePage}?order_id=${orderId}&order_intent_id=${orderIntentId}` ??
                '/'
            );
            return;
          }

          setStep(1);
        })
        .catch(() => {
          notify(
            2,
            <p>
              {translations.error?.internal_server_error ??
                "Une erreur interne s'est produite"}
            </p>
          );
        });
      return;
    }
    notify(
      1,
      <p>
        {translations.payment?.no_order_intent ??
          'Aucune intention de commande, merci de réessayer'}
      </p>
    );
    if (cartPage) router.push(cartPage);
  };

  if (!orderId || !orderIntentId) {
    return redirect(cartPage ?? '/');
  }
  return (
    <AnimatePresence mode='wait' initial={false}>
      <MotionDiv
        key={step}
        initial={{ x: 1000, opacity: 0 }}
        animate={{ x: 0, opacity: 1 }}
        exit={{ x: -1000, opacity: 0 }}
        transition={{ duration: 0.5 }}
        className='w-full'
      >
        <ProgressSteps
          currentStep={step}
          className='mx-auto flex w-full max-w-lg flex-wrap items-center justify-between px-6 py-3 xs:py-5 md:p-6 lg:px-12'
        >
          {[...Array(3)].map((_, index) => (
            <StepContainer key={index} step={index}>
              <StepLabel
                label={
                  index === 0
                    ? translations.order?.billing_step ?? 'Billing'
                    : index === 1
                    ? translations.order?.payment_step ?? 'Payment'
                    : translations.order?.confirmation_step ?? 'Confirmation'
                }
              />
              <StepIcon step={index} />
            </StepContainer>
          ))}
        </ProgressSteps>

        {step === 0 && <AddressForm onSubmit={validateAddressForm} />}
        {step === 1 && (
          <Elements options={elementsOptions} stripe={loadStripe()}>
            <StripePayment
              cart_page={cartPage ?? '/'}
              return_url={completePage ?? '/'}
            />
          </Elements>
        )}

        <div className='flex flex-col items-center gap-3 bg-primary-200 p-3 xs:py-5 md:sticky md:p-6 lg:px-12 [&>*]:gap-6 [&>*]:md:gap-12'>
          <div className='border-dark flex items-center justify-between font-bold'>
            <p className='font-semibold first-letter:uppercase'>
              {translations.total}
            </p>
            <p className='h4 first-letter:uppercase'>{totalPrice} €</p>
          </div>

          {discount && <Discount />}
        </div>
      </MotionDiv>
    </AnimatePresence>
  );
};

export default StripeTunnel;
