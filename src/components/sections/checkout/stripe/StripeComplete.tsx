'use client';

import { PaymentIntentResult, Stripe } from '@stripe/stripe-js';
import { useRouter, useSearchParams } from 'next/navigation';
import { useCallback, useEffect, useRef, useState } from 'react';

import { umamiAnalytics } from '@/lib/helper';
import { ENUM_ORDER_STATUS, Order } from '@/lib/interfaces';

import ProgressSteps from '@/components/elements/steps/ProgressSteps';
import StepContainer from '@/components/elements/steps/StepContainer';
import StepIcon from '@/components/elements/steps/StepIcon';
import StepLabel from '@/components/elements/steps/StepLabel';

import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

import { queryOrderByIntentId } from '@/actions/strapi/order';

const StripeComplete = ({ cartPage }: { cartPage?: string }) => {
  const router = useRouter();
  const searchParams = useSearchParams();
  const translations = useServer.getState().translations;
  const notify = useToaster.getState().notify;

  const [status, setStatus] = useState(translations.loading ?? 'Loading ...');
  const [step, setStep] = useState(2);
  const [order, setOrder] = useState<Order>();
  const [umamiCalled, setUmamiCalled] = useState(false);
  const orderId = useRef(
    searchParams.get('order_id') ?? useCart.getState().orderId
  );
  const orderIntentId = useRef(
    searchParams.get('order_intent_id') ?? useCart.getState().orderIntentId
  );
  const stripePaymentIntentId = searchParams.get('payment_intent');
  const stripeClientSecret = useRef(
    searchParams.get('payment_intent_client_secret') ??
      useCart.getState().stripeClientSecret
  );

  const { emptyCart, setStripeClientSecret, setStripePaymentIntentId } =
    useCart((state) => state);

  const handleOrder = useCallback(
    (id: string, intent: string) => {
      queryOrderByIntentId(id, intent)
        .then((res) => {
          if (!res.data) {
            notify(
              1,
              <p>
                {translations.order?.error_fetching ??
                  "Une erreur s'est produite lors de la récupération de votre commande"}
              </p>
            );
            return;
          }
          switch (res.data[0].attributes.status) {
            case ENUM_ORDER_STATUS.succeeded:
              setStatus(
                translations.payment?.succeeded ?? 'Merci pour votre commande !'
              );
              setStep(3);
              emptyCart();
              break;

            case ENUM_ORDER_STATUS.processing:
              setStatus(
                translations.payment?.processing ??
                  'Merci, votre commande est en cours de traitement'
              );
              setStep(2);
              emptyCart();
              break;

            case ENUM_ORDER_STATUS.failed:
              notify(
                1,
                <p>
                  {translations.payment?.requires_payment_method_after_failed ??
                    'Votre paiement à échoué, vous pouvez réessayer'}
                </p>,
                8000
              );
              router.push(cartPage ?? '/');
              break;

            case ENUM_ORDER_STATUS.canceled:
              notify(
                1,
                <p>
                  {translations.payment?.canceled ??
                    'Votre paiement à été annulé'}
                </p>,
                8000
              );
              router.push(cartPage ?? '/');
              break;

            default:
              router.push(cartPage ?? '/');
              break;
          }
          setOrder(res.data[0]);
        })
        .catch(() =>
          notify(
            1,
            <p>
              {translations.order?.error_fetching ??
                "Une erreur s'est produite lors de la récupération de votre commande"}
            </p>
          )
        );

      if (!umamiCalled && intent) {
        umamiAnalytics('cart-payment-complete', {
          orderIntentId: intent,
          orderId: id,
        });
        setUmamiCalled(true);
      }
    },
    [
      cartPage,
      emptyCart,
      notify,
      router,
      translations.order?.error_fetching,
      translations.payment?.canceled,
      translations.payment?.processing,
      translations.payment?.requires_payment_method_after_failed,
      translations.payment?.succeeded,
      umamiCalled,
    ]
  );

  const handleStripe = useCallback(
    ({ paymentIntent }: PaymentIntentResult) => {
      switch (paymentIntent?.status) {
        case 'requires_payment_method':
          if (stripePaymentIntentId)
            setStripePaymentIntentId(stripePaymentIntentId);
          if (stripeClientSecret.current)
            setStripeClientSecret(stripeClientSecret.current);
          notify(
            1,
            <p>
              {translations.payment?.requires_payment_method_after_failed ??
                'Votre paiement à échoué, vous pouvez réessayer'}
            </p>,
            8000
          );
          router.push(cartPage ?? '/');
          break;

        default:
          if (!orderId.current || !orderIntentId.current) {
            router.push(cartPage ?? '/');
            return;
          }
          handleOrder(orderId.current, orderIntentId.current);
          break;
      }
    },
    [
      cartPage,
      handleOrder,
      notify,
      router,
      setStripeClientSecret,
      setStripePaymentIntentId,
      stripePaymentIntentId,
      translations.payment?.requires_payment_method_after_failed,
    ]
  );

  useEffect(() => {
    if (!orderId.current || !orderIntentId.current) {
      router.push(cartPage ?? '/');
      return;
    }

    async function fetchStripe(): Promise<Stripe | null> {
      const stripePromise = (await import('@stripe/stripe-js')).loadStripe(
        process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY as string
      );
      const stripe = await stripePromise;
      return stripe;
    }

    if (stripeClientSecret.current) {
      fetchStripe().then((stripe) => {
        if (!stripe) return;
        if (!stripeClientSecret.current) {
          router.push(cartPage ?? '/');
          return;
        }

        stripe
          .retrievePaymentIntent(stripeClientSecret.current)
          .then(handleStripe);
      });
    } else {
      handleOrder(orderId.current, orderIntentId.current);
    }
  }, [cartPage, handleOrder, handleStripe, router]);

  return (
    <>
      <ProgressSteps
        currentStep={step}
        className='mx-auto flex w-full max-w-lg flex-wrap items-center justify-between px-6 py-3 xs:py-5 md:p-6 lg:px-12'
      >
        {[...Array(3)].map((_, index) => (
          <StepContainer key={index} step={index}>
            <StepLabel
              label={
                index === 0
                  ? translations.order?.billing_step ?? 'Billing'
                  : index === 1
                  ? translations.order?.payment_step ?? 'Payment'
                  : translations.order?.confirmation_step ?? 'Confirmation'
              }
            />
            <StepIcon step={index} />
          </StepContainer>
        ))}
      </ProgressSteps>

      <div className='flex w-full justify-center p-3 xs:py-5 md:p-6 lg:px-12'>
        <div className='flex max-w-screen-xl flex-1 flex-col items-center gap-3 rounded-md bg-primary-200 p-3 md:p-6'>
          <h2 className='first-letter:uppercase'>{status}</h2>

          <p>
            {translations.order?.total_amount ?? 'Total amount'} :{' '}
            <span className='underline'>{order?.attributes.amount}€</span>
          </p>
          <p>
            {translations.order?.subtotal ?? 'Subtotal'} :{' '}
            <span className='underline'>{order?.attributes.subtotal}€</span>
          </p>

          <span className='prose max-w-full overflow-auto md:prose-md'>
            <table>
              <thead>
                <tr>
                  <th className='first-letter:uppercase'>
                    {translations.product ?? 'Product'}
                  </th>
                  <th className='first-letter:uppercase'>
                    {translations.color ?? 'Color'}
                  </th>
                  <th className='first-letter:uppercase'>
                    {translations.size ?? 'Size'}
                  </th>
                  <th className='first-letter:uppercase'>
                    {translations.price ?? 'Price'}
                  </th>
                </tr>
              </thead>
              <tbody>
                {order?.attributes.products.map((product, index) => (
                  <tr key={`${product.title}-${index}`}>
                    <td className='first-letter:uppercase'>{product.title}</td>
                    <td className='first-letter:uppercase'>{product.color}</td>
                    <td className='first-letter:uppercase'>{product.size}</td>
                    <td className='first-letter:uppercase'>
                      {product.price}€ {product.qty > 1 && `x ${product.qty}`}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </span>
        </div>
      </div>
    </>
  );
};

export default StripeComplete;
