'use client';

import {
  PaymentElement,
  useElements,
  useStripe,
} from '@stripe/react-stripe-js';
import { StripeError } from '@stripe/stripe-js';
import { useRouter } from 'next/navigation';
import { FormEvent, useState } from 'react';

import { umamiAnalytics } from '@/lib/helper';

import Button from '@/components/elements/buttons/Button';

import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

import { deploymentURL } from '@/constant/env';

const StripePayment = ({
  cart_page,
  return_url,
}: {
  cart_page: string;
  return_url: string;
}) => {
  const router = useRouter();

  const {
    stripeClientSecret,
    stripePaymentIntentId,
    refreshCart,
    orderId,
    orderIntentId,
  } = useCart((state) => state);

  const notify = useToaster((state) => state.notify);

  const translations = useServer.getState().translations;

  const stripe = useStripe();
  const elements = useElements();

  const [message, setMessage] = useState<string>();
  const [isLoading, setIsLoading] = useState(false);

  const handleErrors = (
    error: StripeError | { type: string; message?: string }
  ) => {
    switch (error.type) {
      case 'card_error':
        setMessage(error.message);
        break;
      case 'validation_error':
        setMessage(error.message);
        break;
      case 'rate_limit_error':
        setMessage(
          translations.payment?.rate_limit_error ??
            'Trop de demandes, merci de réessayer plus tard'
        );
        break;
      case 'no-payment-intent':
        notify(
          2,
          <p>
            {translations.payment?.no_payment_intent ??
              'Aucune intention de paiement, merci de réessayer'}
          </p>
        );
        router.push(cart_page);
        break;
      case 'no-payment-method':
        setMessage(
          translations.payment?.requires_payment_method ??
            'Vous devez fournir un moyen de paiement'
        );
        break;
      case 'cannot-confirm-order':
        setMessage(
          translations.payment?.cannot_confirm_order ??
            'Impossible de confirmer cette commande'
        );
        break;
      case 'no-valid-cart':
        refreshCart();
        notify(
          2,
          <p>
            {translations.error?.cart_not_equal_updated ??
              'Panier invalide, il a été mis à jour'}
          </p>
        );
        router.push(cart_page);
        break;
      default:
        setMessage(
          translations.payment?.unexpected ??
            "Une erreur innatendue s'est produite, merci de réessayer"
        );
        break;
    }
  };

  const handleSubmit = async (e: FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (!stripe || !elements) {
      return;
    }
    setIsLoading(true);

    umamiAnalytics('cart-payment-stripe', {
      paymentIntentId: stripePaymentIntentId,
    });

    const { error: submitError } = await elements.submit();
    if (submitError) {
      handleErrors(submitError);
      setIsLoading(false);
      return;
    }

    const { error, paymentMethod } = await stripe.createPaymentMethod({
      elements,
    });

    if (error) {
      handleErrors(error);
      setIsLoading(false);
      return;
    }

    const res = await fetch(`${deploymentURL}/api/confirm-intent`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      mode: 'same-origin',
      body: JSON.stringify({
        payment_intent_id: stripePaymentIntentId,
        payment_method: paymentMethod.id,
        return_url: `${deploymentURL}${return_url}?order_id=${orderId}&order_intent_id=${orderIntentId}`,
      }),
      cache: 'no-store',
    });

    const data = await res.json();

    if (data.error) {
      handleErrors(data.error);
      setIsLoading(false);
      return;
    } else if (data.status === 'requires_action') {
      // Use Stripe.js to handle the required next action
      const { error } = await stripe.handleNextAction({
        clientSecret: stripeClientSecret ?? data.client_secret,
      });
      if (error) {
        handleErrors(data.error);
        return;
      }
    }

    router.push(
      `${return_url}?order_id=${orderId}&order_intent_id=${orderIntentId}`
    );
    return;
  };

  return (
    <div className='flex w-full justify-center p-3 xs:py-5 md:p-6 lg:px-12'>
      <div className='flex w-full max-w-screen-xl flex-col gap-4 md:gap-8'>
        <PaymentElement options={{ layout: 'accordion' }} />
        <div className='mx-auto'>
          <Button isLoading={isLoading} onClick={handleSubmit} className=''>
            {translations.payment.pay_btn}
          </Button>
          <p>{message}</p>
        </div>
      </div>
    </div>
  );
};

export default StripePayment;
