'use client';

import { useRouter } from 'next/navigation';
import { useState } from 'react';

import { umamiAnalytics } from '@/lib/helper';

import CartForm from '@/components/sections/checkout/cart/CartForm';
import CartItems from '@/components/sections/checkout/cart/CartItems';
import CartPrice from '@/components/sections/checkout/cart/CartPrice';

import useStore from '@/store';
import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

import createOrUpdatePaymentIntent from '@/actions/stripe/paymentIntents';

const Cart = ({ checkoutPage }: { checkoutPage?: string }) => {
  const router = useRouter();

  const cartItems = useStore(useCart, (state) => state.cartItems);
  const cartTotalPrice = useStore(useCart, (state) => state.totalPrice) ?? 0;
  const {
    orderId,
    setOrderId,
    orderIntentId: order_intent_id,
    setOrderIntentId,
    stripePaymentIntentId: payment_intent_id,
    setStripePaymentIntentId: setPaymentIntentId,
    setStripeClientSecret: setClientSecret,
    refreshCart,
    discount,
  } = useCart((state) => state);

  const { translations, locale } = useServer((state) => state);

  const notify = useToaster.getState().notify;

  const [validated, setValidated] = useState(false);

  const validateCart = (discountId?: number) => {
    if (validated || !cartItems?.length) return;
    setValidated(true);
    const itemsToValidate = cartItems.map((item) => {
      const sizeId = item.product.attributes.sizes.find(
        (el) => el.size === item.size
      );
      return {
        id: item.product.id,
        title: item.product.attributes.title,
        price: item.price,
        qty: item.qty,
        size: item.size,
        sizeId: sizeId?.id ?? 0,
        color: item.color,
        type: item.type,
        gift_card: item.gift_card,
      };
    });

    umamiAnalytics('cart-validate', {
      items: itemsToValidate,
      totalPrice: cartTotalPrice,
    });

    createOrUpdatePaymentIntent(
      itemsToValidate,
      locale,
      orderId,
      order_intent_id,
      discountId ?? discount?.id,
      payment_intent_id
    )
      .then(({ error, data }) => {
        if (error) {
          switch (error.type) {
            case 'insufficient-quantity-available':
              if (error.message) {
                const item = JSON.parse(error.message);
                notify(
                  1,
                  <p>
                    {translations.error?.insufficient_quantity_available_for ??
                      'Quantité disponible insufisante pour '}{' '}
                    {item.title} - {item.color}
                  </p>,
                  8000
                );
              } else {
                notify(
                  1,
                  <p>
                    {translations.error?.insufficient_quantity_available ??
                      'Quantité disponible insufisante'}
                  </p>,
                  8000
                );
              }
              break;
            case 'not-equal':
              refreshCart();
              notify(
                2,
                <p>
                  {translations.error?.cart_not_equal_updated ??
                    'Panier invalide, il a été mis à jour'}
                </p>,
                8000
              );
              break;
            case 'internal-server-error':
              notify(
                2,
                <p>
                  {translations.error?.internal_server_error ??
                    "Une erreur interne s'est produite"}
                </p>
              );
              break;
            case 'cart-minimum-spend':
              notify(
                2,
                <p>
                  {translations.cart?.minmum_spend ??
                    'Votre sous total, doit être supérieur à 50cents'}
                </p>
              );
              break;

            case 'dicount-not-found':
              notify(
                1,
                <p>
                  {translations.error?.discount_not_found ??
                    "Ce code de réduction n'existe pas"}
                </p>
              );
              break;
            case 'discount-not-started':
              notify(
                1,
                <p>
                  {translations.error?.discount_not_started ??
                    "Ce code de réduction n'as pas encore commencé"}
                </p>
              );
              break;
            case 'discount-expired':
              notify(
                1,
                <p>
                  {translations.error?.discount_expired ??
                    'Ce code de réduction a expiré'}
                </p>
              );
              break;
            case 'discount-max-uses':
              notify(
                1,
                <p>
                  {translations.error?.discount_max_uses ??
                    "Ce code de réduction a atteint son nombre maximum d'utilisations"}
                </p>
              );
              break;
            case 'discount-no-value-left':
              notify(
                1,
                <p>
                  {translations.error?.discount_no_value_left ??
                    "Ce code de réduction n'a plus de valeur restante"}
                </p>
              );
              break;
            case 'discount-minimum-spend':
              notify(
                1,
                <p>
                  {translations.error?.discount_minimum_spend.replace(
                    '${min}',
                    error.data.min
                  ) ?? 'Votre panier doit avoir une valeur minimum de ${min} €'}
                </p>
              );
              break;
            case 'discount-maximum-spend':
              notify(
                1,
                <p>
                  {translations.error?.discount_maximum_spend.replace(
                    '${max}',
                    error.data.max
                  ) ?? 'Votre panier doit avoir une valeur maximum de ${max} €'}
                </p>
              );
              break;

            default:
              refreshCart();
              notify(
                2,
                <p>
                  {translations.error?.internal_server_error ??
                    "Une erreur interne s'est produite"}
                </p>
              );
              break;
          }
          setValidated(false);
          return;
        }
        if (data && checkoutPage) {
          if (data.order_id) setOrderId(data.order_id);
          if (data.order_intent_id) setOrderIntentId(data.order_intent_id);
          setPaymentIntentId(data.payment_intent_id);
          setClientSecret(data.client_secret);
          router.push(checkoutPage);
          return;
        }
        setValidated(false);
        return;
      })
      .catch(() => {
        notify(2, <p>{translations.error.internal_server_error}</p>);
        setValidated(false);
      });
  };

  return (
    <>
      <CartItems />

      <div className='shrink-0 md:relative md:w-1/3'>
        <div className='top-28 flex w-full flex-col gap-3 rounded-md bg-primary-200 p-3 md:sticky'>
          <CartPrice />
          <CartForm validateCart={validateCart} submitted={validated} />
        </div>
      </div>
    </>
  );
};

export default Cart;
