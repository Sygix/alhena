import { gql, QueryContentComponent } from '@/lib/graphql';

import Cart from '@/components/sections/checkout/cart/Cart';

import { useServer } from '@/store/serverStore';

const ComponentSectionsCart = gql`
  fragment sectionsCart on ComponentSectionsCart {
    checkout_page {
      data {
        attributes {
          slug
        }
      }
    }
  }
`;

const CartSection = async ({
  pageID,
  index,
  pageType,
}: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;

  const { data } = await QueryContentComponent(
    locale,
    pageID,
    pageType,
    [pageType],
    ComponentSectionsCart,
    'sectionsCart'
  );
  const { checkout_page } = data.attributes.content[index];
  return (
    <div className='flex w-full max-w-screen-3xl flex-col gap-4 p-3 xs:py-5 md:flex-row md:gap-8 md:p-6 lg:px-12'>
      <Cart checkoutPage={`/${locale}/${checkout_page.data.attributes.slug}`} />
    </div>
  );
};

export default CartSection;
