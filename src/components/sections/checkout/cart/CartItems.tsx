'use client';

import { CartItemCard } from '@/components/elements/cards/CartItemCard';

import useStore from '@/store';
import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';

const CartItems = () => {
  const cartItems = useStore(useCart, (state) => state.cartItems);
  const translations = useServer.getState().translations;

  return (
    <div className='flex w-full shrink flex-col items-center justify-center gap-3 xs:gap-5 md:gap-6 lg:gap-12'>
      {cartItems?.map((item, index) => (
        <CartItemCard
          key={`item-${item.size}-${index}`}
          cartItem={item}
          displayQtyBtn={true}
        />
      ))}
      {!cartItems?.length && (
        <p className='h3 p-6 first-letter:uppercase'>
          {translations.cart?.empty}
        </p>
      )}
    </div>
  );
};

export default CartItems;
