'use client';

import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { InferType, object, string } from 'yup';

import { umamiAnalytics } from '@/lib/helper';

import Button from '@/components/elements/buttons/Button';
import IconButton from '@/components/elements/buttons/IconButton';
import FormInput from '@/components/elements/forms/molecules/FormInput';

import useStore from '@/store';
import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

import getDiscountFromCode from '@/actions/strapi/discount';

const schema = object({
  code: string(),
}).required();

type DiscountFormType = InferType<typeof schema>;

const CartForm = ({
  validateCart,
  submitted,
}: {
  validateCart: (discountId?: number) => void;
  submitted: boolean;
}) => {
  const translations = useServer.getState().translations;
  const cartItems = useStore(useCart, (state) => state.cartItems);
  const totalPrice = useStore(useCart, (state) => state.totalPrice) ?? 0;
  const { emptyCart, setDiscount, discount } = useCart((state) => state);
  const notify = useToaster.getState().notify;

  const applyDiscount = async (formData: DiscountFormType) => {
    if (!formData.code) return;
    const { data, error } = await getDiscountFromCode(
      formData.code,
      totalPrice
    );

    switch (error?.type) {
      case 'discount-not-found':
        notify(
          1,
          <p>
            {translations.error?.discount_not_found ??
              "Ce code de réduction n'existe pas"}
          </p>
        );
        break;
      case 'discount-not-started':
        notify(
          1,
          <p>
            {translations.error?.discount_not_started ??
              "Ce code de réduction n'as pas encore commencé"}
          </p>
        );
        break;
      case 'discount-expired':
        notify(
          1,
          <p>
            {translations.error?.discount_expired ??
              'Ce code de réduction a expiré'}
          </p>
        );
        break;
      case 'discount-max-uses':
        notify(
          1,
          <p>
            {translations.error?.discount_max_uses ??
              "Ce code de réduction a atteint son nombre maximum d'utilisations"}
          </p>
        );
        break;
      case 'discount-no-value-left':
        notify(
          1,
          <p>
            {translations.error?.discount_no_value_left ??
              "Ce code de réduction n'a plus de valeur restante"}
          </p>
        );
        break;
      case 'discount-minimum-spend':
        notify(
          1,
          <p>
            {`${
              translations.error?.discount_minimum_spend ??
              'Votre panier doit avoir une valeur minimum de ${min}€'
            }`.replace('${min}', error.data.min)}
          </p>
        );
        break;
      case 'discount-maximum-spend':
        notify(
          1,
          <p>
            {`${
              translations.error?.discount_maximum_spend ??
              'Votre panier doit avoir une valeur maximum de ${max}€'
            }`.replace('${max}', error.data.max)}
          </p>
        );
        break;
      default:
        break;
    }

    if (data?.attributes.code) {
      const { code, type, value } = data.attributes;
      setDiscount(data);
      umamiAnalytics('cart-discount', {
        discount: code,
        discountType: type,
        discountValue: value,
        cartPrice: totalPrice,
      });
      notify(
        0,
        <p>
          {translations.toast?.discount_applied ?? 'Code de réduction appliqué'}
        </p>
      );
    }

    return data?.id;
  };

  const submitCartForm = (formData: DiscountFormType) => {
    if (formData.code && formData.code !== discount?.attributes.code)
      applyDiscount(formData).then((id) => {
        validateCart(id);
      });
    else validateCart();
  };

  const { handleSubmit, register, getValues } = useForm<DiscountFormType>({
    resolver: yupResolver(schema),
    mode: 'onChange',
    shouldUnregister: true,
  });

  return (
    <form
      onSubmit={handleSubmit(submitCartForm)}
      className='flex flex-col gap-3'
    >
      <div className='flex flex-row justify-between rounded-full bg-white text-carbon-900'>
        <FormInput
          name='code'
          label='code'
          placeholder={
            translations.cart?.add_discount_code ??
            'Ajouter un code promo/carte cadeau'
          }
          className='shrink grow rounded-l-full border-0'
          register={register}
        />
        <IconButton
          onClick={() => applyDiscount(getValues())}
          icon='mi:chevron-right'
          variant='ghost'
          className='text-2xl text-carbon-900'
        />
      </div>

      <div className='flex justify-between gap-3'>
        <Button
          variant='outline'
          onClick={() => {
            umamiAnalytics('cart-empty');
            emptyCart();
          }}
          className='bg-transparent'
        >
          <p className='first-letter:uppercase'>
            {translations.cart?.empty_btn}
          </p>
        </Button>
        <Button
          disabled={!cartItems?.length}
          isLoading={submitted}
          variant='dark'
          type='submit'
        >
          <p className='first-letter:uppercase'>
            {translations.cart?.validate_cart}
          </p>
        </Button>
      </div>
    </form>
  );
};

export default CartForm;
