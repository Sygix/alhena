'use client';

import Discount from '@/components/elements/cart/Discount';

import useStore from '@/store';
import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';

const CartPrice = () => {
  const translations = useServer.getState().translations;
  const discount = useCart.getState().discount;
  const cartTotalPrice = useStore(useCart, (state) => state.totalPrice) ?? 0;

  return (
    <>
      <div className='border-dark flex items-center justify-between font-bold'>
        <p className='font-semibold first-letter:uppercase'>
          {translations.total}
        </p>
        <p className='h4 first-letter:uppercase'>{cartTotalPrice} €</p>
      </div>

      {discount && <Discount />}
    </>
  );
};

export default CartPrice;
