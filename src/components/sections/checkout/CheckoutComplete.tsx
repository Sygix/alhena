import { gql, QueryContentComponent } from '@/lib/graphql';

import StripeComplete from '@/components/sections/checkout/stripe/StripeComplete';

import { useServer } from '@/store/serverStore';

const ComponentSectionsCheckoutComplete = gql`
  fragment sectionsCheckoutComplete on ComponentSectionsCheckoutComplete {
    cart_page {
      data {
        attributes {
          slug
        }
      }
    }
  }
`;

const CheckoutComplete = async ({
  pageID,
  index,
  pageType,
}: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const paymentProvider = useServer.getState().paymentProvider;

  const { data } = await QueryContentComponent(
    locale,
    pageID,
    pageType,
    [pageType],
    ComponentSectionsCheckoutComplete,
    'sectionsCheckoutComplete'
  );
  const { cart_page } = data.attributes.content[index];
  if (paymentProvider === 'STRIPE')
    return (
      <StripeComplete
        cartPage={`/${locale}/${cart_page.data.attributes.slug}`}
      />
    );
  return null;
};

export default CheckoutComplete;
