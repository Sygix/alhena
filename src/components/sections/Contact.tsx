import { gql, QueryContentComponent } from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';
import { Media } from '@/lib/interfaces';

import ContactForm from '@/components/elements/forms/ContactForm';

import { useServer } from '@/store/serverStore';

const ComponentSectionsContact = gql`
  fragment sectionsContact on ComponentSectionsContact {
    h1
    background_image {
      data {
        attributes {
          alternativeText
          caption
          height
          width
          mime
          url
        }
      }
    }
    description
    title
    field_name
    field_email
    field_subject
    field_message
    field_consent
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        h1: string;
        background_image: {
          data?: Media;
        };
        description: string;
        title: string;
        field_name: string;
        field_email: string;
        field_subject: string;
        field_message: string;
        field_consent: string;
      }[];
    };
  };
};

const Contact = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const translations = useServer.getState().translations;
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsContact,
    'sectionsContact'
  );
  const {
    h1,
    background_image,
    description,
    title,
    field_name,
    field_email,
    field_subject,
    field_message,
    field_consent,
  } = content[props.index];

  return (
    <section
      className='relative p-3 md:p-6 w-full h-full flex-1 flex flex-col md:flex-row gap-3 items-center before:content-[""] before:absolute before:top-0 before:left-0 before:h-full before:w-full before:bg-carbon-900/20'
      style={{
        backgroundImage: `url(${MediaUrl(
          background_image.data?.attributes.url ?? ''
        )})`,
        backgroundPosition: 'center center',
        backgroundSize: 'cover',
      }}
    >
      <div className='z-10 text-white py-12 md:py-0 md:w-1/2 flex flex-col items-center justify-center'>
        <h1>{h1}</h1>
        <p className='max-w-lg text-center'>{description}</p>
      </div>

      <div className='z-10 text-white bg-primary-200 shadow-lg rounded-md p-3 md:w-1/2 flex flex-col items-center gap-3'>
        <h2 className='h3 text-carbon-900'>{title}</h2>
        <ContactForm
          className='w-full justify-center flex-wrap gap-3'
          submitText={translations.submit}
          placeholders={{
            name: field_name,
            email: field_email,
            subject: field_subject,
            message: field_message,
            consent: field_consent,
          }}
        />
      </div>
    </section>
  );
};

export default Contact;
