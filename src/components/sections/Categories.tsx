import Link from 'next/link';

import { gql, QueryContentComponent } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';
import { Category } from '@/lib/interfaces';

import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsCategories = gql`
  fragment sectionsCategories on ComponentSectionsCategories {
    categories {
      data {
        id
        attributes {
          title
          slug
          image {
            data {
              id
              attributes {
                caption
                alternativeText
                width
                height
                url
                name
              }
            }
          }
        }
      }
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        categories: {
          data: Category[];
        };
      }[];
    };
  };
};

const Categories = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType, 'category'],
    ComponentSectionsCategories,
    'sectionsCategories'
  );
  const { categories } = content[props.index];

  return (
    <section className='grid w-fit max-w-screen-2xl grid-cols-2 gap-3 p-3 after:hidden md:gap-6 md:p-6 lg:px-12'>
      {categories.data.map((cat) => (
        <Link
          key={cat.id}
          href={includeLocaleLink(`/${cat.attributes.slug}`)}
          title={cat.attributes.title}
          className='relative aspect-square h-fit'
        >
          {cat.attributes.image.data && (
            <NextImage
              className='flex h-full w-full justify-center justify-self-center overflow-hidden'
              imgClassName='w-full h-full object-center object-cover hover:scale-110 duration-300 transition-transform'
              width={cat.attributes.image.data.attributes.width}
              height={cat.attributes.image.data.attributes.height}
              src={MediaUrl(cat.attributes.image.data.attributes.url)}
              title={cat.attributes.image.data.attributes.name}
              alt={cat.attributes.image.data.attributes.alternativeText ?? ''}
              sizes='45vw'
            />
          )}
          <h2 className='xl:h0 absolute left-1/2 top-3 -translate-x-1/2 uppercase mix-blend-darken'>
            {cat.attributes.title}
          </h2>
        </Link>
      ))}
    </section>
  );
};

export default Categories;
