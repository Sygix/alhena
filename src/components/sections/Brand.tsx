import { MDXRemote } from 'next-mdx-remote/rsc';

import clsxm from '@/lib/clsxm';
import { gql, QueryContentComponent } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';
import { LinkInterface, Media } from '@/lib/interfaces';

import Link from '@/components/elements/links';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsBrand = gql`
  fragment sectionsBrand on ComponentSectionsBrand {
    text
    media {
      data {
        id
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    link {
      name
      href
      open_new_tab
      icon
      style
      direction
      variant
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        text: string;
        media: {
          data: Media;
        };
        link: LinkInterface;
      }[];
    };
  };
};

const Brand = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsBrand,
    'sectionsBrand'
  );
  const { text, media, link } = content[props.index];

  return (
    <section className='m-3 grid max-w-screen-xl grid-cols-1 grid-rows-2 gap-3 after:hidden md:m-6 md:grid-cols-2 md:grid-rows-1 md:gap-6 lg:mx-12'>
      <div className='flex flex-col items-center justify-center gap-3'>
        <span className='prose prose-carbon md:prose-md'>
          <MDXRemote source={text} />
        </span>
        <Link
          href={includeLocaleLink(link.href)}
          title={link.name}
          openNewTab={link.open_new_tab}
          style={link.style}
          variant={link.variant}
          icon={link.icon}
          direction={link.direction}
          className={clsxm(
            link.style === 'button'
              ? 'border-2 border-white text-lg font-semibold'
              : '',
            link.direction === 'left' ? 'pl-1' : 'pr-1'
          )}
          iconClassName={clsxm(
            link.direction === 'left' ? 'mr-3 text-2xl' : 'ml-3 text-2xl'
          )}
          data-analytics-event='brand-link'
          data-analytics-event-link={link.href}
          data-analytics-event-name={link.name.toLocaleLowerCase()}
        >
          {link.name}
        </Link>
      </div>
      <NextImage
        className='flex w-full max-w-lg justify-center justify-self-center'
        imgClassName='w-full h-full object-center object-cover rounded-md'
        width={media.data.attributes.width}
        height={media.data.attributes.height}
        src={MediaUrl(media.data.attributes.url)}
        title={media.data.attributes.name}
        alt={media.data.attributes.alternativeText ?? ''}
        sizes='30vw'
      />
    </section>
  );
};

export default Brand;
