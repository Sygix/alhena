import { gql, QueryContentComponent } from '@/lib/graphql';

import DynamicIcon from '@/components/elements/DynamicIcon';

import { useServer } from '@/store/serverStore';

const ComponentSectionsServices = gql`
  fragment sectionsServices on ComponentSectionsServices {
    services {
      id
      title
      icon
      description
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        services: {
          id: number;
          title?: string;
          icon?: string;
          description?: string;
        }[];
      }[];
    };
  };
};

const Services = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsServices,
    'sectionsServices'
  );
  const { services } = content[props.index];

  return (
    <section className='p-3 md:p-6 lg:px-12 w-full max-w-screen-3xl'>
      <div className='w-full py-8 border-y-2 border-carbon-900 grid grid-cols-1 xs:grid-cols-2 lg:grid-cols-4 gap-8 justify-center'>
        {services.map((service) => (
          <div
            key={service.id}
            className='flex flex-col items-center gap-3 md:gap-6 text-center'
          >
            {service.title && <h3>{service.title}</h3>}
            {service.icon && (
              <DynamicIcon
                icon={service.icon}
                className='w-6 h-6 lg:w-8 lg:h-8 hover:animate-spin'
              />
            )}
            {service.description && (
              <p className='text-carbon-700'>{service.description}</p>
            )}
          </div>
        ))}
      </div>
    </section>
  );
};

export default Services;
