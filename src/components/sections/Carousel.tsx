import { gql, QueryContentComponent } from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';
import { Media } from '@/lib/interfaces';

import CarouselItem from '@/components/elements/carousel/CarouselItem';
import EmblaCarousel from '@/components/elements/carousel/EmblaCarousel';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsCarousel = gql`
  fragment sectionsCarousel on ComponentSectionsCarousel {
    items {
      id
      title_right
      title_left
      image {
        data {
          attributes {
            alternativeText
            caption
            url
            width
            height
          }
        }
      }
      href
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        items: {
          id: number;
          title_left: string;
          title_right: string;
          description: string;
          image: {
            data: Media;
          };
          href: string;
        }[];
      }[];
    };
  };
};

const Carousel = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsCarousel,
    'sectionsCarousel'
  );
  const { items } = content[props.index];

  return (
    <section className='mb-3 w-full md:mb-6'>
      <EmblaCarousel
        className='aspect-[2/3] h-full max-h-[100svh] w-full shrink-0 md:aspect-video md:max-h-[75svh]'
        containerClassName='w-full h-full'
        options={{ loop: true, containScroll: 'trimSnaps' }}
        autoplay={true}
        autoplayOptions={{
          delay: 5000,
          stopOnInteraction: false,
        }}
      >
        {items.map((item, index) => (
          <CarouselItem
            index={index}
            key={item.id}
            className='relative mr-3 h-full w-full overflow-hidden lg:mr-6'
          >
            <NextImage
              className='flex h-full w-full justify-center'
              imgClassName='w-full h-full object-center object-cover'
              width={item.image.data.attributes.width}
              height={item.image.data.attributes.height}
              src={MediaUrl(item.image.data.attributes.url)}
              title={item.image.data.attributes.name}
              alt={item.image.data.attributes.alternativeText ?? ''}
              sizes='100vw (min-width: 768px) 70vw'
              quality={100}
              priority={index === 0}
            />
            <h3 className='h1 absolute bottom-3 left-3 rotate-180 uppercase [writing-mode:vertical-lr]'>
              {item.title_left}
            </h3>
            <h3 className='h1 absolute right-3 top-3 rotate-180 uppercase [writing-mode:vertical-lr]'>
              {item.title_right}
            </h3>
          </CarouselItem>
        ))}
      </EmblaCarousel>
    </section>
  );
};

export default Carousel;
