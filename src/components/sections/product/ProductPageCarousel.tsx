import { MediaUrl } from '@/lib/helper';
import { Media } from '@/lib/interfaces';

import CarouselItem from '@/components/elements/carousel/CarouselItem';
import CarouselThumbs from '@/components/elements/carousel/CarouselThumbs';
import EmblaCarousel from '@/components/elements/carousel/EmblaCarousel';
import NextButton from '@/components/elements/carousel/NextButton';
import PrevButton from '@/components/elements/carousel/PrevButton';
import NextImage from '@/components/NextImage';

const ProductPageCarousel = ({ medias }: { medias: { data: Media[] } }) => {
  const otherChildrens = (
    <div className='relative'>
      <PrevButton className='absolute left-0 top-1/2 z-10 -translate-y-1/2 text-carbon-900' />
      <CarouselThumbs
        className='relative'
        containerClassName='h-fit'
        thumbClassName='h-fit w-1/3 lg:w-1/5 mr-3 lg:mr-6 first-of-type:ml-12 last-of-type:mr-12'
        options={{
          containScroll: 'keepSnaps',
          dragFree: true,
        }}
      >
        {medias.data.map((media, index) => {
          if (media.attributes.mime.startsWith('video/')) {
            return (
              <video
                key={media.attributes.url + index}
                autoPlay={false}
                muted={true}
                loop={true}
                controls={false}
                width={media.attributes.width}
                height={media.attributes.height}
                className='aspect-[4/3] w-full min-w-0 flex-[0_0_100%] rounded-lg'
                webkit-playsinline='true'
                playsInline
              >
                <source
                  src={MediaUrl(media.attributes.url)}
                  type={media.attributes.mime}
                />
                <meta itemProp='name' content={media.attributes.name} />
                <meta
                  itemProp='description'
                  content={media.attributes.alternativeText}
                />
              </video>
            );
          }
          if (media.attributes.mime.startsWith('image/')) {
            return (
              <NextImage
                key={media.attributes.url + index}
                className='aspect-[4/3] w-full'
                imgClassName='w-full h-full object-cover object-center rounded-lg'
                width={media.attributes.width}
                height={media.attributes.height}
                src={MediaUrl(media.attributes.url)}
                title={media.attributes.name}
                alt={media.attributes.alternativeText ?? ''}
                sizes='100vw (min-width: 768px) 70vw'
                priority={index === 0}
              />
            );
          }
        })}
      </CarouselThumbs>
      <NextButton className='absolute right-0 top-1/2 z-10 -translate-y-1/2 text-carbon-900' />
    </div>
  );

  return (
    <EmblaCarousel
      className='w-full'
      containerClassName='items-start gap-3 lg:gap-6 transition-height duration-300 max-h-[75svh]'
      autoheight={true}
      otherChildrens={medias.data.length > 1 && otherChildrens}
    >
      {medias.data.map((media, index) => (
        <CarouselItem
          key={media.id}
          index={index}
          className='h-full w-full'
          wrapperClassNames=''
        >
          {media.attributes.mime.startsWith('video/') ? (
            <video
              key={media.attributes.url + index}
              autoPlay={true}
              muted={true}
              loop={true}
              controls={true}
              width={media.attributes.width}
              height={media.attributes.height}
              className='mr-1 aspect-video max-h-screen w-full min-w-0 flex-[0_0_100%]'
              webkit-playsinline='true'
              playsInline
            >
              <source
                src={MediaUrl(media.attributes.url)}
                type={media.attributes.mime}
              />
              <meta itemProp='name' content={media.attributes.name} />
              <meta
                itemProp='description'
                content={media.attributes.alternativeText}
              />
            </video>
          ) : (
            <NextImage
              className='flex h-full w-full justify-center'
              imgClassName='w-full h-full object-contain object-center'
              width={media.attributes.width}
              height={media.attributes.height}
              src={MediaUrl(media.attributes.url)}
              title={media.attributes.name}
              alt={media.attributes.alternativeText ?? ''}
              sizes='100vw (min-width: 768px) 70vw'
              quality={100}
              priority={index === 0}
            />
          )}
        </CarouselItem>
      ))}
    </EmblaCarousel>
  );
};

export default ProductPageCarousel;
