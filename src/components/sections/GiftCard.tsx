import { gql, QueryContentComponent } from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';
import { Media, Product } from '@/lib/interfaces';

import GiftCardDropDown from '@/components/elements/cart/GiftCardDropDown';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsGiftCards = gql`
  fragment sectionsGiftCards on ComponentSectionsGiftCard {
    title
    description
    image {
      data {
        attributes {
          name
          alternativeText
          width
          height
          url
        }
      }
    }
    gift_cards {
      data {
        id
        attributes {
          title
          slug
          price
          sale_price
          date_on_sale_from
          date_on_sale_to
          sizes {
            id
            size
            quantity
          }
          medias {
            data {
              attributes {
                name
                alternativeText
                width
                height
                url
              }
            }
          }
          gift_card
          global_quantity
        }
      }
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        title?: string;
        description?: string;
        image?: {
          data: Media;
        };
        gift_cards: {
          data: Product[];
        };
      }[];
    };
  };
};

const GiftCards = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsGiftCards,
    'sectionsGiftCards'
  );
  const { title, description, image, gift_cards } = content[props.index];

  return (
    <section className='m-3 flex flex-col items-center gap-3 text-center md:m-6 md:flex-row md:gap-6 lg:mx-12'>
      <div className='flex max-w-3xl flex-col items-center gap-3 md:gap-6'>
        <h2>{title}</h2>
        <p className='text-gray-500'>{description}</p>
        <GiftCardDropDown products={gift_cards.data} />
      </div>
      {image && (
        <NextImage
          className='order-first flex h-full w-full max-w-lg justify-center justify-self-center md:order-none'
          imgClassName='w-full h-full object-center object-cover rounded-md'
          width={image.data.attributes.width}
          height={image.data.attributes.height}
          src={MediaUrl(image.data.attributes.url)}
          title={image.data.attributes.name}
          alt={image.data.attributes.alternativeText ?? ''}
          sizes='100vw, (min-width: 768px) 50vw'
        />
      )}
    </section>
  );
};

export default GiftCards;
