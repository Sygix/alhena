import { gql, QueryContentComponent } from '@/lib/graphql';
import { Media } from '@/lib/interfaces';

import XConcept from '@/components/elements/XConcept';

import { useServer } from '@/store/serverStore';

const ComponentSectionsConcept = gql`
  fragment sectionsConcept on ComponentSectionsConcept {
    title
    steps {
      id
      description
      image {
        data {
          attributes {
            name
            alternativeText
            caption
            width
            height
            url
            mime
          }
        }
      }
      accent_color
      rotate
      mobile_col_start
      mobile_row_start
      tablet_col_start
      tablet_row_start
      desktop_col_start
      desktop_row_start
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        title: string;
        steps: {
          id: number;
          description?: string;
          image: {
            data: Media;
          };
          accent_color: string;
          rotate: number;
          mobile_col_start?: number;
          mobile_row_start?: number;
          tablet_col_start?: number;
          tablet_row_start?: number;
          desktop_col_start?: number;
          desktop_row_start?: number;
        }[];
      }[];
    };
  };
};

const Concept = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsConcept,
    'sectionsConcept'
  );
  const { title, steps } = content[props.index];

  return (
    <section className='p-3 md:p-6 lg:px-12 w-full flex flex-col gap-3 items-center bg-primary-200'>
      <h2 className='text-center'>{title}</h2>
      <XConcept steps={steps} />
    </section>
  );
};

export default Concept;
