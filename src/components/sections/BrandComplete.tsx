import { gql, QueryContentComponent } from '@/lib/graphql';
import { MediaUrl } from '@/lib/helper';
import { Media } from '@/lib/interfaces';

import RemoteMDX from '@/components/elements/texts/RemoteMDX';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsBrandComplete = gql`
  fragment sectionsBrandComplete on ComponentSectionsBrandComplete {
    image_1 {
      data {
        attributes {
          alternativeText
          caption
          height
          width
          mime
          url
        }
      }
    }
    image_2 {
      data {
        attributes {
          alternativeText
          caption
          height
          width
          mime
          url
        }
      }
    }
    description
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        image_1: {
          data: Media;
        };
        image_2: {
          data: Media;
        };
        description: string;
      }[];
    };
  };
};

const BrandComplete = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsBrandComplete,
    'sectionsBrandComplete'
  );
  const { image_1, image_2, description } = content[props.index];

  return (
    <section className='m-3 grid max-w-screen-3xl grid-cols-2 gap-3 md:m-6 md:gap-6 lg:mx-12 lg:grid-cols-4'>
      <NextImage
        className='flex aspect-[2/3] w-full justify-center md:aspect-square lg:col-span-2 xl:col-span-1'
        imgClassName='w-full h-full object-center object-cover rounded-md'
        width={image_1.data.attributes.width}
        height={image_1.data.attributes.height}
        src={MediaUrl(image_1.data.attributes.url)}
        title={image_1.data.attributes.name}
        alt={image_1.data.attributes.alternativeText ?? ''}
        sizes='50vh'
      />
      <NextImage
        className='flex aspect-[2/3] w-full justify-center md:aspect-square lg:col-span-2 lg:row-start-2 xl:col-span-1 xl:row-start-1'
        imgClassName='w-full h-full object-center object-cover rounded-md'
        width={image_2.data.attributes.width}
        height={image_2.data.attributes.height}
        src={MediaUrl(image_2.data.attributes.url)}
        title={image_2.data.attributes.name}
        alt={image_2.data.attributes.alternativeText ?? ''}
        sizes='50vh'
      />
      <div className='prose prose-carbon col-span-2 flex flex-col gap-3 md:prose-md lg:row-span-2 xl:col-start-2 xl:row-span-1 xl:row-start-1'>
        <RemoteMDX source={description} />
      </div>
    </section>
  );
};

export default BrandComplete;
