import clsxm from '@/lib/clsxm';
import { gql, QueryContentComponent } from '@/lib/graphql';
import { includeLocaleLink } from '@/lib/helper';
import { ENUM_ELEMENTS_LINK_STYLE, Product } from '@/lib/interfaces';

import ProductCarousel from '@/components/elements/carousel/ProductCarousel';
import Link from '@/components/elements/links';

import { useServer } from '@/store/serverStore';

const ComponentSectionsFeatured = gql`
  fragment sectionsFeatured on ComponentSectionsFeatured {
    title
    products {
      product {
        data {
          id
          attributes {
            title
            slug
            short_description
            medias {
              data {
                id
                attributes {
                  caption
                  alternativeText
                  width
                  height
                  url
                  mime
                  name
                }
              }
            }
          }
        }
      }
      btn_text
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        title?: string;
        products: {
          product: {
            data: Product;
          };
          btn_text: string;
        }[];
      }[];
    };
  };
};

const Featured = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType, 'product'],
    ComponentSectionsFeatured,
    'sectionsFeatured'
  );
  const { title, products } = content[props.index];

  return (
    <section className='flex w-full justify-center bg-primary-200'>
      <div className='flex w-full max-w-screen-3xl flex-col justify-center gap-3 p-3 md:gap-6 md:p-6 lg:px-12'>
        {title && <h2 className='text-center uppercase'>{title}</h2>}
        <div className='grid grid-cols-1 gap-3 md:grid-cols-2 md:gap-6'>
          {products.map((element, index) => (
            <article
              key={index}
              className={clsxm(
                'flex flex-col flex-nowrap gap-6 rounded-md bg-secondary-100 p-3 md:flex-col md:p-6',
                index % 2
                  ? 'xs:flex-row-reverse lg:flex-row-reverse'
                  : 'xs:flex-row lg:flex-row'
              )}
            >
              <ProductCarousel
                medias={element.product.data.attributes.medias}
                className='aspect-[3/4] flex-1 overflow-hidden md:h-fit md:w-full md:flex-none lg:h-full lg:w-fit lg:flex-1'
                itemClassName='rounded-md overflow-hidden'
              />
              <div className='flex flex-col items-center justify-center gap-3 text-center xs:flex-1'>
                <h3>{element.product.data.attributes.title}</h3>
                <p>{element.product.data.attributes.short_description}</p>
                <Link
                  href={includeLocaleLink(
                    `/${element.product.data.attributes.slug}`
                  )}
                  title={element.product.data.attributes.title}
                  variant='primary'
                  style={ENUM_ELEMENTS_LINK_STYLE.button}
                  icon='ion:chevron-forward-outline'
                  className='border-2 border-white pr-1 font-semibold text-white'
                  iconClassName='ml-1 text-xl'
                  data-analytics-event='featured-product'
                  data-analytics-event-product={
                    element.product.data.attributes.slug
                  }
                >
                  {element.btn_text}
                </Link>
              </div>
            </article>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Featured;
