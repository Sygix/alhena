'use client';

import { useSearchParams } from 'next/navigation';
import { useCallback, useEffect, useRef, useState } from 'react';

import { buildColorProducts, removeProductVariants } from '@/lib/helper';
import { ColorProducts, Product } from '@/lib/interfaces';

import MultiProductsCard from '@/components/elements/cards/MultiProductsCard';
import SingleProductCard from '@/components/elements/cards/SingleProductCard';
import ProductsListSkeleton from '@/components/layout/ProductsListSkeleton';

import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

import { loadProductsFromStrapi } from '@/actions/strapi/products';

const ProductsList = ({
  products,
  pageSize = 20,
  pageCount = 1,
  page = 1,
  showDescription,
  hideVariants,
}: {
  products: Product[];
  pageSize?: number;
  pageCount?: number;
  page?: number;
  showDescription?: boolean;
  hideVariants: boolean;
}) => {
  const locale = useServer((state) => state.locale);
  const translations = useServer((state) => state.translations);
  const notify = useToaster((state) => state.notify);
  const searchParams = useSearchParams();

  const [currentSearchParams, setCurrentSearchParams] = useState(searchParams);
  const [currentPage, setCurrentPage] = useState(page);
  const [pageCountState, setPageCountState] = useState(pageCount);
  const [listProducts, setListProducts] = useState<Product[]>(products);
  const [productsWithColor, setProductsWithColor] = useState<ColorProducts>({});
  const [isLoading, setIsLoading] = useState(false);
  const observerTarget = useRef(null);

  const loadProducts = useCallback(
    (queryPage = 1) => {
      if (isLoading) return;
      setIsLoading(true);
      const priceFrom = searchParams.get('pricefrom');
      const priceTo = searchParams.get('priceto');
      const sizes = searchParams.getAll('size');
      const colors = searchParams.getAll('color');
      const sort = searchParams.get('sort') as
        | 'price:asc'
        | 'price:desc'
        | 'title:asc'
        | 'title:desc'
        | null;

      loadProductsFromStrapi(
        locale,
        queryPage,
        pageSize,
        searchParams.getAll('category'),
        priceFrom ? parseInt(priceFrom) : undefined,
        priceTo ? parseInt(priceTo) : undefined,
        sizes.length > 0 ? sizes : undefined,
        colors.length > 0 ? colors : undefined,
        sort ? sort : undefined
      )
        .then((res) => {
          const filteredProducts =
            colors.length > 0
              ? res.data.filter(
                  (product) =>
                    product.attributes.colors &&
                    product.attributes.colors.some(
                      (c) =>
                        c.product.data?.id === product.id &&
                        colors.includes(c.name)
                    )
                )
              : res.data;

          res.meta.pagination?.page === currentPage || queryPage === 1
            ? setListProducts(
                hideVariants
                  ? removeProductVariants(filteredProducts)
                  : filteredProducts
              )
            : setListProducts((prev) =>
                hideVariants
                  ? removeProductVariants([...prev, ...filteredProducts])
                  : [...prev, ...filteredProducts]
              );

          res.meta.pagination && setCurrentPage(res.meta.pagination.page);
          res.meta.pagination &&
            setPageCountState(res.meta.pagination.pageCount);
        })
        .catch(() => {
          notify(1, <p>{translations.errors?.internal_server_error}</p>);
        })
        .finally(() => setIsLoading(false));
    },
    [
      isLoading,
      currentPage,
      searchParams,
      locale,
      pageSize,
      notify,
      translations.errors?.internal_server_error,
      hideVariants,
    ]
  );

  useEffect(() => {
    buildColorProducts({ data: listProducts })
      .then((colorProducts) => {
        setProductsWithColor(colorProducts);
      })
      .catch(() => {
        return;
      });
  }, [listProducts]);

  useEffect(() => {
    if (!currentSearchParams) return;
    if (searchParams !== currentSearchParams) {
      loadProducts(1);
      setCurrentSearchParams(searchParams);
    }
  }, [currentSearchParams, loadProducts, searchParams]);

  //Intersection Observer for infinite scroll
  useEffect(() => {
    const target = observerTarget.current;
    const observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting && pageCountState > currentPage) {
          loadProducts(currentPage + 1);
        }
      },
      { threshold: 1 }
    );

    if (observerTarget.current) {
      observer.observe(observerTarget.current);
    }

    return () => {
      if (target) {
        observer.unobserve(target);
      }
    };
  }, [currentPage, loadProducts, observerTarget, pageCountState]);

  return (
    <>
      <ul className='grid w-full max-w-screen-3xl grid-cols-1 gap-3 py-3 xs:grid-cols-2 md:grid-cols-3 md:gap-6 md:py-6 lg:grid-cols-4 xl:grid-cols-5'>
        {listProducts.map((product, index) => {
          const colorProducts = productsWithColor[product.id] || [];
          const productsList = [product, ...colorProducts];
          return (
            <li key={index}>
              {(productsList.length > 1 && (
                <MultiProductsCard
                  products={productsList}
                  showDescription={showDescription}
                  imgSizes='(max-width: 475px) 100vh, 80vh'
                />
              )) || (
                <SingleProductCard
                  product={product}
                  showDescription={showDescription}
                  imgSizes='(max-width: 475px) 100vh, 80vh'
                />
              )}
            </li>
          );
        })}

        {listProducts.length <= 0 && (
          <div className='col-span-full flex w-full justify-center'>
            <p>
              {translations.filters?.no_products_found ?? 'No products found'}
            </p>
          </div>
        )}
        {currentPage < pageCountState && <div ref={observerTarget}></div>}
      </ul>
      {isLoading && <ProductsListSkeleton />}
    </>
  );
};

export default ProductsList;
