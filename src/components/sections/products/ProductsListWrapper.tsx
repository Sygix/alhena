import { QueryProducts } from '@/lib/graphql';
import { removeProductVariants } from '@/lib/helper';

import ProductsList from '@/components/sections/products/ProductsList';

import { useServer } from '@/store/serverStore';

const ProductsListWrapper = async (props: {
  searchParams: { [key: string]: string | string[] | undefined };
  filters?: boolean;
  showDescription?: boolean;
  hideVariants: boolean;
  category?: string | number;
}) => {
  const { locale, categories_page_size } = useServer.getState();

  // Filter search params
  const category: (string | number)[] | undefined = props.category
    ? [props.category]
    : Array.isArray(props.searchParams.category) || !props.filters
    ? (props.searchParams.category as (string | number)[])
    : props.searchParams.category && props.filters
    ? Array.from(props.searchParams.category)
    : undefined;

  const page =
    typeof props.searchParams.page === 'string'
      ? parseInt(props.searchParams.page)
      : 1;

  const sort =
    typeof props.searchParams.sort === 'string' && props.filters
      ? (props.searchParams.sort as
          | 'price:asc'
          | 'price:desc'
          | 'title:asc'
          | 'title:desc')
      : undefined;

  const priceFrom =
    typeof props.searchParams.pricefrom === 'string' && props.filters
      ? parseFloat(props.searchParams.pricefrom)
      : undefined;

  const priceTo =
    typeof props.searchParams.priceto === 'string' && props.filters
      ? parseFloat(props.searchParams.priceto)
      : undefined;

  const sizes =
    Array.isArray(props.searchParams.size) && props.filters
      ? props.searchParams.size
      : props.searchParams.size && props.filters
      ? Array.from(props.searchParams.size)
      : undefined;

  const colors =
    Array.isArray(props.searchParams.color) && props.filters
      ? props.searchParams.color
      : props.searchParams.color && props.filters
      ? Array.from(props.searchParams.color)
      : undefined;

  const { data: products, meta } = await QueryProducts(
    locale,
    page,
    categories_page_size,
    new Date().toISOString(),
    category,
    priceFrom,
    priceTo,
    sizes,
    colors,
    sort
  );

  // Remove products wich color are different from the searchparams
  const filteredProducts = colors
    ? products.filter(
        (product) =>
          product.attributes.colors &&
          product.attributes.colors.some(
            (c) => c.product.data?.id === product.id && colors.includes(c.name)
          )
      )
    : products;

  return (
    <ProductsList
      products={
        props.hideVariants
          ? removeProductVariants(filteredProducts)
          : filteredProducts
      }
      pageSize={categories_page_size}
      pageCount={meta.pagination?.pageCount}
      page={page}
      showDescription={props.showDescription}
      hideVariants={props.hideVariants}
    />
  );
};

export default ProductsListWrapper;
