import {
  QueryCategories,
  QueryCategoryProductsColors,
  QueryCategoryProductsSizes,
  QueryColors,
  QuerySizes,
} from '@/lib/graphql';
import { listTypeEnum } from '@/lib/interfaces';

import DynamicIcon from '@/components/elements/DynamicIcon';
import FiltersForm from '@/components/elements/forms/FiltersForm';

import { useServer } from '@/store/serverStore';

const Filters = async (props: { categoryId?: string | number }) => {
  const translations = useServer.getState().translations;

  const categories = !props.categoryId
    ? (await QueryCategories()).categories.data
    : [];

  const sizes = props.categoryId
    ? (await QueryCategoryProductsSizes(props.categoryId)).categoryProductsSizes
    : (await QuerySizes()).sizes;

  const colors = props.categoryId
    ? (await QueryCategoryProductsColors(props.categoryId))
        .categoryProductsColors
    : (await QueryColors()).colors;

  return (
    <FiltersForm
      filters={[
        //Sort filter
        {
          legend: (
            <>
              <p className='first-letter:capitalize'>
                {translations.filters?.sort_by ?? 'Sort by'}
              </p>
              <DynamicIcon
                icon='ic:round-chevron-left'
                className='-rotate-90 text-xl transition-transform duration-300 group-hover:rotate-90 md:text-2xl'
              />
            </>
          ),
          fieldComponent: 'Radio',
          fieldType: 'sort',
          fieldset: [
            {
              value: 'val-price:asc',
              label: translations.filters?.price_asc ?? 'price:asc',
            },
            {
              value: 'val-price:desc',
              label: translations.filters?.price_desc ?? 'price:desc',
            },
            {
              value: 'val-title:asc',
              label: translations.filters?.title_asc ?? 'title:asc',
            },
            {
              value: 'val-title:desc',
              label: translations.filters?.title_desc ?? 'title:desc',
            },
          ],
        },

        //Categories filter
        ...(props.categoryId === undefined
          ? [
              {
                legend: (
                  <>
                    <p className='first-letter:capitalize'>
                      {translations.filters?.product_type ?? 'Categories'}
                    </p>
                    <DynamicIcon
                      icon='ic:round-chevron-left'
                      className='-rotate-90 text-xl transition-transform duration-300 group-hover:rotate-90 md:text-2xl'
                    />
                  </>
                ),
                fieldComponent: 'Checkbox' as const,
                fieldType: 'category',
                fieldset: categories.map((category) => ({
                  value: `val-${category.id}`,
                  label: category.attributes.title,
                })),
                listType: listTypeEnum.letters,
              },
            ]
          : []),

        //Sizes filter
        {
          legend: (
            <>
              <p className='first-letter:capitalize'>
                {translations.sizes ?? 'Sizes'}
              </p>
              <DynamicIcon
                icon='ic:round-chevron-left'
                className='-rotate-90 text-xl transition-transform duration-300 group-hover:rotate-90 md:text-2xl'
              />
            </>
          ),
          fieldComponent: 'Checkbox',
          fieldset: sizes.map((size) => ({
            value: `val-${size}`,
            label: size,
          })),
          fieldType: 'size',
          listType: listTypeEnum.letters,
        },

        //Colors filter
        {
          legend: (
            <>
              <p className='first-letter:capitalize'>
                {translations.colors ?? 'Colors'}
              </p>
              <DynamicIcon
                icon='ic:round-chevron-left'
                className='-rotate-90 text-xl transition-transform duration-300 group-hover:rotate-90 md:text-2xl'
              />
            </>
          ),
          fieldComponent: 'Checkbox',
          fieldType: 'color',
          fieldset: colors.map((color) => ({
            value: `val-${color}`,
            label: color,
          })),
          listType: listTypeEnum.letters,
        },

        //Price filter
        {
          legend: (
            <>
              <p className='first-letter:capitalize'>
                {translations.price ?? 'Price'}
              </p>
              <DynamicIcon
                icon='ic:round-chevron-left'
                className='-rotate-90 text-xl transition-transform duration-300 group-hover:rotate-90 md:text-2xl'
              />
            </>
          ),
          fieldComponent: 'Input',
          fieldType: 'price',
          fieldset: [
            {
              value: 'pricefrom',
              label: translations.filters?.price_from ?? 'min :',
            },
            {
              value: 'priceto',
              label: translations.filters?.price_to ?? 'max :',
            },
          ],
        },
      ]}
    />
  );
};

export default Filters;
