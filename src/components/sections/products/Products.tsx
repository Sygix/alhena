import { Suspense } from 'react';

import { gql, QueryContentComponent } from '@/lib/graphql';

import ProductsListSkeleton from '@/components/layout/ProductsListSkeleton';
import Filters from '@/components/sections/products/Filters';
import ProductsListWrapper from '@/components/sections/products/ProductsListWrapper';

import { useServer } from '@/store/serverStore';

const ComponentSectionsProductList = gql`
  fragment sectionsProductList on ComponentSectionsProductList {
    filters
    show_description
    hide_variants
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        filters: boolean;
        show_description: boolean;
        hide_variants: boolean;
      }[];
    };
  };
};

const Products = async (props: {
  pageID: number;
  index: number;
  pageType: string;
  searchParams?: { [key: string]: string | string[] | undefined };
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsProductList,
    'sectionsProductList'
  );
  const { filters, show_description, hide_variants } = content[props.index];

  return (
    <section className='relative flex w-full flex-col items-center gap-3 px-3 md:gap-6 md:px-6 lg:px-12'>
      <Suspense fallback={<ProductsListSkeleton />}>
        {filters && (
          <Filters
            categoryId={
              props.pageType === 'category' ? props.pageID : undefined
            }
          />
        )}
        <ProductsListWrapper
          searchParams={props.searchParams ?? {}}
          filters={filters}
          category={props.pageType === 'category' ? props.pageID : undefined}
          showDescription={show_description}
          hideVariants={hide_variants}
        />
      </Suspense>
    </section>
  );
};

export default Products;
