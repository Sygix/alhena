import { gql, QueryContentComponent } from '@/lib/graphql';
import { buildColorProducts } from '@/lib/helper';
import { Product } from '@/lib/interfaces';

import MultiProductsCard from '@/components/elements/cards/MultiProductsCard';
import SingleProductCard from '@/components/elements/cards/SingleProductCard';

import { useServer } from '@/store/serverStore';

const ComponentSectionsProductSelectedList = gql`
  fragment sectionsProductSelectedList on ComponentSectionsProductSelectedList {
    products {
      data {
        id
        attributes {
          title
          slug
          price
          new_product_tag
          sale_price
          date_on_sale_from
          date_on_sale_to
          medias {
            data {
              attributes {
                alternativeText
                name
                url
                width
                height
                mime
              }
            }
          }

          colors {
            color
            image {
              data {
                attributes {
                  alternativeText
                  name
                  url
                  width
                  height
                  mime
                }
              }
            }
            product {
              data {
                id
              }
            }
          }
        }
      }
    }
    show_description
  }
`;

export default async function SelectedList({
  pageID,
  index,
  pageType,
}: {
  pageID: number;
  index: number;
  pageType: string;
}) {
  type dataType = {
    data: {
      attributes: {
        content: {
          products: {
            data: Product[];
          };
          show_description: boolean;
        }[];
      };
    };
  };
  const locale = useServer.getState().locale;
  const { data }: dataType = await QueryContentComponent(
    locale,
    pageID,
    pageType,
    [pageType, 'product'],
    ComponentSectionsProductSelectedList,
    'sectionsProductSelectedList'
  );
  const { products, show_description } = data.attributes.content[index];

  const productsWithColor = await buildColorProducts(products);

  return (
    <ul className='grid w-full max-w-screen-3xl grid-cols-1 gap-3 p-3 xs:grid-cols-2 md:grid-cols-3 md:gap-6 md:p-6 lg:grid-cols-4 lg:px-12 xl:grid-cols-5'>
      {products.data.map((product, index) => {
        const colorProducts = productsWithColor[product.id] || [];
        const productsList = [product, ...colorProducts];
        return (
          <li key={index}>
            {(productsList.length > 1 && (
              <MultiProductsCard
                products={productsList}
                showDescription={show_description}
                imgSizes='(max-width: 475px) 100vh, 80vh'
              />
            )) || (
              <SingleProductCard
                product={product}
                showDescription={show_description}
                imgSizes='(max-width: 475px) 100vh, 80vh'
              />
            )}
          </li>
        );
      })}
    </ul>
  );
}
