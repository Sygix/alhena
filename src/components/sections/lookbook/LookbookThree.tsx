import { gql, QueryContentComponent } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';
import { LinkInterface, Media } from '@/lib/interfaces';

import Link from '@/components/elements/links';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsLookbook3 = gql`
  fragment sectionsLookbookThree on ComponentSectionsLookbook3 {
    image_1 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    image_2 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    image_3 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    image_4 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    image_5 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    title
    description
    link {
      id
      name
      href
      open_new_tab
      icon
      style
      direction
      variant
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        image_1: {
          data: Media;
        };
        image_2: {
          data: Media;
        };
        image_3: {
          data: Media;
        };
        image_4: {
          data: Media;
        };
        image_5: {
          data: Media;
        };
        title: string;
        description: string;
        link: LinkInterface;
      }[];
    };
  };
};

const LookbookThree = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsLookbook3,
    'sectionsLookbookThree'
  );
  const {
    image_1,
    image_2,
    image_3,
    image_4,
    image_5,
    title,
    description,
    link,
  } = content[props.index];

  return (
    <section className="grid w-full max-w-screen-3xl grid-cols-2 grid-rows-5 gap-3 p-3 py-6 before:col-1/1 before:row-1/1 before:w-0 before:pb-[100%] before:content-[''] md:grid-cols-6 md:grid-rows-2 md:gap-6 md:p-6 md:py-12 lg:px-12">
      {/* square */}
      <div className='relative col-span-2 col-start-1 row-span-2 row-start-1'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={image_1.data.attributes.width}
          height={image_1.data.attributes.height}
          src={MediaUrl(image_1.data.attributes.url)}
          title={image_1.data.attributes.name}
          alt={image_1.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      {/* long */}
      <div className='relative row-span-2 md:col-start-3 2xl:col-start-6'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={image_2.data.attributes.width}
          height={image_2.data.attributes.height}
          src={MediaUrl(image_2.data.attributes.url)}
          title={image_2.data.attributes.name}
          alt={image_2.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      {/* square */}
      <div className='relative md:col-span-2 md:col-start-4 md:row-start-1 2xl:col-span-1 2xl:col-start-5'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={image_3.data.attributes.width}
          height={image_3.data.attributes.height}
          src={MediaUrl(image_3.data.attributes.url)}
          title={image_3.data.attributes.name}
          alt={image_3.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      {/* square */}
      <div className='relative md:col-start-6 md:row-span-2 md:row-start-1 2xl:col-start-3 2xl:row-span-1 2xl:row-start-2'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={image_4.data.attributes.width}
          height={image_4.data.attributes.height}
          src={MediaUrl(image_4.data.attributes.url)}
          title={image_4.data.attributes.name}
          alt={image_4.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      {/* large */}
      <div className='relative col-span-2 md:col-start-4 2xl:row-start-2'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={image_5.data.attributes.width}
          height={image_5.data.attributes.height}
          src={MediaUrl(image_5.data.attributes.url)}
          title={image_5.data.attributes.name}
          alt={image_5.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='col-span-2 flex flex-col gap-3 md:col-span-6 lg:col-span-4 lg:col-start-2 2xl:col-span-2 2xl:col-start-3 2xl:row-start-1'>
        {title && <h2 className='h3'>{title}</h2>}
        {description && <p className='text-carbon-700'>{description}</p>}
        {link && (
          <Link
            href={includeLocaleLink(link.href)}
            title={link.name}
            openNewTab={link.open_new_tab}
            icon={link.icon}
            style={link.style}
            direction={link.direction}
            variant={link.variant}
            className='self-end text-right font-bold'
            iconClassName='ml-1 text-2xl'
            data-analytics-event='lookbook-link'
            data-analytics-event-link={link.href}
            data-analytics-event-name={link.name.toLocaleLowerCase()}
          >
            {link.name}
          </Link>
        )}
      </div>
    </section>
  );
};

export default LookbookThree;
