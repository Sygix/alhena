import { gql, QueryContentComponent } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';
import { LinkInterface, Media } from '@/lib/interfaces';

import Link from '@/components/elements/links';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsLookbook1 = gql`
  fragment sectionsLookbookOne on ComponentSectionsLookbook1 {
    large_image {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    square_image {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    large_image_2 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    square_image_2 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    square_image_3 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    title
    description
    link {
      id
      name
      href
      open_new_tab
      icon
      style
      direction
      variant
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        large_image: {
          data: Media;
        };
        square_image: {
          data: Media;
        };
        large_image_2: {
          data: Media;
        };
        square_image_2: {
          data: Media;
        };
        square_image_3: {
          data: Media;
        };
        title: string;
        description: string;
        link: LinkInterface;
      }[];
    };
  };
};

const LookbookOne = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsLookbook1,
    'sectionsLookbookOne'
  );
  const {
    large_image,
    large_image_2,
    square_image,
    square_image_2,
    square_image_3,
    title,
    description,
    link,
  } = content[props.index];

  return (
    <section className="grid w-full max-w-screen-3xl grid-cols-2 grid-rows-5 gap-3 p-3 py-6 before:col-1/1 before:row-1/1 before:w-0 before:pb-[100%] before:content-[''] md:grid-cols-6 md:grid-rows-4 md:gap-6 md:p-6 md:py-12 lg:px-12">
      <div className='relative col-span-2 col-start-1 row-start-1 md:col-span-4 md:col-start-1 md:row-span-2 md:row-start-1'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={large_image.data.attributes.width}
          height={large_image.data.attributes.height}
          src={MediaUrl(large_image.data.attributes.url)}
          title={large_image.data.attributes.name}
          alt={large_image.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='relative col-span-2 row-span-2 md:col-span-2 md:row-span-2'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={square_image.data.attributes.width}
          height={square_image.data.attributes.height}
          src={MediaUrl(square_image.data.attributes.url)}
          title={square_image.data.attributes.name}
          alt={square_image.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='relative col-span-2 md:col-span-4 md:col-start-3 md:row-span-2'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={large_image_2.data.attributes.width}
          height={large_image_2.data.attributes.height}
          src={MediaUrl(large_image_2.data.attributes.url)}
          title={large_image_2.data.attributes.name}
          alt={large_image_2.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='relative md:col-start-2 md:row-start-3 2xl:row-start-4'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={square_image_2.data.attributes.width}
          height={square_image_2.data.attributes.height}
          src={MediaUrl(square_image_2.data.attributes.url)}
          title={square_image_2.data.attributes.name}
          alt={square_image_2.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='relative md:row-start-4'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={square_image_3.data.attributes.width}
          height={square_image_3.data.attributes.height}
          src={MediaUrl(square_image_3.data.attributes.url)}
          title={square_image_3.data.attributes.name}
          alt={square_image_3.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='col-span-2 flex flex-col gap-3 md:col-span-6 lg:col-span-4 lg:col-start-2 2xl:col-span-2 2xl:row-start-3'>
        {title && <h2 className='h3'>{title}</h2>}
        {description && <p className='text-carbon-700'>{description}</p>}
        {link && (
          <Link
            href={includeLocaleLink(link.href)}
            title={link.name}
            openNewTab={link.open_new_tab}
            icon={link.icon}
            style={link.style}
            direction={link.direction}
            variant={link.variant}
            className='self-end text-right font-bold'
            iconClassName='ml-1 text-2xl'
            data-analytics-event='lookbook-link'
            data-analytics-event-link={link.href}
            data-analytics-event-name={link.name.toLocaleLowerCase()}
          >
            {link.name}
          </Link>
        )}
      </div>
    </section>
  );
};

export default LookbookOne;
