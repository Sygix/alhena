import { gql, QueryContentComponent } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';
import { LinkInterface, Media } from '@/lib/interfaces';

import CarouselItem from '@/components/elements/carousel/CarouselItem';
import EmblaCarousel from '@/components/elements/carousel/EmblaCarousel';
import NextButton from '@/components/elements/carousel/NextButton';
import PrevButton from '@/components/elements/carousel/PrevButton';
import Link from '@/components/elements/links';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsLookbookCarousel = gql`
  fragment sectionsLookbookCarousel on ComponentSectionsLookbookCarousel {
    images {
      data {
        id
        attributes {
          name
          alternativeText
          caption
          width
          height
          mime
          url
        }
      }
    }
    title
    description
    link {
      id
      name
      href
      open_new_tab
      icon
      style
      direction
      variant
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        images: {
          data: Media[];
        };
        title: string;
        description: string;
        link: LinkInterface;
      }[];
    };
  };
};

const LookbookCarousel = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsLookbookCarousel,
    'sectionsLookbookCarousel'
  );
  const { images, title, description, link } = content[props.index];

  return (
    <section className='flex w-full max-w-screen-3xl flex-col items-center gap-3 p-3 pb-6 md:gap-6 md:p-6 md:pb-12 lg:px-12'>
      <EmblaCarousel
        otherChildrens={
          <span>
            <PrevButton className='text-carbon-900' />
            <NextButton className='text-carbon-900' />
          </span>
        }
        className='w-full'
        containerClassName='w-full items-center md:gap-3 md:pl-3'
        options={{ loop: true, containScroll: 'trimSnaps', align: 'center' }}
        autoplay={true}
        autoplayOptions={{
          delay: 5000,
          stopOnInteraction: false,
        }}
      >
        {images.data.map((el, index) => (
          <CarouselItem
            key={el.id}
            className='mr-3 aspect-[2/3] w-2/3 transition-spacing duration-300 lg:mr-0 lg:w-3/12'
            selectedClassName='lg:mx-6 xl:mx-12'
            index={index}
            nX={1}
            wrapperClassNames='scale-80 transition-transform duration-300'
            nxSelectedClassNames='scale-90'
            selectedClassNames='scale-100'
          >
            <NextImage
              className='flex h-full w-full justify-center'
              imgClassName='object-center object-cover rounded-md'
              width={el.attributes.width}
              height={el.attributes.height}
              src={MediaUrl(el.attributes.url)}
              title={el.attributes.name}
              alt={el.attributes.alternativeText ?? ''}
              sizes='80vw (min-width: 768px) 50vw'
              quality={100}
              priority={index === 0}
            />
          </CarouselItem>
        ))}
      </EmblaCarousel>
      <div className='flex w-full flex-col gap-3 md:max-w-screen-sm md:gap-6'>
        {title && <h2 className='h3'>{title}</h2>}
        {description && <p className='text-carbon-700'>{description}</p>}
        {link && (
          <Link
            href={includeLocaleLink(link.href)}
            title={link.name}
            openNewTab={link.open_new_tab}
            icon={link.icon}
            style={link.style}
            direction={link.direction}
            variant={link.variant}
            className='self-end text-right font-bold'
            iconClassName='ml-1 text-2xl'
            data-analytics-event='lookbook-link'
            data-analytics-event-link={link.href}
            data-analytics-event-name={link.name.toLocaleLowerCase()}
          >
            {link.name}
          </Link>
        )}
      </div>
    </section>
  );
};

export default LookbookCarousel;
