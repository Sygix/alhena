import { gql, QueryContentComponent } from '@/lib/graphql';
import { includeLocaleLink, MediaUrl } from '@/lib/helper';
import { LinkInterface, Media } from '@/lib/interfaces';

import Link from '@/components/elements/links';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const ComponentSectionsLookbook2 = gql`
  fragment sectionsLookbookTwo on ComponentSectionsLookbook2 {
    long_image {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    square_image {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    square_image_2 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    square_image_3 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    square_image_4 {
      data {
        attributes {
          name
          alternativeText
          caption
          width
          height
          url
        }
      }
    }
    title
    description
    link {
      id
      name
      href
      open_new_tab
      icon
      style
      direction
      variant
    }
  }
`;

type dataType = {
  data: {
    attributes: {
      content: {
        long_image: {
          data: Media;
        };
        square_image: {
          data: Media;
        };
        square_image_2: {
          data: Media;
        };
        square_image_3: {
          data: Media;
        };
        square_image_4: {
          data: Media;
        };
        title: string;
        description: string;
        link: LinkInterface;
      }[];
    };
  };
};

const LookbookTwo = async (props: {
  pageID: number;
  index: number;
  pageType: string;
}) => {
  const locale = useServer.getState().locale;
  const {
    data: {
      attributes: { content },
    },
  }: dataType = await QueryContentComponent(
    locale,
    props.pageID,
    props.pageType,
    [props.pageType],
    ComponentSectionsLookbook2,
    'sectionsLookbookTwo'
  );
  const {
    long_image,
    square_image,
    square_image_2,
    square_image_3,
    square_image_4,
    title,
    description,
    link,
  } = content[props.index];

  return (
    <section className="grid w-full max-w-screen-3xl grid-cols-2 grid-rows-6 gap-3 p-3 py-6 before:col-1/1 before:row-1/1 before:w-0 before:pb-[100%] before:content-[''] md:grid-cols-6 md:grid-rows-2 md:gap-6 md:p-6 md:py-12 lg:px-12 2xl:grid-rows-3">
      <div className='relative col-span-2 col-start-1 row-span-2 row-start-1 2xl:row-start-2'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={square_image.data.attributes.width}
          height={square_image.data.attributes.height}
          src={MediaUrl(square_image.data.attributes.url)}
          title={square_image.data.attributes.name}
          alt={square_image.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='relative 2xl:row-start-2'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={square_image_2.data.attributes.width}
          height={square_image_2.data.attributes.height}
          src={MediaUrl(square_image_2.data.attributes.url)}
          title={square_image_2.data.attributes.name}
          alt={square_image_2.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='relative row-start-4 md:col-start-3 md:row-start-2 2xl:row-start-3'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={square_image_3.data.attributes.width}
          height={square_image_3.data.attributes.height}
          src={MediaUrl(square_image_3.data.attributes.url)}
          title={square_image_3.data.attributes.name}
          alt={square_image_3.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='relative col-start-2 row-span-2 md:col-start-6 2xl:row-start-2'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={long_image.data.attributes.width}
          height={long_image.data.attributes.height}
          src={MediaUrl(long_image.data.attributes.url)}
          title={long_image.data.attributes.name}
          alt={long_image.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='relative col-span-2 row-span-2 md:col-start-4 md:row-start-1 2xl:row-start-2'>
        <NextImage
          className='absolute flex h-full w-full'
          imgClassName='object-center object-cover rounded-md'
          width={square_image_4.data.attributes.width}
          height={square_image_4.data.attributes.height}
          src={MediaUrl(square_image_4.data.attributes.url)}
          title={square_image_4.data.attributes.name}
          alt={square_image_4.data.attributes.alternativeText ?? ''}
          sizes='100vw'
          quality={100}
        />
      </div>

      <div className='col-span-2 flex flex-col gap-3 md:col-span-6 lg:col-span-4 lg:col-start-2 2xl:col-span-2 2xl:col-start-3'>
        {title && <h2 className='h3'>{title}</h2>}
        {description && <p className='text-carbon-700'>{description}</p>}
        {link && (
          <Link
            href={includeLocaleLink(link.href)}
            title={link.name}
            openNewTab={link.open_new_tab}
            icon={link.icon}
            style={link.style}
            direction={link.direction}
            variant={link.variant}
            className='self-end text-right font-bold'
            iconClassName='ml-1 text-2xl'
            data-analytics-event='lookbook-link'
            data-analytics-event-link={link.href}
            data-analytics-event-name={link.name.toLocaleLowerCase()}
          >
            {link.name}
          </Link>
        )}
      </div>
    </section>
  );
};

export default LookbookTwo;
