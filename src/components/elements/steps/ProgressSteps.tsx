'use client';

import { createContext, ReactNode, useContext } from 'react';

const ProgressStepsContext = createContext<{
  currentStep: number;
}>({
  currentStep: 1,
});

export const useProgressStepsContext = () => {
  const context = useContext(ProgressStepsContext);
  if (!context) {
    throw new Error(
      'useProgressStepsContext must be used within a ProgressSteps'
    );
  }
  return context;
};

const ProgressSteps = ({
  children,
  currentStep,
  className,
}: {
  children: ReactNode[];
  currentStep: number;
  className?: string;
}) => {
  return (
    <ProgressStepsContext.Provider value={{ currentStep: currentStep }}>
      <div className={className}>{children}</div>
    </ProgressStepsContext.Provider>
  );
};

export default ProgressSteps;
