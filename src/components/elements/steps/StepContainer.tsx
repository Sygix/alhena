import { ReactNode } from 'react';

import clsxm from '@/lib/clsxm';

import { useProgressStepsContext } from '@/components/elements/steps/ProgressSteps';

const StepContainer = ({
  step,
  className,
  children,
}: {
  step: number;
  className?: string;
  children: ReactNode;
}) => {
  const { currentStep } = useProgressStepsContext();

  return (
    <div
      className={clsxm(
        'group/step relative flex flex-1 flex-col gap-1',
        className,
        currentStep > step ? 'active' : ''
      )}
    >
      {children}
    </div>
  );
};

export default StepContainer;
