'use client';

import clsxm from '@/lib/clsxm';

import { useProgressStepsContext } from '@/components/elements/steps/ProgressSteps';

const StepLabel = ({
  label,
  className,
}: {
  className?: string;
  label?: string;
}) => {
  const { currentStep } = useProgressStepsContext();

  return (
    <p
      className={clsxm(
        'relative -left-1/2 ml-9 text-center first-letter:uppercase',
        className
      )}
    >
      {label ? label : currentStep}
    </p>
  );
};

export default StepLabel;
