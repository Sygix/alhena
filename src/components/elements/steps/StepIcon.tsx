'use client';

import clsxm from '@/lib/clsxm';

import DynamicIcon, {
  DynamicIconProps,
} from '@/components/elements/DynamicIcon';
import { useProgressStepsContext } from '@/components/elements/steps/ProgressSteps';

type StepIcon = {
  step: number;
} & Partial<DynamicIconProps>;

const StepIcon = ({ step, className, wrapperClassName }: StepIcon) => {
  const { currentStep } = useProgressStepsContext();

  return (
    <div
      className={clsxm(
        'relative flex flex-1',
        'before:absolute before:top-1/2 before:-z-10 before:w-full before:-translate-y-1/2 before:border-t-2 before:border-gray-200',
        'group-[&.active]/step:text-green-400 group-[&.active]/step:before:border-green-400'
      )}
    >
      <DynamicIcon
        icon={
          step === currentStep
            ? 'ic:round-check'
            : step < currentStep
            ? 'ic:round-check'
            : 'octicon:dot-16'
        }
        className={clsxm('h-6 w-6', className)}
        wrapperClassName={clsxm(
          'bg-white rounded-full border-2 border-gray-200 group-[&.active]/step:border-green-400 p-1',
          wrapperClassName
        )}
      />
    </div>
  );
};

export default StepIcon;
