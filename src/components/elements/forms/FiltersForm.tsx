'use client';

import { yupResolver } from '@hookform/resolvers/yup';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { InferType, object, string } from 'yup';

import { umamiAnalytics } from '@/lib/helper';

import Button from '@/components/elements/buttons/Button';
import FiltersFieldset, {
  FilterFieldsetProps,
} from '@/components/elements/forms/organisms/FiltersFieldset';

import { useServer } from '@/store/serverStore';

type FiltersFormProps = {
  filters: FilterFieldsetProps[];
};

const generateYupSchema = (filters: FilterFieldsetProps[]) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const schemaObject: Record<string, any> = {};

  filters.forEach((filter) => {
    switch (filter.fieldComponent) {
      case 'Checkbox':
        schemaObject[filter.fieldType] = object();
        break;
      case 'Radio':
        schemaObject[filter.fieldType] = string()
          .oneOf(filter.fieldset.map((item) => item.value))
          .notRequired();
        break;
      default:
        schemaObject[filter.fieldType] = object();
    }
  });

  return object(schemaObject);
};

const FiltersForm = ({ filters }: FiltersFormProps) => {
  const searchParams = useSearchParams();
  const router = useRouter();
  const pathname = usePathname();
  const schema = generateYupSchema(filters);
  const translations = useServer.getState().translations;

  type FormValues = InferType<typeof schema>;
  const { register, handleSubmit, setValue } = useForm<FormValues>({
    resolver: yupResolver(schema),
    mode: 'onChange',
    shouldUnregister: true,
  });

  const createQueryString = useCallback(
    (data: Record<string, string | string[]>) => {
      const params = new URLSearchParams(searchParams);
      const currentKeys = params.keys();
      for (const key of currentKeys) {
        if (!Object.keys(data).includes(key)) {
          params.delete(key);
        }
      }

      Object.entries(data).forEach(([key, value]) => {
        const currentValues = params.getAll(key);

        if (Array.isArray(value)) {
          currentValues.forEach((item) => {
            if (!value.includes(item)) {
              params.delete(key);
            }
          });
          value.forEach((item) => {
            if (!params.getAll(key).includes(item.replace('val-', '')))
              params.append(key, item.replace('val-', ''));
          });
        } else {
          params.set(key, value.replace('val-', ''));
        }
      });

      return params.toString();
    },
    [searchParams]
  );

  // add searchparams filters to url when submitting form
  const onSubmit = (
    data: Record<
      string,
      string | Record<string, string> | null | boolean | undefined
    >
  ) => {
    const filteredData: Record<string, string | string[]> = {};

    Object.entries(data).forEach(([key, value]) => {
      if (value) {
        if (typeof value === 'object') {
          const list: string[] = [];
          Object.entries(value).forEach(([_, val]) => {
            if (val) list.push(val);
          });
          filteredData[key] = list;
        } else {
          filteredData[key] = value.toString();
        }
      }
    });

    umamiAnalytics('filters', { data: filteredData });

    router.push(pathname + '?' + createQueryString(filteredData));
  };

  // set values from url searchparams
  useEffect(() => {
    if (!setValue) return;
    const params = Array.from(searchParams.entries());

    params.forEach(([key, value]) => {
      const filter = filters.find(
        (item) =>
          item.fieldType === key ||
          item.fieldset.find((item) => item.value === key)
      );
      setValue(
        filter?.fieldComponent === 'Radio' || filter?.fieldType === 'price'
          ? key
          : `${key}.val-${value}`,
        filter?.fieldType === 'price' ? value : `val-${value}`
      );
    });
  }, [filters, searchParams, setValue]);

  return (
    <div className='sticky top-20 z-10 flex w-full justify-center md:top-24 md:pt-2'>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className='flex max-w-full flex-wrap justify-center gap-3 font-semibold'
      >
        {filters.map((filter, index) => (
          <FiltersFieldset
            key={`${filter.fieldType}-${index}`}
            {...filter}
            name={filter.fieldType}
            register={register}
            style={{
              zIndex: filters.length - index,
            }}
          />
        ))}

        <div className='relative'>
          <span className='absolute top-0 -z-10 h-full w-full rounded-full bg-white/10 backdrop-blur-lg'></span>
          <Button
            type='submit'
            variant='outline'
            className='w-full max-w-[12rem] rounded-full border-2 border-carbon-900 bg-transparent p-0 px-3 font-semibold'
          >
            <span className='max-w-full truncate first-letter:capitalize'>
              {translations.submit ?? 'Submit'}
            </span>
          </Button>
        </div>
      </form>
    </div>
  );
};

export default FiltersForm;
