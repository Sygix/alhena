'use client';

import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { boolean, InferType, object, string } from 'yup';

import clsxm from '@/lib/clsxm';
import { umamiAnalytics } from '@/lib/helper';

import Button from '@/components/elements/buttons/Button';
import FormInput from '@/components/elements/forms/molecules/FormInput';
import FormTextArea from '@/components/elements/forms/molecules/FormTextArea';

import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

import { sendContactMail } from '@/actions/strapi/send-contact-mail';

let translations = useServer.getState().translations;

const schema = object({
  name: string()
    .min(
      2,
      translations.form?.min_name ??
        'Votre nom doit comporter plus de 2 lettres'
    )
    .required(translations.form?.required_name ?? 'Un nom est requis'),
  subject: string()
    .min(5, translations.form?.min ?? 'Votre sujet est trop court')
    .required(translations.form?.subject_required ?? 'Un sujet est requis'),
  email: string()
    .email(
      translations.form?.invalid_email ?? "Votre adresse email n'est pas valide"
    )
    .required(translations.form?.required_email ?? 'Email requise'),
  message: string()
    .min(25, translations.form?.min ?? 'Votre message est trop court')
    .required(translations.form?.required_message ?? 'Un message est requis'),
  consent: boolean()
    .isTrue(
      translations.form?.consent_required ?? 'Vous devez accepter pour envoyer'
    )
    .required(
      translations.form?.consent_required ?? 'Vous devez accepter pour envoyer'
    ),
}).required();

export type ContactFormType = InferType<typeof schema>;

const ContactForm = ({
  placeholders,
  submitText,
  className,
  inputClassName,
}: {
  placeholders?: {
    name?: string;
    email?: string;
    subject?: string;
    message?: string;
    consent?: string;
  };
  submitText: string;
  className?: string;
  inputClassName?: string;
}) => {
  translations = useServer((state) => state.translations);
  const locale = useServer((state) => state.locale);
  const notify = useToaster((state) => state.notify);
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<ContactFormType>({
    resolver: yupResolver(schema),
    mode: 'onChange',
    shouldUnregister: true,
  });

  const submitForm = (data: ContactFormType) => {
    umamiAnalytics('contact-form-submit');
    sendContactMail(locale, data)
      .then(() => notify(0, <p>!Votre demande a été prise en compte, merci</p>))
      .catch(() =>
        notify(
          1,
          <p>
            !Oups, il y a eu un problème avec votre inscription, veuillez
            réessayer plus tard
          </p>
        )
      );
  };

  return (
    <form
      onSubmit={handleSubmit(submitForm)}
      className={clsxm('flex text-carbon-900', className)}
    >
      <FormInput
        name='name'
        label='Name'
        placeholder={placeholders?.name}
        className={clsxm(
          'w-full grow rounded-full border-0 text-sm md:text-base xl:w-auto xl:shrink',
          inputClassName
        )}
        register={register}
        errors={errors}
        errorClassName='text-red-600 text-sm'
      />

      <FormInput
        name='email'
        label='Email'
        placeholder={placeholders?.email}
        className={clsxm(
          'w-full grow rounded-full border-0 text-sm md:text-base xl:w-auto xl:shrink',
          inputClassName
        )}
        register={register}
        errors={errors}
        errorClassName='text-red-600 text-sm'
      />

      <FormInput
        name='subject'
        label='Subject'
        placeholder={placeholders?.subject}
        className={clsxm(
          'w-full rounded-full border-0 text-sm md:text-base',
          inputClassName
        )}
        register={register}
        errors={errors}
        errorClassName='text-red-600 text-sm'
      />

      <FormTextArea
        name='message'
        label='Message'
        placeholder={placeholders?.message}
        className={clsxm(
          'w-full rounded-2xl border-0 text-sm md:text-base',
          inputClassName
        )}
        rows={5}
        register={register}
        errors={errors}
        errorClassName='text-red-600 text-sm'
      />

      <div className='w-full'>
        <FormInput
          name='consent'
          label='Consent'
          type='checkbox'
          className={clsxm(
            'form-checkbox mr-3 rounded-full border-0',
            inputClassName
          )}
          register={register}
          errors={errors}
          errorClassName='text-red-600 text-sm'
        />
        <label htmlFor='consent'>{placeholders?.consent}</label>
      </div>

      <Button
        type='submit'
        variant='outline'
        className='w-fit justify-center border-2 border-white bg-primary-200 px-6 py-1 text-sm font-semibold capitalize text-white hover:bg-primary-300 md:ml-auto md:px-12 md:text-base'
      >
        {submitText}
      </Button>
    </form>
  );
};

export default ContactForm;
