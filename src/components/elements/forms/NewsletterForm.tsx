'use client';

import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { InferType, object, string } from 'yup';

import clsxm from '@/lib/clsxm';
import { umamiAnalytics } from '@/lib/helper';

import IconButton from '@/components/elements/buttons/IconButton';
import FormInput from '@/components/elements/forms/molecules/FormInput';

import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

import { addContact } from '@/actions/sendgrid/marketing';

let translations = useServer.getState().translations;

const schema = object({
  email: string()
    .email(
      translations.form?.invalid_email ?? "Votre adresse email n'est pas valide"
    )
    .required(translations.form?.required_email ?? 'Email requise'),
}).required();

export type NewsletterFormType = InferType<typeof schema>;

const NewsletterForm = ({
  placeholder,
  className,
  inputClassName,
  buttonClassName,
}: {
  placeholder?: string;
  className?: string;
  inputClassName?: string;
  buttonClassName?: string;
}) => {
  translations = useServer((state) => state.translations);
  const notify = useToaster((state) => state.notify);

  const { handleSubmit, register } = useForm<NewsletterFormType>({
    resolver: yupResolver(schema),
    mode: 'onChange',
    shouldUnregister: true,
  });

  const submitSignUp = (data: NewsletterFormType) => {
    umamiAnalytics('newsletter-signup');
    addContact(data.email)
      .then(() =>
        notify(
          0,
          <p>{translations.form?.newsletter_email_added ?? 'Email ajouté !'}</p>
        )
      )
      .catch(() =>
        notify(
          1,
          <p>
            {translations.form?.newsletter_error ?? "Problème d'inscription"}
          </p>
        )
      );
  };

  return (
    <form
      onSubmit={handleSubmit(submitSignUp)}
      className={clsxm(
        'flex flex-row justify-between rounded-full bg-white text-carbon-900',
        className
      )}
    >
      <FormInput
        name='email'
        label='Email'
        placeholder={placeholder}
        className={clsxm('shrink rounded-l-full border-0', inputClassName)}
        register={register}
      />
      <IconButton
        type='submit'
        icon='mi:chevron-right'
        variant='ghost'
        className={clsxm(
          'aspect-square h-full shrink-0 rounded-full border-primary-200 bg-primary-50 text-2xl text-carbon-900',
          buttonClassName
        )}
      />
    </form>
  );
};

export default NewsletterForm;
