'use client';

import {
  FieldErrors,
  FieldValues,
  get,
  Path,
  RegisterOptions,
  UseFormRegister,
} from 'react-hook-form';

import Checkbox, {
  CheckboxProps,
} from '@/components/elements/forms/atoms/Checkbox';
import FormErrorMessage from '@/components/elements/forms/atoms/FormErrorMessage';

export type FormCheckboxProps<TFormValues extends FieldValues> = {
  name: Path<TFormValues>;
  rules?: RegisterOptions;
  register?: UseFormRegister<TFormValues>;
  errors?: FieldErrors<TFormValues>;
  errorClassName?: string;
} & Omit<CheckboxProps, 'name'>;

const FormCheckbox = <TFormValues extends Record<string, unknown>>({
  name,
  register,
  rules,
  errors,
  className,
  errorClassName,
  ...rest
}: FormCheckboxProps<TFormValues>): JSX.Element => {
  const errorMessage = (errors && get(errors, name)?.message) as string;

  return (
    <>
      <Checkbox
        name={name}
        className={className}
        {...rest}
        {...(register && register(name, rules))}
      />
      {errorMessage && (
        <FormErrorMessage className={errorClassName}>
          {errorMessage}
        </FormErrorMessage>
      )}
    </>
  );
};

export default FormCheckbox;
