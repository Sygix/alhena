'use client';

import {
  FieldErrors,
  FieldValues,
  get,
  Path,
  RegisterOptions,
  UseFormRegister,
} from 'react-hook-form';

import FormErrorMessage from '@/components/elements/forms/atoms/FormErrorMessage';
import Radio, { RadioProps } from '@/components/elements/forms/atoms/Radio';

export type FormRadioProps<TFormValues extends FieldValues> = {
  name: Path<TFormValues>;
  rules?: RegisterOptions;
  register?: UseFormRegister<TFormValues>;
  errors?: FieldErrors<TFormValues>;
  errorClassName?: string;
} & Omit<RadioProps, 'name'>;

const FormRadio = <TFormValues extends Record<string, unknown>>({
  name,
  register,
  rules,
  errors,
  className,
  errorClassName,
  ...rest
}: FormRadioProps<TFormValues>): JSX.Element => {
  const errorMessage = (errors && get(errors, name)?.message) as string;

  return (
    <>
      <Radio
        name={name}
        className={className}
        {...rest}
        {...(register && register(name, rules))}
      />
      {errorMessage && (
        <FormErrorMessage className={errorClassName}>
          {errorMessage}
        </FormErrorMessage>
      )}
    </>
  );
};

export default FormRadio;
