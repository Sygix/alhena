import { forwardRef } from 'react';

import clsxm from '@/lib/clsxm';

export type CheckboxProps = {
  label: string;
} & React.ComponentPropsWithRef<'input'>;

const Checkbox = forwardRef<HTMLInputElement, CheckboxProps>(
  ({ label, className, ...rest }, ref) => {
    return (
      <input
        ref={ref}
        aria-label={label}
        type='checkbox'
        className={clsxm('form-checkbox', className)}
        {...rest}
      />
    );
  }
);

export default Checkbox;
