import { forwardRef } from 'react';

import clsxm from '@/lib/clsxm';

export type RadioProps = {
  label: string;
} & React.ComponentPropsWithRef<'input'>;

const Radio = forwardRef<HTMLInputElement, RadioProps>(
  ({ label, className, ...rest }, ref) => {
    return (
      <input
        ref={ref}
        aria-label={label}
        type='radio'
        className={clsxm('form-radio', className)}
        {...rest}
      />
    );
  }
);

export default Radio;
