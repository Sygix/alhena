'use client';

import { yupResolver } from '@hookform/resolvers/yup';
import dynamic from 'next/dynamic';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';

import Switch from '@/components/elements/buttons/Switch';
import FormErrorMessage from '@/components/elements/forms/atoms/FormErrorMessage';
import FormInput from '@/components/elements/forms/molecules/FormInput';

import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';

const CountryDropdown = dynamic(
  () =>
    import('react-country-region-selector').then((mod) => mod.CountryDropdown),
  { ssr: false }
);
const RegionDropdown = dynamic(
  () =>
    import('react-country-region-selector').then((mod) => mod.RegionDropdown),
  { ssr: false }
);

let translations = useServer.getState().translations;

const schema = yup
  .object({
    shippingDifferent: yup.boolean().required(),
    email: yup
      .string()
      .email(
        translations.form?.invalid_email ??
          "Votre adresse email n'est pas valide"
      )
      .required(translations.form?.required_email ?? 'Email requise'),
    address: yup
      .object({
        name: yup
          .string()
          .required(translations.form?.required_name ?? 'Un nom est requis')
          .min(
            2,
            translations.form?.min_name ??
              'Votre nom doit comporter plus de 2 lettres'
          )
          .max(50, translations.form?.max_name ?? 'Votre nom est trop long')
          .matches(
            /^[a-zA-ZÀ-ÿ\s\-']+$/i,
            translations.form?.regex_name ??
              "Votre nom doit être composé de lettres et éventuellement d'un espace"
          ),

        city: yup
          .string()
          .required(translations.form?.required_city ?? 'Une ville est requise')
          .max(
            100,
            translations.form?.max_city ?? 'Votre ville est trop longue'
          )
          .matches(
            /^[a-zA-ZÀ-ÿ\s\-']+$/i,
            translations.form?.regex_city ??
              "Votre ville doit être composée de lettres, d'espaces, de tirets ou d'apostrophes"
          ),

        country: yup
          .string()
          .required(
            translations.form?.required_country ?? 'Un pays est requis'
          ),

        line1: yup
          .string()
          .required(
            translations.form?.required_address ?? 'Une adresse est requise'
          )
          .matches(
            /^[0-9A-Za-zÀ-ÿ\s\-',.]+$/i,
            translations.form?.regex_address ??
              "Votre adresse doit être composée de chiffres, de lettres, d'espaces, de tirets, d'apostrophes ou de points"
          ),

        line2: yup
          .string()
          .notRequired()
          .matches(/^[0-9A-Za-zÀ-ÿ\s\-',.]+$/i, {
            excludeEmptyString: true,
            message:
              translations.form?.regex_address ??
              "Votre adresse doit être composée de chiffres, de lettres, d'espaces, de tirets, d'apostrophes ou de points",
          }),

        postal_code: yup
          .string()
          .required(
            translations.form?.required_postalcode ??
              'Un code postal est requis'
          )
          .max(
            10,
            translations.form?.max_postalcode ??
              'Votre code postal est trop long'
          )
          .matches(
            /^[0-9A-Za-zÀ-ÿ\s\-'()]+$/i,
            translations.form?.regex_postalcode ??
              "Votre code postal doit être composé de chiffres, de lettres, d'espaces, de tirets, d'apostrophes ou de parenthèses"
          ),

        state: yup.string().notRequired(),
      })
      .required(),

    shipping: yup
      .object({
        name: yup
          .string()
          .required(translations.form?.required_name ?? 'Un nom est requis')
          .min(
            2,
            translations.form?.min_name ??
              'Votre nom doit comporter plus de 2 lettres'
          )
          .max(50, translations.form?.max_name ?? 'Votre nom est trop long')
          .matches(
            /^[a-zA-ZÀ-ÿ\s\-']+$/i,
            translations.form?.regex_name ??
              "Votre nom doit être composé de lettres et éventuellement d'un espace"
          )
          .notRequired(),

        city: yup
          .string()
          .required(translations.form?.required_city ?? 'Une ville est requise')
          .max(
            100,
            translations.form?.max_city ?? 'Votre ville est trop longue'
          )
          .matches(
            /^[a-zA-ZÀ-ÿ\s\-']+$/i,
            translations.form?.regex_city ??
              "Votre ville doit être composée de lettres, d'espaces, de tirets ou d'apostrophes"
          )
          .notRequired(),

        country: yup
          .string()
          .required(translations.form?.required_country ?? 'Un pays est requis')
          .notRequired(),

        line1: yup
          .string()
          .required(
            translations.form?.required_address ?? 'Une adresse est requise'
          )
          .matches(
            /^[0-9A-Za-zÀ-ÿ\s\-',.]+$/i,
            translations.form?.regex_address ??
              "Votre adresse doit être composée de chiffres, de lettres, d'espaces, de tirets, d'apostrophes ou de points"
          )
          .notRequired(),

        line2: yup
          .string()
          .notRequired()
          .matches(/^[0-9A-Za-zÀ-ÿ\s\-',.]+$/i, {
            excludeEmptyString: true,
            message:
              translations.form?.regex_address ??
              "Votre adresse doit être composée de chiffres, de lettres, d'espaces, de tirets, d'apostrophes ou de points",
          }),

        postal_code: yup
          .string()
          .required(
            translations.form?.required_postalcode ??
              'Un code postal est requis'
          )
          .max(
            10,
            translations.form?.max_postalcode ??
              'Votre code postal est trop long'
          )
          .matches(
            /^[0-9A-Za-zÀ-ÿ\s\-'()]+$/i,
            translations.form?.regex_postalcode ??
              "Votre code postal doit être composé de chiffres, de lettres, d'espaces, de tirets, d'apostrophes ou de parenthèses"
          )
          .notRequired(),

        state: yup.string().notRequired(),
      })
      .when('shippingDifferent', {
        is: false,
        then(schema) {
          return schema.nullable();
        },
      }),

    giftCard: yup
      .object({
        email: yup
          .string()
          .email(
            translations.form?.invalid_email ??
              "Votre adresse email n'est pas valide"
          )
          .required(translations.form?.required_email ?? 'Email requise')
          .notRequired(),
        sendToRecipient: yup.boolean().required().notRequired(),
        sendMeCopy: yup.boolean().required().notRequired(),
      })
      .notRequired(),
  })
  .required();
export type AddressFormType = yup.InferType<typeof schema>;

const AddressForm = ({
  onSubmit,
}: {
  onSubmit: (data: AddressFormType) => void;
}) => {
  translations = useServer((state) => state.translations);
  const address = useCart((state) => state.address);
  const cartItems = useCart((state) => state.cartItems);

  const cartContainsGiftCard = cartItems.some((item) => item.gift_card);

  const {
    control,
    register,
    formState: { errors },
    handleSubmit,
    watch,
  } = useForm<AddressFormType>({
    resolver: yupResolver(schema),
    mode: 'onChange',
    shouldUnregister: true,
    defaultValues: async () => address,
  });

  return (
    <>
      <form
        className='flex w-full max-w-screen-3xl flex-col items-center justify-center gap-6 p-3 xs:py-5 md:flex-row md:items-start md:gap-12 md:p-6 lg:px-12'
        onSubmit={handleSubmit(onSubmit)}
      >
        {cartContainsGiftCard && (
          <div className='flex max-w-md flex-1 flex-col items-center gap-3'>
            <h2 className='text-center first-letter:uppercase'>
              {translations.form?.gift_card_info ?? 'Gift Card informations'}
            </h2>

            <p className='text-center text-gray-500'>
              {translations.form?.gift_card_warning ??
                'Only the email used below will be able to spend the gift card'}
            </p>

            <FormInput<AddressFormType>
              name='giftCard.email'
              placeholder={
                translations.form?.email_placeholder ?? 'use@proton.me'
              }
              label='Email'
              className='w-full rounded-full border-2 border-carbon-900'
              errorClassName='w-full text-center mx-3 mb-2 text-red-600'
              register={register}
              errors={errors}
            />

            <div className='flex items-center justify-center gap-3'>
              <p className='first-letter:uppercase'>
                {translations.form?.send_to_recipient ??
                  "Envoyez la carte cadeau à l'e-mail du destinataire : "}
              </p>
              <Controller
                name='giftCard.sendToRecipient'
                control={control}
                defaultValue={true}
                render={({ field }) => (
                  <Switch
                    toggle={field.onChange}
                    toggleIsOn={field.value ?? false}
                    className='bg-primary-200 text-xl md:text-2xl'
                    isOnClassName='bg-green-400'
                  />
                )}
              />
            </div>

            <div className='flex items-center justify-center gap-3'>
              <p className='first-letter:uppercase'>
                {translations.form?.send_me_copy ??
                  'Envoyez-moi une copie de la carte cadeau par email : '}
              </p>
              <Controller
                name='giftCard.sendMeCopy'
                control={control}
                defaultValue={false}
                render={({ field }) => (
                  <Switch
                    toggle={field.onChange}
                    toggleIsOn={field.value ?? false}
                    className='bg-primary-200 text-xl md:text-2xl'
                    isOnClassName='bg-green-400'
                  />
                )}
              />
            </div>
          </div>
        )}

        <div className='flex max-w-md flex-1 flex-col items-center gap-3'>
          <h2 className='text-center first-letter:uppercase'>
            {translations.form?.billing_address ?? 'Billing informations'}
          </h2>

          <FormInput<AddressFormType>
            name='email'
            placeholder={
              translations.form?.email_placeholder ?? 'use@proton.me'
            }
            label='Email'
            className='w-full rounded-full border-2 border-carbon-900'
            errorClassName='w-full text-center mx-3 mb-2 text-red-600'
            register={register}
            errors={errors}
          />

          <FormInput<AddressFormType>
            name='address.name'
            placeholder={
              translations.form?.name_placeholder ?? 'Clara Martinez'
            }
            label='Full name'
            className='w-full rounded-full border-2 border-carbon-900'
            errorClassName='w-full text-center mx-3 mb-2 text-red-600'
            register={register}
            errors={errors}
          />

          <FormInput<AddressFormType>
            name='address.city'
            placeholder={translations.form?.city_placeholder ?? 'Paris'}
            label='City'
            className='w-full rounded-full border-2 border-carbon-900'
            errorClassName='w-full text-center mx-3 mb-2 text-red-600'
            register={register}
            errors={errors}
          />

          <Controller
            name='address.country'
            render={({ field }) => (
              <CountryDropdown
                defaultOptionLabel={
                  translations.form?.country_dropdown ?? '-- Pays --'
                }
                name={field.name}
                value={field.value}
                valueType='short'
                onChange={field.onChange}
                classes='w-full border-2 rounded-full border-carbon-900'
              />
            )}
            control={control}
          />
          {errors.address?.country && (
            <FormErrorMessage className='mx-3 mb-2 w-full text-center text-red-600'>
              {errors.address.country.message}
            </FormErrorMessage>
          )}

          <FormInput<AddressFormType>
            name='address.line1'
            placeholder={
              translations.form?.line_placeholder ?? '1 rue de la paix'
            }
            label='line 1'
            className='w-full rounded-full border-2 border-carbon-900'
            errorClassName='w-full text-center mx-3 mb-2 text-red-600'
            register={register}
            errors={errors}
          />

          <FormInput<AddressFormType>
            name='address.line2'
            label='line 2'
            className='w-full rounded-full border-2 border-carbon-900'
            errorClassName='w-full text-center mx-3 mb-2 text-red-600'
            register={register}
            errors={errors}
          />

          <FormInput<AddressFormType>
            name='address.postal_code'
            placeholder={translations.form?.postal_code_placeholder ?? '75001'}
            label='Postal code'
            className='w-full rounded-full border-2 border-carbon-900'
            errorClassName='w-full text-center mx-3 mb-2 text-red-600'
            register={register}
            errors={errors}
          />

          {watch().address?.country && (
            <>
              <Controller
                name='address.state'
                render={({ field: { name, onChange, value } }) => (
                  <RegionDropdown
                    country={watch().address.country}
                    countryValueType='short'
                    defaultOptionLabel={
                      translations.form?.state_dropdown ?? '-- Région --'
                    }
                    name={name}
                    value={value ?? ''}
                    onChange={onChange}
                    classes='w-full border-2 rounded-full border-carbon-900'
                  />
                )}
                control={control}
              />

              {errors.address?.state && (
                <FormErrorMessage className='mx-3 mb-2 w-full text-center text-red-600'>
                  {errors.address.state.message}
                </FormErrorMessage>
              )}
            </>
          )}

          <div className='flex items-center justify-center gap-3'>
            <p className='first-letter:uppercase'>
              {translations.form?.shipping_different ??
                'My shipping address is different: '}
            </p>
            <Controller
              name='shippingDifferent'
              control={control}
              defaultValue={false}
              render={({ field }) => (
                <Switch
                  toggle={field.onChange}
                  toggleIsOn={field.value}
                  className='bg-primary-200 text-xl md:text-2xl'
                  isOnClassName='bg-green-400'
                />
              )}
            />
          </div>

          {!watch('shippingDifferent') && (
            <input
              className='cursor-pointer rounded-full bg-primary-200 px-6 py-2'
              type='submit'
              value={translations.submit ?? 'envoyer'}
            />
          )}
        </div>

        {watch('shippingDifferent') && (
          <div className='flex max-w-md flex-1 flex-col items-center gap-3'>
            <h2 className='text-center first-letter:uppercase'>
              {translations.form?.shipping_address ?? 'Shipping address'}
            </h2>
            <FormInput<AddressFormType>
              name='shipping.name'
              placeholder={
                translations.form?.name_placeholder ?? 'Clara Martinez'
              }
              label='Full name'
              className='w-full rounded-full border-2 border-carbon-900'
              errorClassName='w-full text-center mx-3 mb-2 text-red-600'
              register={register}
              errors={errors}
            />

            <FormInput<AddressFormType>
              name='shipping.city'
              placeholder={translations.form?.city_placeholder ?? 'Paris'}
              label='City'
              className='w-full rounded-full border-2 border-carbon-900'
              errorClassName='w-full text-center mx-3 mb-2 text-red-600'
              register={register}
              errors={errors}
            />

            <Controller
              name='shipping.country'
              render={({ field }) => (
                <CountryDropdown
                  defaultOptionLabel={
                    translations.form?.country_dropdown ?? '-- Pays --'
                  }
                  name={field.name}
                  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                  value={field.value!}
                  valueType='short'
                  onChange={field.onChange}
                  classes='w-full border-2 rounded-full border-carbon-900'
                />
              )}
              control={control}
            />
            {errors.shipping?.country && (
              <FormErrorMessage className='mx-3 mb-2 w-full text-center text-red-600'>
                {errors.shipping.country.message}
              </FormErrorMessage>
            )}

            <FormInput<AddressFormType>
              name='shipping.line1'
              placeholder={
                translations.form?.line_placeholder ?? '1 rue de la paix'
              }
              label='line 1'
              className='w-full rounded-full border-2 border-carbon-900'
              errorClassName='w-full text-center mx-3 mb-2 text-red-600'
              register={register}
              errors={errors}
            />

            <FormInput<AddressFormType>
              name='shipping.line2'
              label='line 2'
              className='w-full rounded-full border-2 border-carbon-900'
              errorClassName='w-full text-center mx-3 mb-2 text-red-600'
              register={register}
              errors={errors}
            />

            <FormInput<AddressFormType>
              name='shipping.postal_code'
              placeholder={
                translations.form?.postal_code_placeholder ?? '75001'
              }
              label='Postal code'
              className='w-full rounded-full border-2 border-carbon-900'
              errorClassName='text-center mx-3 mb-2 text-red-600'
              register={register}
              errors={errors}
            />

            {watch().shipping?.country && (
              <>
                <Controller
                  name='shipping.state'
                  render={({ field: { name, onChange, value } }) => (
                    <RegionDropdown
                      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                      country={watch().shipping.country!}
                      countryValueType='short'
                      defaultOptionLabel={
                        translations.form?.state_dropdown ?? '-- Région --'
                      }
                      name={name}
                      value={value ?? ''}
                      onChange={onChange}
                      classes='w-full border-2 rounded-full border-carbon-900'
                    />
                  )}
                  control={control}
                />
                {errors.shipping?.state && (
                  <FormErrorMessage className='mx-3 mb-2 w-full text-center text-red-600'>
                    {errors.shipping.state.message}
                  </FormErrorMessage>
                )}
              </>
            )}

            <input
              className='cursor-pointer rounded-full bg-primary-200 px-6 py-2'
              type='submit'
              value={translations.submit ?? 'envoyer'}
            />
          </div>
        )}
      </form>
    </>
  );
};

export default AddressForm;
