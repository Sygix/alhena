'use client';

import { CSSProperties, ReactNode } from 'react';
import {
  FieldErrors,
  FieldValues,
  Path,
  RegisterOptions,
  UseFormRegister,
} from 'react-hook-form';

import clsxm from '@/lib/clsxm';
import { listTypeEnum } from '@/lib/interfaces';

import FormCheckbox from '@/components/elements/forms/molecules/FormCheckbox';
import FormInput from '@/components/elements/forms/molecules/FormInput';
import FormRadio from '@/components/elements/forms/molecules/FormRadio';

const fieldMolecule = {
  Checkbox: FormCheckbox,
  Radio: FormRadio,
  Input: FormInput,
};

export type FilterFieldsetProps = {
  legend: ReactNode;
  fieldComponent: keyof typeof fieldMolecule;
  fieldset: { value: string; label: string }[];
  fieldType: string;
  listType?: listTypeEnum;
};

type FormFilterFieldsetProps<TFormValues extends FieldValues> = {
  name: Path<TFormValues>;
  className?: string;
  style?: CSSProperties;
  rules?: RegisterOptions;
  register?: UseFormRegister<TFormValues>;
  errors?: FieldErrors<TFormValues>;
};

const FilterFieldset = <TFormValues extends Record<string, unknown>>({
  className,
  style,
  legend,
  fieldComponent,
  fieldset,
  fieldType,
  listType = listTypeEnum.numbers,
  register,
  name,
}: FilterFieldsetProps & FormFilterFieldsetProps<TFormValues>) => {
  const Field = fieldMolecule[fieldComponent];

  return (
    <fieldset className={clsxm(className)} style={style}>
      <span className='group relative flex max-w-[12rem] flex-col'>
        <span className='absolute top-0 -z-10 h-full w-full rounded-full bg-white/10 backdrop-blur-lg'></span>
        <legend className='flex w-full max-w-full items-center gap-1 truncate rounded-full border-2 border-carbon-900 px-3'>
          {legend}
        </legend>

        <div className='relative -z-20 w-full'>
          <ol
            className='absolute -top-3 hidden w-full rounded-2xl rounded-t-none border-2 border-t-0 border-carbon-900 first:pt-3 group-hover:block'
            type={listType}
          >
            <span className='absolute -z-10 -mt-3 h-full w-full rounded-2xl rounded-t-none bg-white/10 backdrop-blur-lg'></span>

            <div className='mx-2 my-2'>
              {fieldset.map((field, index) => {
                const fieldPath =
                  fieldComponent === 'Radio'
                    ? name
                    : fieldType === 'price'
                    ? (`${field.value}` as Path<TFormValues>)
                    : (`${name}.${field.value}` as Path<TFormValues>);

                return (
                  <li
                    key={`${field} - ${index}`}
                    className={clsxm(
                      'group/filter-field line-clamp-2 flex w-full max-w-full items-center',
                      fieldType === 'price' && 'flex-wrap-reverse gap-1'
                    )}
                  >
                    {fieldType === 'price' ? (
                      <Field
                        name={fieldPath}
                        id={field.value}
                        label={field.value}
                        type='number'
                        register={register}
                        className={clsxm(
                          fieldComponent === 'Input' &&
                            'w-full rounded-md border-2 border-carbon-900 p-0'
                        )}
                      />
                    ) : (
                      <Field
                        name={fieldPath}
                        id={field.value}
                        label={field.value}
                        register={register}
                        value={field.value}
                        className={clsxm(
                          fieldComponent === 'Radio' && 'peer/radio sr-only',
                          fieldComponent === 'Checkbox' &&
                            'ml-1 mr-1 rounded-sm border-2 border-carbon-900 checked:bg-primary-300 checked:hover:bg-primary-400 focus:ring-primary-300 focus:ring-offset-2 focus:checked:bg-primary-300'
                        )}
                      />
                    )}
                    <label
                      htmlFor={field.value}
                      className={clsxm(
                        fieldComponent === 'Radio' &&
                          'group-hover/filter-field:cursor-pointer peer-checked/radio:text-primary-300'
                      )}
                    >
                      {field.label}
                    </label>
                  </li>
                );
              })}
            </div>
          </ol>
        </div>
      </span>
    </fieldset>
  );
};

export default FilterFieldset;
