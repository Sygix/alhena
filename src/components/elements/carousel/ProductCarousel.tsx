'use client';

import clsxm from '@/lib/clsxm';
import { MediaUrl } from '@/lib/helper';
import { Product } from '@/lib/interfaces';

import CarouselItem from '@/components/elements/carousel/CarouselItem';
import EmblaCarousel from '@/components/elements/carousel/EmblaCarousel';
import NextImage from '@/components/NextImage';

type MediaType = Pick<Product['attributes']['medias'], 'data'>;

const ProductCarousel = ({
  medias,
  className,
  itemClassName,
  imgSizes,
}: {
  medias: MediaType;
  className?: string;
  itemClassName?: string;
  imgSizes?: string;
}) => {
  return (
    <EmblaCarousel
      className={clsxm('flex h-full', className)}
      containerClassName='w-full'
    >
      {medias.data.map((media, index) => (
        <CarouselItem
          key={media.id + media.attributes.url}
          index={index}
          className='mr-3 h-full w-full last-of-type:mr-0'
        >
          {media.attributes.mime.startsWith('video/') ? (
            <video
              autoPlay={true}
              muted={true}
              loop={true}
              width={media.attributes.width}
              height={media.attributes.height}
              className={clsxm(
                'mr-1 h-full w-full min-w-0 flex-[0_0_100%] object-cover object-center',
                itemClassName
              )}
              webkit-playsinline='true'
              playsInline
            >
              <source
                src={MediaUrl(media.attributes.url)}
                type={media.attributes.mime}
              />
              <meta itemProp='name' content={media.attributes.name} />
              <meta
                itemProp='description'
                content={media.attributes.alternativeText}
              />
            </video>
          ) : (
            <NextImage
              useSkeleton
              src={MediaUrl(media.attributes.url)}
              width={media.attributes.width}
              height={media.attributes.height}
              title={media.attributes.name}
              alt={media.attributes.alternativeText ?? ''}
              className={clsxm(
                'mr-1 h-full w-full min-w-0 flex-[0_0_100%]',
                itemClassName
              )}
              imgClassName='w-full h-full object-center object-cover '
              sizes={imgSizes}
            />
          )}
        </CarouselItem>
      ))}
    </EmblaCarousel>
  );
};

export default ProductCarousel;
