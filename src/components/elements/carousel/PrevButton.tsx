'use client';

import IconButton from '@/components/elements/buttons/IconButton';
import { useCarouselContext } from '@/components/elements/carousel/EmblaCarousel';

const PrevButton = (props: { className?: string }) => {
  const { carouselApi } = useCarouselContext();

  const handleClick = () => {
    if (!carouselApi) return;
    if (carouselApi.canScrollPrev()) {
      carouselApi.scrollPrev();
    }
  };

  return (
    <IconButton
      variant='ghost'
      className={props.className}
      onClick={handleClick}
      icon='ic:round-chevron-left'
      iconClassName='text-4xl'
    />
  );
};

export default PrevButton;
