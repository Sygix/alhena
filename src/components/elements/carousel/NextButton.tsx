'use client';

import IconButton from '@/components/elements/buttons/IconButton';
import { useCarouselContext } from '@/components/elements/carousel/EmblaCarousel';

const NextButton = (props: { className?: string }) => {
  const { carouselApi } = useCarouselContext();

  const handleClick = () => {
    if (!carouselApi) return;
    if (carouselApi.canScrollNext()) {
      carouselApi.scrollNext();
    }
  };

  return (
    <IconButton
      variant='ghost'
      className={props.className}
      onClick={handleClick}
      icon='ic:round-chevron-right'
      iconClassName='text-4xl'
    />
  );
};

export default NextButton;
