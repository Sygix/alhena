'use client';

import { Icon } from '@iconify/react';
import { useState } from 'react';

import Skeleton from '@/components/Skeleton';

export type DynamicIconProps = {
  icon: string;
  className?: string;
  wrapperClassName?: string;
};

const DynamicIcon = ({
  icon,
  className,
  wrapperClassName,
}: DynamicIconProps) => {
  const [loading, setLoading] = useState(true);

  return (
    <div className={wrapperClassName}>
      {loading && (
        <Skeleton className='h-full w-full rounded-full opacity-40' />
      )}
      <Icon
        icon={icon}
        className={className}
        onLoad={() => setLoading(false)}
      />
    </div>
  );
};

export default DynamicIcon;
