'use client';

import dynamic from 'next/dynamic';
import React, { useEffect, useState } from 'react';

import clsxm from '@/lib/clsxm';

const MotionDiv = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.div)
);

type SwitchProps = {
  isDark?: boolean;
  toggleIsOn?: boolean;
  toggle?: (isOn: boolean) => void;
  isOnClassName?: string;
} & React.ComponentPropsWithRef<'div'>;

const Switch = React.forwardRef<HTMLDivElement, SwitchProps>(
  (
    {
      className,
      isDark = false,
      toggleIsOn = false,
      toggle,
      isOnClassName,
      ...rest
    },
    ref
  ) => {
    const [isOn, setIsOn] = useState(toggleIsOn);
    const toggleSwitch = () => {
      if (toggle !== undefined) toggle(!isOn);
      setIsOn(!isOn);
    };

    useEffect(() => {
      setIsOn(toggleIsOn);
    }, [toggleIsOn]);

    const spring = {
      type: 'spring',
      stiffness: 700,
      damping: 30,
    };

    return (
      <div
        ref={ref}
        className={clsxm(
          isDark ? 'bg-dark/40' : 'bg-gray-200/40',
          'flex h-[1em] w-[2em] cursor-pointer justify-start rounded-full p-[0.1em] transition-colors duration-300',
          isOn && 'justify-end',
          className,
          isOn && isOnClassName
        )}
        onClick={toggleSwitch}
        {...rest}
      >
        <MotionDiv
          className='h-[0.8em] w-[0.8em] rounded-full bg-white'
          layout
          transition={spring}
        />
      </div>
    );
  }
);

export default Switch;
