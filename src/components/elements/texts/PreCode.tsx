'use client';

import { ReactNode, useRef } from 'react';

import DynamicIcon from '@/components/elements/DynamicIcon';

import { useToaster } from '@/store/toasterStore';

const PreCode = (props: { children?: ReactNode }) => {
  const preRef = useRef<HTMLPreElement | null>(null);
  const notify = useToaster((state) => state.notify);

  const copyToClipboard = () => {
    if (!preRef.current) return;

    const clipboard = navigator.clipboard;

    if (!clipboard) return;

    clipboard.writeText(preRef.current.innerText).then(() => {
      notify(0, <p>!Copied to your clipboard !</p>);
    });
  };

  return (
    <div className='relative group/pre-code bg-carbon-200 rounded-lg p-1'>
      <button
        onClick={copyToClipboard}
        className='absolute top-4 right-4 p-1 opacity-0 group-hover/pre-code:opacity-100 bg-carbon-200 rounded-full text-xl text-carbon-700 hover:text-carbon-950 transition-all duration-300'
      >
        <DynamicIcon icon='tabler:clipboard' />
      </button>

      <pre ref={preRef}>{props.children}</pre>
    </div>
  );
};

export default PreCode;
