/* eslint-disable @typescript-eslint/no-explicit-any */
import { MDXRemote } from 'next-mdx-remote/rsc';
import remarkEmoji from 'remark-emoji';
import remarkGfm from 'remark-gfm';

import MdxImage from '@/components/elements/images/MdxImage';
import P from '@/components/elements/texts/P';
import PreCode from '@/components/elements/texts/PreCode';
import Table from '@/components/elements/texts/Table';

const components = {
  pre: PreCode,
  table: Table,
  p: P,
  img: MdxImage,
};

const RemoteMDX = (props: { source: string }) => {
  return (
    <MDXRemote
      source={props.source}
      options={{
        mdxOptions: {
          remarkPlugins: [
            remarkGfm,
            [remarkEmoji as any, { accessible: true }],
          ],
          rehypePlugins: [],
          format: 'mdx',
        },
      }}
      components={components}
    />
  );
};

export default RemoteMDX;
