'use client';

import { ReactNode } from 'react';

import clsxm from '@/lib/clsxm';
import { ProductType } from '@/lib/interfaces';

import Button from '@/components/elements/buttons/Button';
import { useAddToCartContext } from '@/components/elements/cart/AddToCart';

const TypeSelect = ({
  type,
  children,
  className,
  disabled,
}: {
  type: ProductType;
  children?: ReactNode;
  className?: string;
  disabled?: boolean;
}) => {
  const { selectedType, setSelectedType } = useAddToCartContext();

  return (
    <Button
      variant='primary'
      disabled={disabled}
      className={clsxm(className, selectedType === type.name && 'active')}
      onClick={() => setSelectedType(type.name)}
    >
      {children}
    </Button>
  );
};

export default TypeSelect;
