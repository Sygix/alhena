import { toFixedNumber } from '@/lib/helper';

import useStore from '@/store';
import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';

const Discount = () => {
  const cartState = useStore(useCart, (state) => state);
  if (!cartState?.discount) return null;

  const translations = useServer.getState().translations;

  const totalPrice = cartState.totalPrice;
  const discountType = cartState.discount.attributes.type;
  const discountValue =
    discountType === 'gift_card'
      ? cartState.discount.attributes.value_left
      : cartState.discount.attributes.value;

  const discountAmount = toFixedNumber(
    discountType === 'percentage'
      ? (totalPrice * discountValue) / 100
      : discountValue > totalPrice
      ? totalPrice
      : discountValue,
    2
  );
  const subTotal = toFixedNumber(totalPrice - discountAmount, 2);

  return (
    <>
      <div className='border-dark flex items-center justify-between font-bold'>
        <p className='font-semibold first-letter:uppercase'>
          {translations.cart?.discount_code ?? 'promotion: '}
          {cartState.discount.attributes.code}
        </p>
        <p className='h4 first-letter:uppercase'>
          {discountType === 'percentage'
            ? `-${discountValue} %`
            : `-${discountAmount} €`}
        </p>
      </div>
      <div className='border-dark flex items-center justify-between font-bold'>
        <p className='font-semibold first-letter:uppercase'>
          {translations.sub_total ?? 'sous-total'}
        </p>
        <p className='h4 first-letter:uppercase'>{subTotal} €</p>
      </div>
    </>
  );
};

export default Discount;
