'use client';

import { useRouter } from 'next/navigation';
import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';

import { includeLocaleLink, isOnSale, umamiAnalytics } from '@/lib/helper';
import { Product } from '@/lib/interfaces';

import Button from '@/components/elements/buttons/Button';
import { QtyBtn } from '@/components/elements/cart/QtyBtn';
import ButtonLink from '@/components/elements/links/ButtonLink';

import { useCart } from '@/store/cartStore';
import { useServer } from '@/store/serverStore';
import { useToaster } from '@/store/toasterStore';

const AddToCartContext = createContext<
  | {
      selectedSize: string;
      selectedType: string;
      setSelectedSize: Dispatch<SetStateAction<string>>;
      setSelectedType: Dispatch<SetStateAction<string>>;
      product: Product;
    }
  | undefined
  | null
>(null);

export const useAddToCartContext = () => {
  const context = useContext(AddToCartContext);
  if (!context) {
    throw new Error('useAddToCartContext must be used within a AddToCart');
  }
  return context;
};

const AddToCart = ({
  children,
  product,
  containerClassName,
  btnClassName,
  hidePrice = false,
  hideContinueShopping = true,
}: {
  children?: ReactNode;
  product: Product;
  containerClassName?: string;
  btnClassName?: string;
  hidePrice?: boolean;
  hideContinueShopping?: boolean;
}) => {
  const router = useRouter();

  const cartPage = useServer((state) => state.cart_page);
  const { increment, decrement } = useCart((state) => ({
    increment: state.increment,
    decrement: state.decrement,
  }));
  const [selectedSize, setSelectedSize] = useState(
    product.attributes.gift_card ? product.attributes.sizes[0].size : ''
  );
  const [selectedType, setSelectedType] = useState<string>('');

  const cartItems = useCart((state) => state.cartItems);

  const qty = useMemo(
    () =>
      cartItems.find(
        (item) =>
          item.product.id === product.id &&
          item.product.selectedSize === selectedSize &&
          item.product.selectedType === selectedType
      )?.qty ?? 0,
    [cartItems, product.id, selectedSize, selectedType]
  );

  const price = isOnSale(
    product.attributes.date_on_sale_from,
    product.attributes.date_on_sale_to
  )
    ? product.attributes.sale_price ?? product.attributes.price
    : product.attributes.price;

  const notify = useToaster((state) => state.notify);
  const translations = useServer.getState().translations;

  const handleAddToCart = () => {
    if (selectedSize) {
      // Check if product has types and if user has selected one, but attributes.types can be undefined
      if (product.attributes.types && selectedType === '') {
        if (product.attributes.types.length > 0) {
          notify(
            3,
            <p>
              {translations.toast?.must_select_type ??
                'Please select a type of product first'}
            </p>
          );
          return;
        }
      }
      umamiAnalytics('product-add-cart', {
        product: { id: product.id, slug: product.attributes.slug },
        qty: qty + 1,
        size: selectedSize,
      });

      increment({ ...product, selectedSize, selectedType });
      notify(0, <p>{translations.toast?.product_added}</p>);
    } else {
      notify(3, <p>{translations.toast?.must_select_size}</p>);
    }
  };

  useEffect(
    function updateSelectedSizeIfGiftCard() {
      if (product.attributes.gift_card) {
        setSelectedSize(product.attributes.sizes[0].size);
      }
    },
    [product]
  );

  return (
    <AddToCartContext.Provider
      value={{
        selectedSize,
        setSelectedSize,
        selectedType,
        setSelectedType,
        product,
      }}
    >
      {children}

      <div className={containerClassName}>
        {qty <= 0 && (
          <Button
            variant='primary'
            onClick={handleAddToCart}
            className={btnClassName}
          >
            {translations.cart?.add_btn}
          </Button>
        )}
        {qty > 0 && (
          <>
            <div className='flex items-center gap-3'>
              <QtyBtn
                onIncrease={() => {
                  umamiAnalytics('product-plus-cart', {
                    product: { id: product.id, slug: product.attributes.slug },
                    qty: qty + 1,
                    size: selectedSize,
                  });

                  increment({ ...product, selectedSize, selectedType });
                }}
                onDecrease={() => {
                  umamiAnalytics('product-minus-cart', {
                    product: { id: product.id, slug: product.attributes.slug },
                    qty: qty + 1,
                    size: selectedSize,
                  });

                  decrement({ ...product, selectedSize, selectedType });
                }}
                qty={qty}
              />
            </div>
          </>
        )}

        {!hidePrice && <p>{price}€</p>}
      </div>
      {!hideContinueShopping && qty > 0 && (
        <div className='flex flex-col gap-3 xs:flex-row'>
          <ButtonLink href={includeLocaleLink(cartPage)}>
            <span className='first-letter:uppercase'>
              {translations.cart?.go_to_cart ?? 'voir mon panier'}
            </span>
          </ButtonLink>
          <Button onClick={() => router.back()}>
            <span className='first-letter:uppercase'>
              {translations.cart?.continue_shopping ?? 'continuer mes achats'}
            </span>
          </Button>
        </div>
      )}
    </AddToCartContext.Provider>
  );
};

export default AddToCart;
