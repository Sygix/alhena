'use client';

import { useState } from 'react';

import { Product } from '@/lib/interfaces';

import AddToCart from '@/components/elements/cart/AddToCart';

const GiftCardDropDown = ({ products }: { products: Product[] }) => {
  const [selectedProduct, setSelectedProduct] = useState<Product>(products[0]);

  return (
    <div className='flex w-full max-w-sm gap-3 font-semibold'>
      <select
        value={selectedProduct.id}
        onChange={(e) => {
          const selected = products.find(
            (product) => product.id.toString() === e.target.value
          );
          if (selected) {
            setSelectedProduct(selected);
          }
        }}
        className='w-1/3 appearance-none rounded-full border-none bg-primary-200 px-4 py-2 text-sm'
      >
        {products.map((product) => (
          <option key={product.id} value={product.id}>
            {product.attributes.price} €
          </option>
        ))}
      </select>
      <AddToCart
        containerClassName='w-2/3 flex justify-center'
        btnClassName='w-full py-2 px-4 bg-primary-200 rounded-full text-sm justify-center'
        product={selectedProduct}
        hidePrice
      />
    </div>
  );
};

export default GiftCardDropDown;
