'use client';

import { ReactNode } from 'react';

import clsxm from '@/lib/clsxm';
import { ProductSize } from '@/lib/interfaces';

import Button from '@/components/elements/buttons/Button';
import { useAddToCartContext } from '@/components/elements/cart/AddToCart';

const SizeSelect = ({
  size,
  children,
  className,
  disabled,
}: {
  size: ProductSize;
  children?: ReactNode;
  className?: string;
  disabled?: boolean;
}) => {
  const { selectedSize, setSelectedSize } = useAddToCartContext();

  return (
    <Button
      variant='primary'
      disabled={disabled}
      className={clsxm(className, selectedSize === size.size && 'active')}
      onClick={() => setSelectedSize(size.size)}
    >
      {children}
    </Button>
  );
};

export default SizeSelect;
