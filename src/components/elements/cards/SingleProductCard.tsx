import Link from 'next/link';

import { includeLocaleLink, isOnSale, MediaUrl } from '@/lib/helper';
import { Product } from '@/lib/interfaces';

import ProductCarousel from '@/components/elements/carousel/ProductCarousel';
import NextImage from '@/components/NextImage';

import { useServer } from '@/store/serverStore';

const SingleProductCard = ({
  product,
  imgSizes,
  showDescription = false,
  colorSwitch,
}: {
  product: Product;
  imgSizes?: string;
  showDescription?: boolean;
  colorSwitch?: (id: number) => void;
}) => {
  const {
    title,
    slug,
    price,
    new_product_tag,
    sale_price,
    date_on_sale_from,
    date_on_sale_to,
    medias,
    short_description,
    colors,
    gift_card,
  } = product.attributes;

  const { translations } = useServer.getState();

  const sale =
    (sale_price && isOnSale(date_on_sale_from, date_on_sale_to)) || false;

  return (
    <article className='flex flex-col gap-4'>
      <Link
        href={includeLocaleLink(`/${slug}`)}
        title={title}
        className='flex flex-col gap-4'
        data-analytics-event='product-card'
        data-analytics-event-product={slug}
      >
        <div className='relative aspect-[2/3] h-full overflow-hidden rounded-md'>
          <ProductCarousel medias={medias} imgSizes={imgSizes} />
          {(sale || new_product_tag) && (
            <div className='absolute bottom-0 right-0 rounded-tl-xl bg-white px-3 py-1'>
              <p className='h4 font-bold uppercase'>
                {sale ? translations.promotion_tag : translations.new_tag}
              </p>
            </div>
          )}
        </div>
        <div className='flex items-center justify-between gap-2 whitespace-nowrap font-bold'>
          <h4 className='truncate'>{title}</h4>
          {(sale && (
            <p className='max-w-full text-lg text-primary-200'>
              <span>{sale_price} | </span>
              <s className='line-through decoration-2'>{price}</s> €
            </p>
          )) || (
            <p className='max-w-full text-lg text-primary-200'>{price} €</p>
          )}
        </div>
        {showDescription && short_description && (
          <p className='text-gray-500'>{short_description}</p>
        )}
      </Link>
      {!gift_card && colorSwitch && colors && colors.length > 0 && (
        <p className='flex flex-wrap items-center gap-4 font-bold text-gray-500'>
          {colors.map((color, index) => (
            <button
              key={`${color.name} - ${index}`}
              style={{ backgroundColor: color.color }}
              className='aspect-square w-4 shrink-0 rounded-full border border-carbon-900'
              onClick={() => colorSwitch && colorSwitch(color.product.data.id)}
            >
              {color.image.data && (
                <NextImage
                  className='relative aspect-square w-full overflow-hidden'
                  imgClassName='z-10 w-full h-full object-center object-cover rounded-md'
                  width={color.image.data.attributes.width}
                  height={color.image.data.attributes.height}
                  src={MediaUrl(color.image.data.attributes.url)}
                  title={color.image.data.attributes.name}
                  alt={color.image.data.attributes.alternativeText ?? ''}
                  quality={50}
                  sizes='5vw'
                />
              )}
            </button>
          ))}
          {colors.length} {translations.colors}
        </p>
      )}
    </article>
  );
};

export default SingleProductCard;
