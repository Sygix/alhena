'use client';

import Link from 'next/link';

import clsxm from '@/lib/clsxm';
import { includeLocaleLink, MediaUrl, toFixedNumber } from '@/lib/helper';
import { CartItem } from '@/lib/interfaces';

import { QtyBtn } from '@/components/elements/cart/QtyBtn';
import NextImage from '@/components/NextImage';

import { useCart } from '@/store/cartStore';

type CartItemCardType = {
  cartItem: CartItem;
  className?: string;
  displayQtyBtn?: boolean;
};

export const CartItemCard = ({
  cartItem,
  className,
  displayQtyBtn = false,
}: CartItemCardType) => {
  const { increment, decrement } = useCart((state) => ({
    increment: state.increment,
    decrement: state.decrement,
  }));
  const { title, medias, slug, gift_card } = cartItem.product.attributes ?? {};
  const totalPrice = toFixedNumber(cartItem.price * cartItem.qty, 2);

  return (
    <div
      className={clsxm(
        'grid w-full auto-cols-auto grid-cols-4 gap-3 md:gap-8',
        className
      )}
    >
      <Link
        href={includeLocaleLink(`/${slug}`)}
        title={title}
        data-analytics-event='cart-back-product'
        data-analytics-event-product-title={title}
        data-analytics-event-product-id={cartItem.product.id}
        className='relative flex aspect-square max-w-[8rem]'
      >
        {medias && (
          <NextImage
            src={MediaUrl(medias.data[0].attributes.url)}
            useSkeleton
            priority
            width={medias.data[0].attributes.width}
            height={medias.data[0].attributes.height}
            title={medias.data[0].attributes.name}
            alt={medias.data[0].attributes.alternativeText ?? ''}
            className='overflow-hidden rounded-full'
            imgClassName='object-cover object-center w-full h-full'
            sizes='50vw'
          />
        )}
        <span className='absolute -top-0 right-0 flex aspect-square h-[1em] w-[1em] items-center justify-center rounded-full border-2 border-carbon-900 p-2 text-[.8em] font-bold backdrop-blur-sm'>
          {cartItem.qty}
        </span>
      </Link>
      <Link
        href={includeLocaleLink(slug)}
        title={title}
        data-analytics-event='cart-back-product'
        data-analytics-event-product-title={title}
        data-analytics-event-product-id={cartItem.product.id}
        className='col-span-2 flex flex-col justify-center'
      >
        <p className='font-bold text-carbon-900'>{title}</p>
        {!gift_card && (
          <span className='font-normal text-carbon-400'>
            {cartItem.size.replace('_', '')}
            {cartItem.color && ` - ${cartItem.color.replace('_', '')}`}
            {cartItem.type && ` - ${cartItem.type.replace('_', '')}`}
          </span>
        )}
      </Link>
      <div className='flex flex-col items-end justify-center gap-4'>
        <p className='max-w-full truncate'>{totalPrice} €</p>
        {displayQtyBtn && (
          <div className='flex flex-nowrap items-center justify-center gap-1'>
            <QtyBtn
              onIncrease={() => increment(cartItem.product)}
              onDecrease={() => decrement(cartItem.product)}
              qty={cartItem.qty}
            />
          </div>
        )}
      </div>
    </div>
  );
};
