'use client';

import dynamic from 'next/dynamic';
import { forwardRef, useRef } from 'react';
import Xarrow from 'react-xarrows';

import clsxm from '@/lib/clsxm';
import { MediaUrl } from '@/lib/helper';
import { Media } from '@/lib/interfaces';

import NextImage from '@/components/NextImage';

const Xwrapper = dynamic(() =>
  import('react-xarrows').then((mod) => mod.Xwrapper)
);
// Dynamic import of Xarrow cause the style to be different is there are mulitple arrows in map

type StepProps = {
  description?: string;
  image: { data: Media };
  accent_color: string;
  rotate: number;
  mobile_col_start?: number;
  mobile_row_start?: number;
  tablet_col_start?: number;
  tablet_row_start?: number;
  desktop_col_start?: number;
  desktop_row_start?: number;
};

const Step = forwardRef<HTMLDivElement, StepProps>(
  (
    {
      description,
      image,
      rotate,
      accent_color,
      mobile_col_start,
      mobile_row_start,
      tablet_col_start,
      tablet_row_start,
      desktop_col_start,
      desktop_row_start,
    },
    ref
  ) => {
    return (
      <div
        className={clsxm(
          'col-span-2 mx-3 my-6 flex flex-col gap-1 md:row-span-2 md:mx-12',
          mobile_col_start && `col-start-${mobile_col_start}`,
          mobile_row_start && `row-start-${mobile_row_start}`,
          tablet_col_start && `md:col-start-${tablet_col_start}`,
          tablet_row_start && `md:row-start-${tablet_row_start}`,
          desktop_col_start && `xl:col-start-${desktop_col_start}`,
          desktop_row_start && `xl:row-start-${desktop_row_start}`
        )}
        ref={ref}
      >
        <div className='relative'>
          <div
            className='absolute left-0 top-0 h-full w-full rounded-md'
            style={{
              transform: `rotate(${rotate}deg)`,
              backgroundColor: accent_color,
            }}
          ></div>
          <NextImage
            className='relative aspect-square w-full overflow-hidden'
            imgClassName='z-10 w-full h-full object-center object-cover rounded-md'
            width={image.data.attributes.width}
            height={image.data.attributes.height}
            src={MediaUrl(image.data.attributes.url)}
            title={image.data.attributes.name}
            alt={image.data.attributes.alternativeText ?? ''}
            sizes='50vw'
          />
        </div>
        <p>{description}</p>
      </div>
    );
  }
);

const XConcept = ({
  steps,
}: {
  steps: {
    id: number;
    description?: string;
    image: {
      data: Media;
    };
    accent_color: string;
    rotate: number;
    mobile_col_start?: number;
    mobile_row_start?: number;
    tablet_col_start?: number;
    tablet_row_start?: number;
    desktop_col_start?: number;
    desktop_row_start?: number;
  }[];
}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const stepsRef = steps.map(() => useRef(null));

  return (
    <div className='grid w-full grid-cols-4 gap-3 md:grid-cols-8 xl:grid-cols-12 xl:gap-6'>
      <Xwrapper>
        {steps.map((step, i) => (
          <Step key={step.id} {...step} ref={stepsRef[i]} />
        ))}
        {steps.slice(0, -1).map((step, i) => (
          <Xarrow
            key={i}
            start={stepsRef[i]}
            end={stepsRef[i + 1]}
            color='#1e1e1e'
            strokeWidth={2}
            dashness={{ strokeLen: 4, nonStrokeLen: 4 }}
          />
        ))}
      </Xwrapper>
    </div>
  );
};

export default XConcept;
