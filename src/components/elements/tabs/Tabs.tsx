'use client';

import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useContext,
  useState,
} from 'react';

const TabsContext = createContext<{
  selectedTab: number;
  setSelectedTab?: Dispatch<SetStateAction<number>>;
  productSlug?: string;
}>({
  selectedTab: 0,
  setSelectedTab: () => null,
});

export const useTabsContext = () => {
  const context = useContext(TabsContext);
  if (!context) {
    throw new Error('useTabsContext must be used within a Tabs');
  }
  return context;
};

const Tabs = ({
  children,
  productSlug,
}: {
  children: ReactNode;
  productSlug?: string;
}) => {
  const [selectedTab, setSelectedTab] = useState(0);

  return (
    <TabsContext.Provider value={{ selectedTab, setSelectedTab, productSlug }}>
      {children}
    </TabsContext.Provider>
  );
};

export default Tabs;
