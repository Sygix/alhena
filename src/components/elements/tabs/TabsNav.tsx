'use client';

import { ReactNode } from 'react';

import { umamiAnalytics } from '@/lib/helper';

import { useTabsContext } from '@/components/elements/tabs/Tabs';

const TabsNav = ({
  children,
  className,
  ulClassName,
  liClassName,
}: {
  children?: ReactNode[];
  className?: string;
  ulClassName?: string;
  liClassName?: string;
}) => {
  const { setSelectedTab, productSlug } = useTabsContext();

  return (
    <nav className={className}>
      <ul className={ulClassName}>
        {children?.map((item, index) => (
          <li
            key={index}
            className={liClassName}
            onClick={() => {
              umamiAnalytics('product-tab', { slug: productSlug, index });
              setSelectedTab && setSelectedTab(index);
            }}
            data-analytics-event='click_product_tab'
            data-analytics-event-tabindex={index}
          >
            {item}
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default TabsNav;
