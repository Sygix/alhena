'use client';

import dynamic from 'next/dynamic';
import { ReactNode } from 'react';

import { useTabsContext } from '@/components/elements/tabs/Tabs';

const AnimatePresence = dynamic(() =>
  import('framer-motion').then((mod) => mod.AnimatePresence)
);
const MotionDiv = dynamic(() =>
  import('framer-motion').then((mod) => mod.motion.div)
);

const TabsContent = ({
  children,
  className,
}: {
  children?: ReactNode[];
  className?: string;
}) => {
  const { selectedTab } = useTabsContext();

  return (
    <AnimatePresence initial={false} mode='wait'>
      <MotionDiv
        key={selectedTab}
        initial={{ y: 10, opacity: 0 }}
        animate={{ y: 0, opacity: 1 }}
        exit={{ y: -10, opacity: 0 }}
        transition={{ duration: 0.2 }}
        className={className}
      >
        {children && children[selectedTab]}
      </MotionDiv>
    </AnimatePresence>
  );
};

export default TabsContent;
