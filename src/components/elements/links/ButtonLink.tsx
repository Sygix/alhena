import * as React from 'react';

import clsxm from '@/lib/clsxm';
import { ENUM_ELEMENTS_LINK_DIRECTION } from '@/lib/interfaces';

import DynamicIcon from '@/components/elements/DynamicIcon';
import UnstyledLink, {
  UnstyledLinkProps,
} from '@/components/elements/links/UnstyledLink';

const ButtonLinkVariant = [
  'primary',
  'outline',
  'ghost',
  'light',
  'dark',
] as const;
const ButtonLinkSize = ['sm', 'base', 'md', 'lg', 'xl'] as const;

export type ButtonLinkProps = {
  isDarkBg?: boolean;
  variant?: (typeof ButtonLinkVariant)[number];
  size?: (typeof ButtonLinkSize)[number];
  icon?: string;
  iconClassName?: string;
  direction?: ENUM_ELEMENTS_LINK_DIRECTION;
} & UnstyledLinkProps;

const ButtonLink = React.forwardRef<HTMLAnchorElement, ButtonLinkProps>(
  (
    {
      children,
      className,
      variant = 'primary',
      size = 'base',
      isDarkBg = false,
      icon,
      iconClassName,
      direction,
      ...rest
    },
    ref
  ) => {
    return (
      <UnstyledLink
        ref={ref}
        {...rest}
        className={clsxm(
          'inline-flex items-center rounded-full text-center font-bold',
          'focus:outline-none focus-visible:ring focus-visible:ring-primary-300',
          'transition-colors duration-75',
          //#region  //*=========== Size ===========
          [
            size === 'xl' && ['px-4 py-2', 'text-lg md:text-xl'],
            size === 'lg' && ['px-4 py-2', 'text-md md:text-lg'],
            size === 'md' && ['px-3 py-1.5', 'md:text-md text-base'],
            size === 'base' && ['px-3 py-1.5', 'text-sm md:text-base'],
            size === 'sm' && ['px-2 py-1', 'text-xs md:text-sm'],
          ],
          //#endregion  //*======== Size ===========
          //#region  //*=========== Variants ===========
          [
            variant === 'primary' && [
              'bg-primary-300 text-carbon-900',
              'hover:bg-primary-400',
              'active:bg-primary-400',
              'disabled:bg-primary-400',
            ],
            variant === 'outline' && [
              'text-primary-200',
              'border border-primary-200',
              'hover:bg-primary-300 active:bg-primary-300 disabled:bg-primary-300',
              isDarkBg &&
                'hover:text-carbon-900 active:text-carbon-800 disabled:text-carbon-700',
            ],
            variant === 'ghost' && [
              'text-primary-200',
              'shadow-none',
              'hover:bg-primary-300 active:bg-primary-300 disabled:bg-primary-300',
              isDarkBg &&
                'hover:text-carbon-900 active:text-carbon-800 disabled:text-carbon-700',
            ],
            variant === 'light' && [
              'bg-carbon-50 text-carbon-900',
              'border border-carbon-300',
              'hover:bg-carbon-100 hover:text-carbon-900',
              'active:bg-white/80 disabled:bg-carbon-200',
            ],
            variant === 'dark' && [
              'bg-carbon-900 text-white',
              'border border-carbon-600',
              'hover:bg-carbon-800 active:bg-carbon-700 disabled:bg-carbon-700',
            ],
          ],
          //#endregion  //*======== Variants ===========
          'disabled:cursor-not-allowed',
          className
        )}
      >
        {direction === 'left' && icon && (
          <div
            className={clsxm([
              size === 'base' && 'mr-1',
              size === 'sm' && 'mr-1.5',
            ])}
          >
            <DynamicIcon
              icon={icon}
              className={clsxm(
                [
                  size === 'base' && 'md:text-md text-md',
                  size === 'sm' && 'md:text-md text-sm',
                ],
                iconClassName
              )}
            />
          </div>
        )}
        {children}
        {(direction === 'right' || direction === undefined) && icon && (
          <div
            className={clsxm([
              size === 'base' && 'ml-1',
              size === 'sm' && 'ml-1.5',
            ])}
          >
            <DynamicIcon
              icon={icon}
              className={clsxm(
                [
                  size === 'base' && 'text-md md:text-md',
                  size === 'sm' && 'md:text-md text-sm',
                ],
                iconClassName
              )}
            />
          </div>
        )}
      </UnstyledLink>
    );
  }
);

export default ButtonLink;
