import * as React from 'react';

import clsxm from '@/lib/clsxm';
import { ENUM_ELEMENTS_LINK_DIRECTION } from '@/lib/interfaces';

import DynamicIcon from '@/components/elements/DynamicIcon';
import UnstyledLink, {
  UnstyledLinkProps,
} from '@/components/elements/links/UnstyledLink';

const PrimaryLinkVariant = ['primary', 'basic'] as const;
export type PrimaryLinkProps = {
  variant?: (typeof PrimaryLinkVariant)[number];
  icon?: string;
  iconClassName?: string;
  direction?: ENUM_ELEMENTS_LINK_DIRECTION;
} & UnstyledLinkProps;

const PrimaryLink = React.forwardRef<HTMLAnchorElement, PrimaryLinkProps>(
  (
    {
      className,
      children,
      variant = 'primary',
      icon,
      iconClassName,
      direction,
      ...rest
    },
    ref
  ) => {
    return (
      <UnstyledLink
        ref={ref}
        {...rest}
        className={clsxm(
          'inline-flex items-center',
          'focus-visible:ring-primary-500 focus:outline-none focus-visible:rounded focus-visible:ring focus-visible:ring-offset-2',
          'font-medium',
          //#region  //*=========== Variant ===========
          variant === 'primary' && [
            'text-primary-500 hover:text-primary-600 active:text-primary-700',
            'disabled:text-primary-200',
          ],
          variant === 'basic' && [
            'text-black hover:text-gray-600 active:text-gray-800',
            'disabled:text-gray-300',
          ],
          //#endregion  //*======== Variant ===========
          className
        )}
      >
        {direction === 'left' && icon && (
          <div className={clsxm(['base mr-1'])}>
            <DynamicIcon
              icon={icon}
              className={clsxm('base md:text-md text-md', iconClassName)}
            />
          </div>
        )}
        {children}
        {(direction === 'right' || direction === undefined) && icon && (
          <div className={clsxm(['base ml-1'])}>
            <DynamicIcon
              icon={icon}
              className={clsxm('base text-md md:text-md', iconClassName)}
            />
          </div>
        )}
      </UnstyledLink>
    );
  }
);

export default PrimaryLink;
