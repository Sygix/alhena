import type { Config } from 'tailwindcss';
import defaultTheme from 'tailwindcss/defaultTheme';

export default {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  safelist: [
    'col-start-1',
    'col-start-2',
    'col-start-3',
    'col-start-4',
    'col-start-5',
    'col-start-6',
    'col-start-7',
    'col-start-8',
    'col-start-9',
    'col-start-10',
    'col-start-11',
    'col-start-12',
    'row-start-1',
    'row-start-2',
    'row-start-3',
    'row-start-4',
    'row-start-5',
    'row-start-6',
    'row-start-7',
    'md:col-start-1',
    'md:col-start-2',
    'md:col-start-3',
    'md:col-start-4',
    'md:col-start-5',
    'md:col-start-6',
    'md:col-start-7',
    'md:col-start-8',
    'md:col-start-9',
    'md:col-start-10',
    'md:col-start-11',
    'md:col-start-12',
    'md:row-start-1',
    'md:row-start-2',
    'md:row-start-3',
    'md:row-start-4',
    'md:row-start-5',
    'md:row-start-6',
    'md:row-start-7',
    'xl:col-start-1',
    'xl:col-start-2',
    'xl:col-start-3',
    'xl:col-start-4',
    'xl:col-start-5',
    'xl:col-start-6',
    'xl:col-start-7',
    'xl:col-start-8',
    'xl:col-start-9',
    'xl:col-start-10',
    'xl:col-start-11',
    'xl:col-start-12',
    'xl:row-start-1',
    'xl:row-start-2',
    'xl:row-start-3',
    'xl:row-start-4',
    'xl:row-start-5',
    'xl:row-start-6',
    'xl:row-start-7',
  ],
  theme: {
    screens: {
      xs: '475px',
      ...defaultTheme.screens,
    },
    extend: {
      fontFamily: {
        primary: [
          'var(--font-noto-sans-display)',
          'var(--font-inter)',
          ...defaultTheme.fontFamily.sans,
        ],
      },
      colors: {
        primary: {
          // Customize it on globals.css :root
          50: 'rgb(var(--tw-color-primary-50) / <alpha-value>)',
          100: 'rgb(var(--tw-color-primary-100) / <alpha-value>)',
          200: 'rgb(var(--tw-color-primary-200) / <alpha-value>)',
          300: 'rgb(var(--tw-color-primary-300) / <alpha-value>)',
          400: 'rgb(var(--tw-color-primary-400) / <alpha-value>)',
          500: 'rgb(var(--tw-color-primary-500) / <alpha-value>)',
          600: 'rgb(var(--tw-color-primary-600) / <alpha-value>)',
          700: 'rgb(var(--tw-color-primary-700) / <alpha-value>)',
          800: 'rgb(var(--tw-color-primary-800) / <alpha-value>)',
          900: 'rgb(var(--tw-color-primary-900) / <alpha-value>)',
          950: 'rgb(var(--tw-color-primary-950) / <alpha-value>)',
        },
        secondary: {
          // Customize it on globals.css :root
          50: 'rgb(var(--tw-color-secondary-50) / <alpha-value>)',
          100: 'rgb(var(--tw-color-secondary-100) / <alpha-value>)',
          200: 'rgb(var(--tw-color-secondary-200) / <alpha-value>)',
          300: 'rgb(var(--tw-color-secondary-300) / <alpha-value>)',
          400: 'rgb(var(--tw-color-secondary-400) / <alpha-value>)',
          500: 'rgb(var(--tw-color-secondary-500) / <alpha-value>)',
          600: 'rgb(var(--tw-color-secondary-600) / <alpha-value>)',
          700: 'rgb(var(--tw-color-secondary-700) / <alpha-value>)',
          800: 'rgb(var(--tw-color-secondary-800) / <alpha-value>)',
          900: 'rgb(var(--tw-color-secondary-900) / <alpha-value>)',
          950: 'rgb(var(--tw-color-secondary-950) / <alpha-value>)',
        },
        carbon: {
          DEFAULT: '#1E1E1E',
          50: '#CBCBCB',
          100: '#C1C1C1',
          200: '#ADADAD',
          300: '#989898',
          400: '#848484',
          500: '#707070',
          600: '#5B5B5B',
          700: '#474747',
          800: '#323232',
          900: '#1E1E1E',
          950: '#101010',
        },
      },
      keyframes: {
        flicker: {
          '0%, 19.999%, 22%, 62.999%, 64%, 64.999%, 70%, 100%': {
            opacity: '0.99',
            filter:
              'drop-shadow(0 0 1px rgba(252, 211, 77)) drop-shadow(0 0 15px rgba(245, 158, 11)) drop-shadow(0 0 1px rgba(252, 211, 77))',
          },
          '20%, 21.999%, 63%, 63.999%, 65%, 69.999%': {
            opacity: '0.4',
            filter: 'none',
          },
        },
        shimmer: {
          '0%': {
            backgroundPosition: '-700px 0',
          },
          '100%': {
            backgroundPosition: '700px 0',
          },
        },
      },
      animation: {
        flicker: 'flicker 3s linear infinite',
        shimmer: 'shimmer 1.3s linear infinite',
      },
      maxWidth: {
        'screen-3xl': '1920px',
      },
      scale: {
        '80': '.80',
      },
      transitionProperty: {
        height: 'height',
        spacing: 'margin, padding',
      },
      gridColumn: {
        '1/1': '1 / 1',
      },
      gridRow: {
        '1/1': '1 / 1',
      },
    },
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    typography: ({ theme }: { theme: any }) => ({
      DEFAULT: {
        css: {
          h1: {
            fontSize: theme('fontSize.2xl'),
          },
          h2: {
            fontSize: theme('fontSize.xl'),
          },
          h3: {
            fontSize: theme('fontSize.lg'),
          },
          h4: {
            fontSize: theme('fontSize.base'),
          },
          p: {
            fontSize: theme('fontSize.sm'),
          },
          li: {
            marginTop: 'unset',
            marginBottom: 'unset',
            fontSize: theme('fontSize.sm'),
          },
          code: {
            backgroundColor: theme('colors.carbon[200]'),
            fontWeight: theme('fontWeight.normal'),
            borderRadius: '0.375rem',
            padding: theme('padding.1'),
            '&::before': {
              content: 'none !important',
            },
            '&::after': {
              content: 'none !important',
            },
          },
          table: {
            width: theme('width.fit'),
            borderColor: theme('colors.carbon[600]'),
            borderRadius: '0.375rem',
            borderWidth: '1px',
            borderCollapse: 'separate',
            borderSpacing: '0',
            overflow: 'hidden',
            color: theme('colors.white'),
            backgroundColor: theme('colors.carbon[800]'),
          },
          thead: {
            backgroundColor: theme('colors.carbon[700]'),
            borderWidth: '0',

            th: {
              padding: theme('padding.2'),
              borderBottomWidth: '1px',
              borderBottomColor: theme('colors.carbon[600]'),

              '&:not(:last-child)': {
                borderRightWidth: '1px',
                borderRightColor: theme('colors.carbon[600]'),
              },
            },
          },
          tbody: {
            td: {
              padding: theme('padding.2'),

              '&:not(:last-child)': {
                borderRightWidth: '1px',
                borderRightColor: theme('colors.carbon[600]'),
              },
            },
          },

          'table>thead>tr:not(:last-child)>th': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
          'table>thead>tr:not(:last-child)>td': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
          'table>tbody>tr:not(:last-child)>th': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
          'table>tbody>tr:not(:last-child)>td': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
          'table>tfoot>tr:not(:last-child)>th': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
          'table>tfoot>tr:not(:last-child)>td': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
          'table>tr:not(:last-child)>td': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
          'table>tr:not(:last-child)>th': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
          'table>thead:not(:last-child)': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
          'table>tbody:not(:last-child)': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
          'table>tfoot:not(:last-child)': {
            borderBottomWidth: '1px',
            borderBottomColor: theme('colors.carbon[600]'),
          },
        },
      },
      md: {
        css: {
          h1: {
            fontSize: theme('fontSize.4xl'),
          },
          h2: {
            fontSize: theme('fontSize.3xl'),
          },
          h3: {
            fontSize: theme('fontSize.2xl'),
          },
          h4: {
            fontSize: theme('fontSize.lg'),
          },
          p: {
            fontSize: theme('fontSize.base'),
          },
          li: {
            fontSize: theme('fontSize.base'),
          },
        },
      },
      carbon: {
        css: {
          '--tw-prose-body': theme('colors.carbon[800]'),
          '--tw-prose-headings': theme('colors.carbon[900]'),
          '--tw-prose-lead': theme('colors.carbon[700]'),
          '--tw-prose-links': theme('colors.carbon[900]'),
          '--tw-prose-bold': theme('colors.carbon[900]'),
          '--tw-prose-counters': theme('colors.carbon[600]'),
          '--tw-prose-bullets': theme('colors.carbon[400]'),
          '--tw-prose-hr': theme('colors.carbon[300]'),
          '--tw-prose-quotes': theme('colors.carbon[900]'),
          '--tw-prose-quote-borders': theme('colors.carbon[300]'),
          '--tw-prose-captions': theme('colors.carbon[700]'),
          '--tw-prose-code': theme('colors.carbon[900]'),
          '--tw-prose-pre-code': theme('colors.carbon[100]'),
          '--tw-prose-pre-bg': theme('colors.carbon[900]'),
          '--tw-prose-th-borders': theme('colors.carbon[300]'),
          '--tw-prose-td-borders': theme('colors.carbon[200]'),
          '--tw-prose-invert-body': theme('colors.carbon[200]'),
          '--tw-prose-invert-headings': theme('colors.white'),
          '--tw-prose-invert-lead': theme('colors.carbon[300]'),
          '--tw-prose-invert-links': theme('colors.white'),
          '--tw-prose-invert-bold': theme('colors.white'),
          '--tw-prose-invert-counters': theme('colors.carbon[400]'),
          '--tw-prose-invert-bullets': theme('colors.carbon[600]'),
          '--tw-prose-invert-hr': theme('colors.carbon[700]'),
          '--tw-prose-invert-quotes': theme('colors.carbon[100]'),
          '--tw-prose-invert-quote-borders': theme('colors.carbon[700]'),
          '--tw-prose-invert-captions': theme('colors.carbon[400]'),
          '--tw-prose-invert-code': theme('colors.white'),
          '--tw-prose-invert-pre-code': theme('colors.carbon[300]'),
          '--tw-prose-invert-pre-bg': 'rgb(0 0 0 / 50%)',
          '--tw-prose-invert-th-borders': theme('colors.carbon[600]'),
          '--tw-prose-invert-td-borders': theme('colors.carbon[700]'),
        },
      },
    }),
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
} satisfies Config;
